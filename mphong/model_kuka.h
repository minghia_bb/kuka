/*
* Copyright (c) Le Minh Nghia - KSTN - CDT k54 11/12/2013
*
* model_kuka.h
*/

#ifndef MODEL_KUKA_H
#define MODEL_KUKA_H

#include "model.h"

namespace robotData {
class CKuka : public Robot
{
public: // data 
	double tx1, ty1, tz1,
		tx2, ty2, tz2,
		tx3, ty3, tz3,
		tx4, ty4, tz4,
		tx5, ty5, tz5,
		tx6, ty6, tz6, rx6;
	double xE6, yE6, zE6;
	double m1, m2, m3, m4, m5, m6;
	double g1, g2, g3;
	double xCi1, yCi1, zCi1,
		xCi2, yCi2, zCi2,
		xCi3, yCi3, zCi3,
		xCi4, yCi4, zCi4,
		xCi5, yCi5, zCi5,
		xCi6, yCi6, zCi6;
	double Ixx1, Ixy1, Ixz1, Iyy1, Iyz1, Izz1,
		Ixx2, Ixy2, Ixz2, Iyy2, Iyz2, Izz2,
		Ixx3, Ixy3, Ixz3, Iyy3, Iyz3, Izz3,
		Ixx4, Ixy4, Ixz4, Iyy4, Iyz4, Izz4,
		Ixx5, Ixy5, Ixz5, Iyy5, Iyz5, Izz5,
		Ixx6, Ixy6, Ixz6, Iyy6, Iyz6, Izz6;
public:
	CKuka() 
	{
		tx1 = ty1 = 0.0; tz1 = 35.5e-2;
		tx2 = 5e-2; ty2 = 30e-2; tz2 = 26e-2;
		tx3 = -5e-2; ty3 = 0.0; tz3 = 64.99e-2;
		tx4 = -0.5e-2; ty4 = 34.5e-2; tz4 = 14.27e-2;
		tx5 = tz5 = 0.0; ty5 = 23.01e-2;
		tx6 = tz6 = 0.0; ty6 = 9.5e-2; rx6 = -PI/2;
		//
		xE6 = yE6 = 0.0;
		zE6 = 5e-2;
		//
		g1 = g2 = 0.0; g3 = -9.8;
		//
		m1 =  82.86076333;
		xCi1 = -0.00922199;		yCi1 = 0.09356479;		zCi1 = 0.12493503;
		Ixx1 = 5.40448946;
		Ixy1 = -0.22968662;		Iyy1 = 3.11791269;		
		Ixz1 = -0.19748672;		Iyz1 = 1.95287670;		Izz1 = 3.80191927;
		// 
		m2 = 12.09175059;
		xCi2 = 0.03478178;		yCi2 = 0.00000000;		zCi2 = 0.29369616;
		Ixx2 = 1.55734033;
		Ixy2 = -0.00000004;		Iyy2 = 1.55828681;
		Ixz2 = 0.12386719;		Iyz2 = -0.00000046;		Izz2 = 0.05697335;
		//
		m3 = 31.72385513;
		xCi3 = -0.03159768;		yCi3 = -0.01919667;		zCi3 = 0.11460646;
		Ixx3 = 1.37317183;
		Ixy3 = 0.06585863;		Iyy3 = 0.69119939;
		Ixz3 = -0.11401524;		Iyz3 = -0.09410482;		Izz3 = 0.89991726;
		//
		m4 = 7.73631962;
		xCi4 = 0.00631865;		yCi4 = 0.12961065;		zCi4 = 0.0;
		Ixx4 = 0.18224353;
		Ixy4 = 0.00808192;		Iyy4 = 0.01989315;
		Ixz4 = 0.00000002;		Iyz4 = -0.00000001;		Izz4 = 0.19088048;
		// 
		m5 = 2.24516398;
		xCi5 = 0.0; yCi5 = 0.02640240; zCi5 = 0.0;
		Ixx5 = 0.00788592;		
		Ixy5 = -0.00000002;		Iyy5 = 0.00244860;
		Ixz5 = Iyz5 = 0.00000000;	Izz5 = 0.00699308;
		//
		m6 = 0.27241484;// kilograms
		xCi6 = 0.00000000;		yCi6 = 0.02243118;		zCi6 = 0.0;
		Ixx6 = 0.00038778;				
		Ixy6 = 0.00000000;		Iyy6 = 0.00038778;
		Ixz6 = 0.00000000;		Iyz6 = 0.00000000;		Izz6 = 0.00047720;
	}
	void rE_kuka (double q1, double q2, double q3, double q4, double q5, double q6, double cgret[3]);
	void vE_kuka (double q1, double q2, double q3, double q4, double q5, double q6,
		double dq1, double dq2, double dq3, double dq4, double dq5, double dq6, double cgret[3]);
	void aE_kuka (double q1, double q2, double q3, double q4, double q5, double q6,
		double dq1, double dq2, double dq3, double dq4, double dq5, double dq6,
		double ddq1, double ddq2, double ddq3, double ddq4, double ddq5, double ddq6, double cgret[3]);
	void funcs3_kuka (double q1, double q2, double q3, double q4, double q5, double q6, double cgret[3]);
	void funcs6_kuka (double q1, double q2, double q3, double q4, double q5, double q6, double cgret[6]);
	void jacobi36_kuka (double q1, double q2, double q3, double q4, double q5, double q6, double cgret[3][6]);
	void jacobi66_kuka (double q1, double q2, double q3, double q4, double q5, double q6, double cgret[6][6]);
	void diff_jacobi36_kuka (double q1, double q2, double q3, double q4, double q5, double q6,
		double dq1, double dq2, double dq3, double dq4, double dq5, double dq6, double cgret[3][6]);
	void diff_jacobi66_kuka (double q1, double q2, double q3, double q4, double q5, double q6,
		double dq1, double dq2, double dq3, double dq4, double dq5, double dq6, double cgret[6][6]);
	void M_kuka (double q1, double q2, double q3, double q4, double q5, double q6, double cgret[6][6]);
	void Gq_kuka (double q1, double q2, double q3, double q4, double q5, double q6, double cgret[6]);
	//
	void C1_kuka (double q1, double q2, double q3, double q4, double q5, double q6,
		double dq1, double dq2, double dq3, double dq4, double dq5, double dq6, double cgret[6][6]);
	void C2_kuka (double q1, double q2, double q3, double q4, double q5, double q6,
		double dq1, double dq2, double dq3, double dq4, double dq5, double dq6, double cgret[6][6]);
	~CKuka() {};
};
}

namespace mdl {
	using namespace opengl;
	//
class Ckuka : public robotData::CKuka
{
public:
	robot::LoaiBaiToan		loaiBaiToan;
	Ckuka() {loaiBaiToan = robot::DHT_VITRI;};
	// vitri diem thao tac
	vectorXd	rE(vectorXd q)
	{
		vectorXd ret(3);
		double tmp[3];
		rE_kuka(q(0), q(1), q(2), q(3), q(4), q(5), tmp);
		ret(0) = tmp[0]; ret(1) = tmp[1]; ret(2) = tmp[2];
		return ret;
	}
	// vantoc diem thao tac
	vectorXd vE(vectorXd q, vectorXd dq)
	{
		vectorXd ret(3);
		double tmp[3];
		vE_kuka(q(0), q(1), q(2), q(3), q(4), q(5), dq(0), dq(1), dq(2), dq(3), dq(4), dq(5), tmp);
		ret(0) = tmp[0]; ret(1) = tmp[1]; ret(2) = tmp[2];
		return ret;
	}
	// giatoc diem thao tac
	vectorXd aE(vectorXd q, vectorXd dq, vectorXd ddq)
	{
		vectorXd ret(3);
		double tmp[3];
		aE_kuka(q(0), q(1), q(2), q(3), q(4), q(5), 
			dq(0), dq(1), dq(2), dq(3), dq(4), dq(5), 
			ddq(0), ddq(1), ddq(2), ddq(3), ddq(4), ddq(5), tmp);
		ret(0) = tmp[0]; ret(1) = tmp[1]; ret(2) = tmp[2];
		return ret;
	}
	// hept rang buoc dong hoc ve vi tri
	vectorXd funcsys3(vectorXd q)
	{
		vectorXd ret(3);
		double tmp[3];
		funcs3_kuka(q(0), q(1), q(2), q(3), q(4), q(5), tmp);
		for (int i=0;i<3;i++)
			ret(i)=tmp[i];
		return ret;
	}
	// hept rang buoc dong hoc ve vi tri va huong
	vectorXd funcsys6(vectorXd q)
	{
		vectorXd ret(6);
		double tmp[6];
		funcs6_kuka(q(0), q(1), q(2), q(3), q(4), q(5), tmp);
		for (int i=0;i<6;i++)
			ret(i)=tmp[i];
		return ret;
	}
	// matranJacobian
	matrixXd Jacobian36(vectorXd q)
	{
		matrixXd ret(3,6);
		double tmp[3][6];
		jacobi36_kuka(q(0), q(1), q(2), q(3), q(4), q(5), tmp);
		for (int i=0;i<3;i++)
			for (int j=0;j<6;j++)
				ret(i,j) = tmp[i][j];
		return ret;
	}
	// matranJacobian
	matrixXd Jacobian66(vectorXd q)
	{
		matrixXd ret(6,6);
		double tmp[6][6];
		jacobi66_kuka(q(0), q(1), q(2), q(3), q(4), q(5), tmp);
		for (int i=0;i<6;i++)
			for (int j=0;j<6;j++)
				ret(i,j) = tmp[i][j];
		return ret;
	}
	// daoham matranJacobian
	matrixXd diff_Jacobian36(vectorXd q, vectorXd dq)
	{
		matrixXd ret(3,6);
		double tmp[3][6];
		diff_jacobi36_kuka(q(0), q(1), q(2), q(3), q(4), q(5), dq(0), dq(1), dq(2), dq(3), dq(4), dq(5), tmp);
		for (int i=0;i<3;i++)
			for (int j=0;j<6;j++)
				ret(i,j) = tmp[i][j];
		return ret;
	}
	// daoham matranJacobian
	matrixXd diff_Jacobian66(vectorXd q, vectorXd dq)
	{
		matrixXd ret(6,6);
		double tmp[6][6];
		diff_jacobi66_kuka(q(0), q(1), q(2), q(3), q(4), q(5), dq(0), dq(1), dq(2), dq(3), dq(4), dq(5), tmp);
		for (int i=0;i<6;i++)
			for (int j=0;j<6;j++)
				ret(i,j) = tmp[i][j];
		return ret;
	}
	// matran khoiluong
	matrixXd MTKL(vectorXd q)
	{
		matrixXd ret(6,6);
		double mtkl[6][6];
		M_kuka(q(0), q(1), q(2), q(3), q(4), q(5), mtkl);
		for (int i=0;i<6;i++)
			for (int j=0;j<6;j++)
				ret(i,j)=mtkl[i][j];
		return ret;
	}
	// Vector luc co the
	vectorXd	Gq(vectorXd q)
	{
		vectorXd ret(6);
		double tmp[6];
		Gq_kuka(q(0), q(1), q(2), q(3), q(4), q(5), tmp);
		for (int i=0;i<6;i++)
			ret(i) = tmp[i];
		return ret;
	}
	// Ma tran coriolis
	matrixXd	Coriolis(vectorXd q, vectorXd dq)
	{
		// C1 = d(M)/dt = sigma(dM,qi)*dqi
		double h = 1e-6;
		matrixXd C1(6,6); C1.fill(0.0);
		for (int i=0;i<6;i++)
		{
			vectorXd qtmp(6); qtmp.fill(0.0);
			qtmp(i) = h;
			C1 = C1 + ((MTKL(q+qtmp) - MTKL(q))/h)*dq(i);
		}
		// C2 = jacobian(M.dq, q)
		matrixXd C2(6,6);
		/*for (int col = 0; col < 6; col++)
		{
			vectorXd col_i(6); col_i.fill(0.0);
			vectorXd qtmp(6); qtmp.fill(0.0);
			qtmp(col) = h;
			col_i = ((MTKL(q+qtmp) - MTKL(q)) / h)* dq ;
			for (int r = 0; r < 6; r++)
				C2(r, col) = col_i(r);
		}*/
		double c2_kuka[6][6];
		C2_kuka(q(0),q(1),q(2),q(3),q(4),q(5),dq(0),dq(1),dq(2),dq(3),dq(4),dq(5),c2_kuka);
		for (int i=0;i<6;i++)
			for(int j=0;j<6;j++)
				C2(i,j)=c2_kuka[i][j];
		return (C1 - 0.5*(C2.transpose()));
	}
	// Vv = Corioilis*dq + Gq
	vectorXd Vv(vectorXd q, vectorXd dq)
	{
		matrixXd coriolis = Coriolis(q, dq);
		vectorXd gq = Gq(q);
		return (coriolis * dq + gq);
	}
	// Tau
	vectorXd Tau(vectorXd q, vectorXd dq, vectorXd ddq)
	{
		matrixXd mtkl = MTKL(q),
			coriolis = Coriolis(q, dq);
		vectorXd gq = Gq(q);
		return (mtkl * ddq + coriolis * dq + gq);
	}
	~Ckuka() {};
	// vector
	matrixXd operator()(vectorXd q)
	{
		switch(loaiBaiToan)
		{
		default:
		case robot::DHT_VITRI:
			{
				return (rE(q));
			}
		case robot::DHN_VITRI: // diff_funcsys
			{
				if (numOfConstraints == 3)
					return (Jacobian36(q));
				return (Jacobian66(q));
			}
		//case robot::DHT_VIPHAN:
		case robot::DHN_VIPHAN: // Jacobian
			{
				if (numOfConstraints == 3)
					return (Jacobian36(q));
				return (Jacobian66(q));
			}
		case robot::DLHT:
			{
				return (MTKL(q));
			}
		//case robot::DLHN:
		}
	}
	// vectorXd
	matrixXd operator()(vectorXd q, vectorXd dq)
	{
		switch(loaiBaiToan)
		{
		default:
		//case robot::DHT_VITRI:
		//case robot::DHN_VITRI:
		case robot::DHT_VIPHAN:
			{
				return (vE(q, dq));
			}
		case robot::DHN_VIPHAN:
			{
				if (numOfConstraints == 3)
					return (diff_Jacobian36(q,dq));
				return (diff_Jacobian66(q,dq));
			}
		case robot::DLHT:
			{
				return (Vv(q,dq));
			}
		//case robot::DLHN:
		}
	}
	//
	vectorXd operator()(vectorXd q, vectorXd dq, vectorXd ddq)
	{
		switch(loaiBaiToan)
		{
		default:
		//case robot::DHT_VITRI:
		//case robot::DHN_VITRI:
		case robot::DHT_VIPHAN:
			{
				return (aE(q,dq,ddq));
			}
		//case robot::DHN_VIPHAN:
		//case robot::DLHT:
		case robot::DLHN:
			{
				return (Tau(q,dq,ddq));
			}
		}
	}
	// funcsys
	vectorXd operator()(vectorXd q, int func)
	{
		if (numOfConstraints == 3)
			return (funcsys3(q));
		return (funcsys6(q));
	}
};

class CmdlKuka 
{
	int numStlFiles;		// 8
	// stlFiles
	CStlReader			stlFiles[8];  // + 1 khau co dinh (base)
	GLuint				displayLists[8];
	CMaterial			material[8];
	// search path
	std::vector<std::string>	searchPaths;
public:
	// data
	Ckuka			myRobot;
	//CRRR			myRobot;
	//
	int numLinks;			// so khau = so khau dong + so khau co dinh (de^') = 3
	// scene
	CScene				m_Scene;
	double				donvi;				// draw trace, don vi // cm
public: // methods
	// Rotx (-90) = [1, 0, 0; 
	//				0, 0, 1;
	//				0, -1, 0]
	// C = [0,0,100]
	CmdlKuka() : myRobot(), m_Scene(500, 500, 600, 0, 100, 0), donvi(100.0)
	{
		numLinks = 7;		numStlFiles = 8;
		stlFiles[0].ObjectName = "8.stl";
		stlFiles[1].ObjectName = "1.stl";
		stlFiles[2].ObjectName = "2.stl";
		stlFiles[3].ObjectName = "3.stl";
		stlFiles[4].ObjectName = "4.stl";
		stlFiles[5].ObjectName = "5.stl";
		stlFiles[6].ObjectName = "6.stl";
		stlFiles[7].ObjectName = "7.stl";
		//
		searchPaths.push_back("stl/");
		searchPaths.push_back("F:/Documents/SolidWorks 2010/KUKA/");
		searchPaths.push_back("../Debug/stl/");
		//
		for (int i=0;i<numStlFiles;i++)
		{
			displayLists[i]=0;
			material[i] = storageMaterial[i];
		}
	}
	~CmdlKuka() {}
	void SetMaterial(const CMaterial& material, int face = _FACE_DEFAULT)
	{
		material.ApplyToFace(face);
	}
	void Create()
	{
		// Load stlFiles
		for (int i=0; i< numStlFiles; i++)
		{
			FILE* f = fopen(stlFiles[i].ObjectName.c_str(), "r");
			if (f)
			{
				fclose(f);
				continue;
			}
			for (int j=0;j<(int)searchPaths.size();j++)
			{
				std::string filePath;
				filePath.append(searchPaths[j]);
				filePath.append(stlFiles[i].ObjectName);
				FILE* ff = fopen(filePath.c_str(), "r");
				if (!ff)
					continue;
				stlFiles[i].ObjectName = filePath;
				fclose(ff);
				break;
			}
		}
		// load stlfiles
		for (int i =0;i < numStlFiles ;i++)
			stlFiles[i].Load(stlFiles[i].ObjectName);
		// create display list
		displayLists[0] = glGenLists(numStlFiles);
		for (int i=0; i < numStlFiles;i++)
		{
			if (i!=0)
				displayLists[i] = displayLists[i-1] + 1; // xac dinh dia chi cua list tiep theo
			glNewList(displayLists[i], GL_COMPILE);
			stlFiles[i].Draw(false, true);
			glEndList();
		}
	}
	void drawi(int linkID)
	{
		SetMaterial(material[linkID]);
		glCallList(displayLists[linkID]);
	}
	void draw(vectorXd q)
	{
		double q1 =0, q2 = 0, q3=0,q4=0,q5=0,q6=0;
		q1 = RAD2DEG(q(0)); q2 = RAD2DEG(q(1)); q3 = RAD2DEG(q(2)); q4 = RAD2DEG(q(3)); q5 = RAD2DEG(q(4)); q6 = RAD2DEG(q(5));
		//
		drawi(7);
		// 7 - > 6
		glTranslatef(0, 0, 35.5);
		drawi(6);
		// 6 - > 1
		glRotatef(q1, 0, 0, 1);
		drawi(1);
		// 1 - > 5
		glTranslatef(5.0f, 30.0f, 26.0f);
		glRotatef(q2, 1, 0, 0);
		drawi(5);
		// 5 - > 4
		glTranslatef(-5.0f, 0.0f, 64.99f);
		glRotatef(q3, 1, 0, 0);
		drawi(4);
		// 4 - > 3
		glTranslatef(-0.5f, 34.5f, 14.27f);
		glRotatef(q4, 0, 1, 0);
		drawi(3);
		// 3 - > 2
		glTranslatef(0.0f, 23.01f, 0.0f);
		glRotatef(q5, 1, 0, 0);
		drawi(2);
		// 2 - > 8
		glTranslatef(0.0f, 9.5f, 0.0f);
		glRotatef(q6, 0, 1, 0);
		glRotatef(-90.0, 1, 0, 0);
		drawi(0);
	}
	void drawTrace(int i, matrixXd rE, bool bLighting = true)
	{
		int nrow = rE.rows(), ncol = rE.cols();
		int ith = min(i, nrow-1);
		if (bLighting)
		{
			glDisable(GL_LIGHTING);
			glDisable(GL_LIGHT0);
		}
		glLineWidth(2.0f);
		glBegin(GL_LINE_STRIP);
		glColor4f(1,1,1,1);
		for (int j = 0; j < ith;j++)
		{
			double xe, ye, ze;
			xe = rE(j,0); ye = rE(j,1); ze = rE(j,2);
			xe *= donvi; ye *= donvi; ze *= donvi;
			glVertex3d(xe, ye, ze);
		}
		glEnd();
		if (bLighting)
		{
			glEnable(GL_LIGHTING);
			glEnable(GL_LIGHT0);
		}
	}
};
}
#endif //MODEL_KUKA_H