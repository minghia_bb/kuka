/*
*
*
*/
#pragma once
#include "robotdata.h"
#include "Matrix4x4d.h"
#include <fstream>

namespace robotData {
class C3dofs : public Robot
{
public:
	/*==========================================================================================================*/
	/* kich thuoc cac khau*/
	double th1, th2, th3,
		d1, d2, d3,
		a1, a2, a3,
		alp1, alp2 , alp3;
	/* klg are not defined, it'll get default value     [m1, m2, m3]*/
	double m1, m2, m3;
	/* gravity is not defined, it'll get default value  [g1, g2, g3]*/
	double g1, g2, g3;				// vetor gia toc trong truong
	/* rCi... are not defined, it'll get default value  */
	double xCi1, yCi1, zCi1;			// vi tri trong tam trong htd khau
	double xCi2, yCi2, zCi2;
	double xCi3, yCi3, zCi3;
	/* mtqt... are not defined, it'll get default value */
	double			// mtqt
		Ix1, Iy1, Iz1,
		Ix2, Iy2, Iz2,
		Ix3, Iy3, Iz3;
	//
	// Loai bai toan - loai cau hinh
	
	//
public:
	//==========================================================================================================
	// R-R-R * #7
	//==========================================================================================================
	double xE_RRR(double q1, double q2, double q3);
	double yE_RRR(double q1, double q2, double q3);
	double zE_RRR(double q1, double q2, double q3); // === > rE
	double vEx_RRR(double q1, double q2, double q3, double dq1, double dq2, double dq3);
	double vEy_RRR(double q1, double q2, double q3, double dq1, double dq2, double dq3);
	double vEz_RRR(double q1, double q2, double q3, double dq1, double dq2, double dq3);
	double aEx_RRR(double q1, double q2, double q3, double dq1, double dq2, double dq3, double ddq1, double ddq2, double ddq3);
	double aEy_RRR(double q1, double q2, double q3, double dq1, double dq2, double dq3, double ddq1, double ddq2, double ddq3);
	double aEz_RRR(double q1, double q2, double q3, double dq1, double dq2, double dq3, double ddq1, double ddq2, double ddq3);
	// dong hoc vi phan nguoc, ham xuat ra tu Maple
	void Jacobi_RRR (  double q1,  double q2,  double q3,  double output[3][3]);	// ma tran jacobi
	void diff_Jacobi_RRR (double q1, double q2, double q3, double dq1, double dq2, double dq3, double cgret[3][3]);	// dao ham ma tran Jacobi theo thoi gian
	// dong luc hoc M.dqq + H(q, dq) = tau(t) = QQE
	void M_RRR (double q1, double q2, double q3, double output[3][3]);		// ma tran khoi luong
	void H_RRR (double q1, double q2, double q3, double dq1, double dq2, double dq3, double cgret[3]);
	// my functions seft-define
	
	// addition functions
	
public:
	C3dofs(void);
	~C3dofs(void);
};
/*
class CRR : public C3dofs
{
public:
	CRR() : C3dofs() {loaiBaiToan = DHT_VITRI;}
	~CRR() {}
	//
	LoaiBaiToan		loaiBaiToan;
	// vitri diem thao tac
	vectorXd	rE(vectorXd q)
	{
		vectorXd ret(3);
		double xe, ye, ze;
		xe = xE_RRR(q(0), q(1), q(2));
		ye = yE_RRR(q(0), q(1), q(2));
		ze = zE_RRR(q(0), q(1), q(2));
		ret(0) = xe; ret(1) = ye; ret(2) = ze;
		return ret;
	}
	// vantoc diem thao tac
	vectorXd	vE(vectorXd	q, vectorXd dq)
	{
		vectorXd ret(3);
		ret(0) = vEx_RRR(q(0),q(1),q(2),dq(0), dq(1),dq(2));
		ret(1) = vEy_RRR(q(0),q(1),q(2),dq(0), dq(1),dq(2));
		ret(2) = vEz_RRR(q(0),q(1),q(2),dq(0), dq(1),dq(2));
		return ret;
	}
	// giatoc diem thao tac
	vectorXd	aE(vectorXd	q, vectorXd dq, vectorXd ddq)
	{
		vectorXd ret(3);
		ret(0) = aEx_RRR(q(0),q(1),q(2),dq(0), dq(1),dq(2), ddq(0), ddq(1), ddq(2));
		ret(1) = aEy_RRR(q(0),q(1),q(2),dq(0), dq(1),dq(2), ddq(0), ddq(1), ddq(2));
		ret(2) = aEz_RRR(q(0),q(1),q(2),dq(0), dq(1),dq(2), ddq(0), ddq(1), ddq(2));
		return ret;
	}
	// hept rang buoc dong hoc ve vi tri
	vectorXd funcsys(vectorXd q)
	{
		return (rE(q));
	}
	// matran Jacobian
	matrixXd Jacobian(vectorXd q)
	{
		double jac[3][3];
		Jacobi_RRR(q(0), q(1), q(3), jac);
		matrixXd ret(3,3);
		for (int i=0;i<3;i++)
			for (int j=0;j<3;j++)
				ret(i,j)=jac[i][j];
		return ret;
	}
	// daoham matranJacobian
	matrixXd diff_Jacobian(vectorXd q, vectorXd dq)
	{
		double diff_jac[3][3];
		diff_Jacobi_RRR(q(0),q(1),q(2),dq(0),dq(1),dq(2),diff_jac);
		matrixXd ret(3,3);
		for (int i=0;i<3;i++)
			for (int j=0;j<3;j++)
				ret(i,j)=diff_jac[i][j];
		return ret;
	}
	// matran khoiluong
	matrixXd MTKL(vectorXd q)
	{
		double m_rrr[3][3];
		M_RRR(q(0),q(1),q(2),m_rrr);
		matrixXd ret(3,3);
		for (int i=0;i<3;i++)
			for (int j=0;j<3;j++)
				ret(i,j)=m_rrr[i][j];
		return ret;
	}
	// 
	vectorXd Vv(vectorXd q, vectorXd dq)
	{
		double vV[3];
		H_RRR(q(0),q(1),q(2),dq(0),dq(1),dq(2),vV);
		vectorXd ret(3);
		for (int i=0;i<3;i++)
			ret(i)=vV[i];
		return ret;
	}
	// Tau
	vectorXd Tau(vectorXd q, vectorXd dq, vectorXd ddq)
	{
		return (MTKL(q)*ddq + Vv(q,dq));
	}
	//
	matrixXd operator()(vectorXd q)
	{
		switch(loaiBaiToan)
		{
		default:
		case DHT_VITRI:
			{
				return (rE(q));
			}
		case DHN_VITRI: // diff_funcsys
			{
				return (Jacobian(q));
			}
		case DHT_VIPHAN:
		case DHN_VIPHAN: // Jacobian
			{
				return (Jacobian(q));
			}
		case DLHT:
			{
				return (MTKL(q));
			}
		case DLHN:
			break;
		}
	}
	//
	matrixXd operator()(vectorXd q, vectorXd dq)
	{
		switch(loaiBaiToan)
		{
		default:
		case DHT_VITRI:
		case DHN_VITRI:
		case DHT_VIPHAN:
			{
				return (vE(q, dq));
			}
		case DHN_VIPHAN:
			{
				return (diff_Jacobian(q,dq));
			}
		case DLHT:
			{
				return (Vv(q,dq));
			}
		case DLHN:
			break;
		}
	}
	//
	vectorXd operator()(vectorXd q, vectorXd dq, vectorXd ddq)
	{
		switch(loaiBaiToan)
		{
		default:
		case DHT_VITRI:
		case DHN_VITRI:
		case DHT_VIPHAN:
			{
				return (aE(q,dq,ddq));
			}
		case DHN_VIPHAN:
		case DLHT:
		case DLHN:
			{
				return (Tau(q,dq,ddq));
			}
		}
	}
	// funcsys
	void operator()(vectorXd q, vectorXd &funcs)
	{
		funcs = funcsys(q);
	}
};

class Cobjects
{
public:
	// data 
	CRR			myRobot;
	int numLinks;
	// stlFiles
	CStlReader			stlFiles[4];
	GLuint				displayLists[4];
	CColor4d			colors[4];
	//
	matrixXd			m_input;
	matrixXd			m_output;
	//
	Cobjects() : myRobot()
	{
		numLinks = 4;
		stlFiles[0].ObjectName = "../Debug/stl/de.stl";
		stlFiles[1].ObjectName = "../Debug/stl/khau1.stl";
		stlFiles[2].ObjectName = "../Debug/stl/khau2.stl";
		stlFiles[3].ObjectName = "../Debug/stl/khau3.stl";
		colors[0] = CColor4d(.2, .2, .8);
		colors[1] = CColor4d(.5, .8, .1);
		colors[2] = CColor4d(.8, .8, 1.);
		colors[3] = CColor4d(.1, .8, .2);
		displayLists[0]=displayLists[1]=displayLists[2]=displayLists[3]=0;
	}
	~Cobjects() {}
	void Create()
	{
		// Load stlFiles
		FILE* f = fopen(stlFiles[0].ObjectName.c_str(), "r");
		if (!f)
		{
			FILE* ff = fopen("stl/de.stl", "r");
			if (ff)
			{
				stlFiles[0].ObjectName = "stl/de.stl";
				stlFiles[1].ObjectName = "stl/khau1.stl";
				stlFiles[2].ObjectName = "stl/khau2.stl";
				stlFiles[3].ObjectName = "stl/khau3.stl";
				fclose(ff);
			}
			else
			{
				stlFiles[0].ObjectName = "de.stl";
				stlFiles[1].ObjectName = "khau1.stl";
				stlFiles[2].ObjectName = "khau2.stl";
				stlFiles[3].ObjectName = "khau3.stl";
			}
		}
		else
			fclose(f);
		// load stlfiles
		for (int i =0;i<numLinks ;i++)
			stlFiles[i].Load(stlFiles[i].ObjectName);
		// create display list
		displayLists[0] = glGenLists(numLinks);
		for (int i=0;i<numLinks;i++)
		{
			if (i!=0)
				displayLists[i] = displayLists[i-1] + 1; // xac dinh dia chi cua list tiep theo
			glNewList(displayLists[i], GL_COMPILE);
			stlFiles[i].Draw(false, true);
		}
		glEndList();
	}
	void Transform(int i, vectorXd q)
	{
		CMatrix4x4d m;
		double a[] = {0, myRobot.a1, myRobot.a2, myRobot.a3};
		double d[] = {0, myRobot.d1, myRobot.d2, myRobot.d3};
		double alp[] = {0, myRobot.alp1, myRobot.alp2, myRobot.alp3};
		m.MatrixDH(q(i), d[i], a[i], alp[i]);
		glMultMatrixd(m.getm());
	}
	void draw(vectorXd q)
	{
		for (int i=0;i<numLinks; i++)
		{
			Transform(i, q);
			glMaterialfv(GL_FRONT, GL_DIFFUSE, colors[i].getColor());
			glMaterialfv(GL_FRONT, GL_SPECULAR, colors[i].getColor());
			glCallList(displayLists[i]);
		}
	}
	void drawTrace(int i, matrixXd rE)
	{
		int nrow = rE.rows(), ncol = rE.cols();
		int ith = min(i, nrow-1);
		glBegin(GL_LINE_STRIP);
		for (int j = 0; j<ith;j++)
		{
			double xe,ye, ze;
			xe=rE(j,0); ye=rE(j,1); ze=rE(j,2);
			glVertex3d(xe, ye, ze);
		}
		glEnd();
	}
};
*/

} // end robotData