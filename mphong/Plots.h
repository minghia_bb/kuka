/*
	(c) by Le Minh Nghia - KSTN CDT k54 - HUST - 26/9/2013
	Du lieu 1 dialog plot - n-line
*/
#pragma once
#include "PlotData.h"
namespace opengl
{
	namespace plot
	{
class CPlots
{
public:
	std::string m_Caption;
	std::string xtitle, ytitle;
	double xmax, ymax, xmin, ymin;	
	bool	fitWindow;
	bool bGrid, bAxes;
	
	std::vector<CPlotData>	plots;
	float	color_bgrd[4];
public:
	CPlots(void);
	~CPlots(void);
	int size(void);
	CPlotData operator[](int i_th);
	CPlots(const CPlots& plots);
	void AddPlot(std::vector<double> X, std::vector<double> Y, std::string legend,
		float r = 0.0, float g = 0.0, float b = 1.0, 
		int line_type = LINE_CONTINUOUS, double thickness = 1.0);
	void clear(void);
};

	}}