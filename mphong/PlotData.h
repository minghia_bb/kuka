/*
	(c) by Le Minh Nghia - KSTN CDT k54 - HUST - 26/9/2013
	Du lieu 1 plot - 1 line
*/

#pragma once
#include <vector>

#ifndef LINE_CONTINUOUS
#define LINE_CONTINUOUS			0
#define LINE_PLUS				1
#define LINE_DASH				2
#define LINE_DOT				3
#define LINE_DASH_DOT			4
#define LINE_ASTERISK			5
#endif // LINE_CONTINUOUS

#ifndef drawOneLine
#define drawOneLine(x1,y1,x2,y2) glBegin(GL_LINES); glVertex2f((x1),(y1));glVertex2f((x2),(y2));glEnd();
#endif //drawOneLine
namespace opengl
{
	namespace plot
	{
class CPlotData
{
public:
	int numpoint;	
	std::string legends;
	double thickness;
	float color[4];
	std::vector<double> X;
	std::vector<double> Y;
	int line_type;

	CPlotData(void);
	~CPlotData(void);
	void clear(void);
	CPlotData(const CPlotData& plot);
};
	}}
