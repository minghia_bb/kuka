// DialogControl.cpp : implementation file
//

#include "stdafx.h"
#include "mphong.h"
#include "DialogControl.h"
#include "DialogConfiurations.h"

// CDialogControl

IMPLEMENT_DYNCREATE(CDialogControl, CFormView)

CDialogControl::CDialogControl()
	: CFormView(CDialogControl::IDD)
{
	m_strFileName = _T("");
	m_nLoaiBaiToan = 0;
	m_bPlay = false;    
}

CDialogControl::~CDialogControl()
{
}

void CDialogControl::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_FILENAME, m_strFileName);
	DDX_Radio(pDX, IDC_RADIO_DHT_VITRI, m_nLoaiBaiToan);
}

BEGIN_MESSAGE_MAP(CDialogControl, CFormView)
	ON_BN_CLICKED(IDC_BTN_BROWSER, &CDialogControl::OnBrowser)
	ON_BN_CLICKED(IDC_BTN_COMPUTE, &CDialogControl::OnCompute)
	ON_BN_CLICKED(IDC_BTN_PLAY, &CDialogControl::OnBnClickedBtnPlay)
    ON_BN_CLICKED(IDC_BTN_CONFIGURATION, &CDialogControl::OnBnClickedBtnConfiguration)
END_MESSAGE_MAP()


// CDialogControl diagnostics

#ifdef _DEBUG
void CDialogControl::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CDialogControl::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CDialogControl message handlers


void CDialogControl::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	m_nLoaiBaiToan = 0;//robot::DHT_VIPHAN;
    KukaData = GetRenderView()->GetDocument()->m_OpenGLView.m_Objects.myRobot;
	// TODO: Add your specialized code here and/or call the base class
	UpdateData(0);
}


// get CMpView Class
CMpView* CDialogControl::GetRenderView(void)
{
	CMpApp* pApp=(CMpApp*) AfxGetApp();
	CMainFrame* pMainFrame = (CMainFrame*) pApp->m_pMainWnd;
	CMpView* pView = (CMpView*) pMainFrame->m_wndSplitter.GetPane(0, 1);
	return pView;
}


void CDialogControl::OnBrowser()
{
	// TODO: Add your control notification handler code here
	static TCHAR BASED_CODE szFilter1[]=_T("Text Files (*.txt)|*.txt|");
	CFileDialog fdlg(TRUE, // open
		NULL, 
		NULL, 
		OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, 
		szFilter1);			// filter
	if (fdlg.DoModal()==IDOK)
	{
		CString str=fdlg.GetPathName();
		GetDlgItem(IDC_FILENAME)->SetWindowTextW(str);
	}
}


void CDialogControl::OnCompute()
{
	// TODO: Add your control notification handler code here
	UpdateData();// save data from dialog
	//
	matrixXd dataBlock;
	vectorXd initial;
	std::string msg = "File not correct";
	if (!robot::ReadDataFromFile(CString2string(m_strFileName), dataBlock, initial, msg))
	{
		MessageBox(_T("Error while reading file: \n") + m_strFileName, CString(msg.c_str()), MB_OK | MB_ICONERROR);
		return;
	}
	// xu li du lieu
	CString caption;
	//
	switch (m_nLoaiBaiToan)
	{
	default:
	case robot::DHT_VITRI:
		{
			int DOFs = GetRenderView()->GetDocument()->m_OpenGLView.m_Objects.numLinks - 1;
			if (dataBlock.cols() != DOFs)
			{
				CString str; str.Format(_T("File not correct - num of columns must= dofs (= %d)\n But received %d columns!"),  
					DOFs, dataBlock.cols());
				MessageBox(str, _T("Error"), MB_OK | MB_ICONERROR);
				return;
			}
			//
			GetRenderView()->GetDocument()->m_OpenGLView.m_Objects.myRobot.loaiBaiToan = robot::DHT_VITRI;
			GetRenderView()->GetDocument()->m_q = dataBlock;
			GetRenderView()->GetDocument()->m_rE = 
				robot::ForwardKinematics(
				GetRenderView()->GetDocument()->m_OpenGLView.m_Objects.myRobot,
				GetRenderView()->GetDocument()->m_q);
			//
			caption = _T("DHT_VITRI");
			break;
		}
	case robot::DHN_VITRI:
		{
			int DOFs = GetRenderView()->GetDocument()->m_OpenGLView.m_Objects.numLinks - 1;
			if (dataBlock.cols() != 3 && dataBlock.cols() != 6)
			{
				CString str; str.Format(_T("File not correct - num of columns = %d or %d\n But received %d columns!"), 3,6, dataBlock.cols());
				MessageBox(str, _T("Error"), MB_OK | MB_ICONERROR);
				return ;
			}
			if (dataBlock.cols() > GetRenderView()->GetDocument()->m_OpenGLView.m_Objects.numLinks - 1)
			{
				CString str; str.Format(_T("File not correct - num of columns <= %d\n But received %d colums!"), 
					DOFs, dataBlock.cols());
				MessageBox(str, _T("Error"), MB_OK | MB_ICONERROR);
				return ;
			}
			if (NC::length(initial) != DOFs)
			{
				CString str; str.Format(_T("File not correct - length of initial must= %d\n But received %d columns!"), 
					DOFs, NC::length(initial));
				MessageBox(str, _T("Error"), MB_OK | MB_ICONERROR);
				return ;
			}
			GetRenderView()->GetDocument()->m_OpenGLView.m_Objects.myRobot.loaiBaiToan = robot::DHN_VITRI;
			//
			GetRenderView()->GetDocument()->m_OpenGLView.m_Objects.myRobot.numOfConstraints = dataBlock.cols(); // 3 or 6
			int nrow = dataBlock.rows(), ncol = dataBlock.cols();
			GetRenderView()->GetDocument()->m_rE = NC::sub_matrix(dataBlock, 0, nrow-1, 0, 2);
			GetRenderView()->GetDocument()->m_q = 
				robot::inverseKinematics(
				GetRenderView()->GetDocument()->m_OpenGLView.m_Objects.myRobot,
				dataBlock/*GetRenderView()->GetDocument()->m_rE*/, 
				initial, 4, 50, 1e-4);
			if (GetRenderView()->GetDocument()->m_q.rows() < 5)
			{
				MessageBox(_T("Error while computing! - singular points"), _T("Error"), MB_OK | MB_ICONERROR);
				return ;
			}
			//
			caption = _T("DHN_VITRI");
			if (ncol > 3)
				caption = _T("DHN_VITRI & HUONG");
			break;
		}
	case robot::DHT_VIPHAN:
		{
			int DOFs = GetRenderView()->GetDocument()->m_OpenGLView.m_Objects.numLinks - 1;
			if (dataBlock.cols() != 3*DOFs + 1)
			{
				CString str; str.Format(_T("File not correct - num of columns must= %d\n But received %d columns!"),  3*DOFs+1, dataBlock.cols());
				MessageBox(str, _T("Error"), MB_OK | MB_ICONERROR);
				return;
			}
			GetRenderView()->GetDocument()->m_OpenGLView.m_Objects.myRobot.loaiBaiToan = robot::DHT_VIPHAN;
			GetRenderView()->GetDocument()->m_time = dataBlock.col(0);
			int nrow = dataBlock.rows(), ncol = dataBlock.cols();
			if ((ncol - 1) % 3 !=0)
				return;
			int dofs = (ncol-1)/3;
			//		
			GetRenderView()->GetDocument()->m_q = NC::sub_matrix(dataBlock, 0, nrow-1, 1, dofs);
			GetRenderView()->GetDocument()->m_dq = NC::sub_matrix(dataBlock, 0, nrow-1, dofs+1, 2*dofs);
			GetRenderView()->GetDocument()->m_ddq = NC::sub_matrix(dataBlock, 0, nrow-1, 2*dofs+1, 3*dofs);
			GetRenderView()->GetDocument()->m_OpenGLView.m_Objects.myRobot.loaiBaiToan = robot::DHT_VITRI;
			GetRenderView()->GetDocument()->m_rE = 
				robot::ForwardKinematics(
				GetRenderView()->GetDocument()->m_OpenGLView.m_Objects.myRobot,
				GetRenderView()->GetDocument()->m_q);
			GetRenderView()->GetDocument()->m_OpenGLView.m_Objects.myRobot.loaiBaiToan = robot::DHT_VIPHAN;
			matrixXd vE_aE = robot::ForwardKinematics(
				GetRenderView()->GetDocument()->m_OpenGLView.m_Objects.myRobot,
				GetRenderView()->GetDocument()->m_q,
				GetRenderView()->GetDocument()->m_dq,
				GetRenderView()->GetDocument()->m_ddq);
			nrow = vE_aE.rows(); ncol = vE_aE.cols();
			GetRenderView()->GetDocument()->m_vE = NC::sub_matrix(vE_aE, 0, nrow-1, 0, 2);
			GetRenderView()->GetDocument()->m_aE = NC::sub_matrix(vE_aE, 0, nrow-1, 3, 5);
			GetRenderView()->GetDocument()->m_OpenGLView.m_Objects.myRobot.loaiBaiToan = robot::DLHN;
			GetRenderView()->GetDocument()->m_tau = 
				robot::inverseDynamics(
				GetRenderView()->GetDocument()->m_OpenGLView.m_Objects.myRobot,
				GetRenderView()->GetDocument()->m_q,
				GetRenderView()->GetDocument()->m_dq,
				GetRenderView()->GetDocument()->m_ddq);
			GetRenderView()->GetDocument()->m_OpenGLView.m_Objects.myRobot.loaiBaiToan = robot::DHT_VIPHAN;
			caption = _T("DHT_VIPHAN");
			break;
		}
	
	case robot::DHN_VIPHAN: // DHN_VIPHAN_HUONG or DHN_VPHAN_VITRI
		{
			int DOFs = GetRenderView()->GetDocument()->m_OpenGLView.m_Objects.numLinks - 1;
			if (dataBlock.cols() != 10 || dataBlock.cols() != 1+3*DOFs)
			{
				CString str; 
				str.Format(_T("File not correct - num of columns = %d (DHN_VIPHAN_VITRI)\n Or num of columns = %d (DHN_VIPHAN_HUONG+VITRI)\n But recived %d columns !"),  10, 3 * DOFs + 1, dataBlock.cols());
				MessageBox(str, _T("Error"), MB_OK | MB_ICONERROR);
				return;
			}
			if (NC::length(initial) != DOFs)
			{
				CString str; str.Format(_T("File not correct - length of initial must = %d\n But received %d !"), 
					DOFs, NC::length(initial));
				MessageBox(str, _T("Error"), MB_OK | MB_ICONERROR);
				return ;
			}
			GetRenderView()->GetDocument()->m_OpenGLView.m_Objects.myRobot.loaiBaiToan = robot::DHN_VIPHAN;
			GetRenderView()->GetDocument()->m_time = dataBlock.col(0);
			int nrow = dataBlock.rows(), ncol = dataBlock.cols();
			if ((ncol - 1) % 3 !=0)
				return;
			int dofs = (ncol-1)/3;
			GetRenderView()->GetDocument()->m_rE = NC::sub_matrix(dataBlock, 0, nrow-1, 1, 3);
			GetRenderView()->GetDocument()->m_vE = NC::sub_matrix(dataBlock, 0, nrow-1, dofs+1, dofs+3);
			GetRenderView()->GetDocument()->m_aE = NC::sub_matrix(dataBlock, 0, nrow-1, 2*dofs+1, 2*dofs+3);
			matrixXd block1 = NC::sub_matrix(dataBlock, 0, nrow-1, 1, dofs);
			matrixXd block2 = NC::sub_matrix(dataBlock, 0, nrow-1, dofs+1, 2*dofs);
			matrixXd block3 = NC::sub_matrix(dataBlock, 0, nrow-1, 2*dofs+1, 3*dofs);
			GetRenderView()->GetDocument()->m_OpenGLView.m_Objects.myRobot.loaiBaiToan = robot::DHN_VITRI;
			GetRenderView()->GetDocument()->m_OpenGLView.m_Objects.myRobot.numOfConstraints = dofs;
			GetRenderView()->GetDocument()->m_q = 
				robot::inverseKinematics(
				GetRenderView()->GetDocument()->m_OpenGLView.m_Objects.myRobot,
				block1, initial);
			if (GetRenderView()->GetDocument()->m_q.rows() < 5)
			{
				MessageBox(_T("Error while computing - singular points"), _T("Error"), MB_OK | MB_ICONERROR);
				return;
			}
			GetRenderView()->GetDocument()->m_OpenGLView.m_Objects.myRobot.loaiBaiToan = robot::DHN_VIPHAN;
			matrixXd dq_ddq = 
				robot::inverseKinematics(
				GetRenderView()->GetDocument()->m_OpenGLView.m_Objects.myRobot,
				GetRenderView()->GetDocument()->m_q,
				block2,
				block3);
			nrow = dq_ddq.rows(); ncol = dq_ddq.cols();
			GetRenderView()->GetDocument()->m_dq = NC::sub_matrix(dq_ddq, 0, nrow-1, 0, ncol/2-1);
			GetRenderView()->GetDocument()->m_ddq = NC::sub_matrix(dq_ddq, 0, nrow-1, ncol/2, ncol-1);
			GetRenderView()->GetDocument()->m_OpenGLView.m_Objects.myRobot.loaiBaiToan = robot::DLHN;
			GetRenderView()->GetDocument()->m_tau = 
				robot::inverseDynamics(
				GetRenderView()->GetDocument()->m_OpenGLView.m_Objects.myRobot,
				GetRenderView()->GetDocument()->m_q,
				GetRenderView()->GetDocument()->m_dq,
				GetRenderView()->GetDocument()->m_ddq);
			GetRenderView()->GetDocument()->m_OpenGLView.m_Objects.myRobot.loaiBaiToan = robot::DHN_VIPHAN;
			//
			if (dofs == 3)
				caption = _T("DHN_VIPHAN");
			else
				caption = _T("DHN_VIPHAN HUONG & VITRI");
			break;
		}
	case robot::DLHT:
		{
			int DOFs = GetRenderView()->GetDocument()->m_OpenGLView.m_Objects.numLinks - 1;
			if (dataBlock.cols() != 1+DOFs)
			{
				CString str; 
				str.Format(_T("File not correct - num of columns = %d"), DOFs + 1);
				MessageBox(str, _T("Error"), MB_OK | MB_ICONERROR);
				return;
			}
			if (NC::length(initial) != 2*DOFs)
			{
				CString str; str.Format(_T("File not correct - length of initial must = %d\n but received %d columns"), 
					2*DOFs, NC::length(initial));
				MessageBox(str, _T("Error"), MB_OK | MB_ICONERROR);
				return ;
			}
			GetRenderView()->GetDocument()->m_OpenGLView.m_Objects.myRobot.loaiBaiToan = robot::DLHT;
			GetRenderView()->GetDocument()->m_time = dataBlock.col(0);
			int nrow = dataBlock.rows(), ncol = dataBlock.cols();
			matrixXd	tau = NC::sub_matrix(dataBlock, 0, nrow-1, 1, ncol-1);
			ncol = NC::length(initial);
			vectorXd q0 = NC::sub_vector(initial, 0, ncol/2-1),
				dq0 = NC::sub_vector(initial, ncol/2, ncol-1);
			matrixXd time_q_dq_ddq
				= robot::ForwardDynamics(
				GetRenderView()->GetDocument()->m_OpenGLView.m_Objects.myRobot,
				/*GetRenderView()->GetDocument()->m_tau*/tau,
				GetRenderView()->GetDocument()->m_time,
				q0,
				dq0);
			nrow = time_q_dq_ddq.rows(); ncol = time_q_dq_ddq.cols();
			int dofs = (ncol-1)/3;
			GetRenderView()->GetDocument()->m_time = time_q_dq_ddq.col(0);
			GetRenderView()->GetDocument()->m_q = NC::sub_matrix(time_q_dq_ddq, 0, nrow-1, 1, dofs);
			GetRenderView()->GetDocument()->m_dq = NC::sub_matrix(time_q_dq_ddq, 0, nrow-1, dofs+1, 2*dofs);
			GetRenderView()->GetDocument()->m_ddq = NC::sub_matrix(time_q_dq_ddq, 0, nrow-1, 2*dofs+1, 3*dofs);
			GetRenderView()->GetDocument()->m_tau = matrixXd(nrow, dofs);
			for (int i=0;i<nrow;i++)
			{
				for (int j=0;j<dofs;j++)
					GetRenderView()->GetDocument()->m_tau(i,j) = tau(2*i,j);
			}
			GetRenderView()->GetDocument()->m_OpenGLView.m_Objects.myRobot.loaiBaiToan = robot::DHT_VITRI;
			GetRenderView()->GetDocument()->m_rE = 
				robot::ForwardKinematics(
				GetRenderView()->GetDocument()->m_OpenGLView.m_Objects.myRobot,
				GetRenderView()->GetDocument()->m_q);
			GetRenderView()->GetDocument()->m_OpenGLView.m_Objects.myRobot.loaiBaiToan = robot::DHT_VIPHAN;
			matrixXd vE_aE = robot::ForwardKinematics(
				GetRenderView()->GetDocument()->m_OpenGLView.m_Objects.myRobot,
				GetRenderView()->GetDocument()->m_q,
				GetRenderView()->GetDocument()->m_dq,
				GetRenderView()->GetDocument()->m_ddq);
			nrow = vE_aE.rows(); ncol = vE_aE.cols();
			GetRenderView()->GetDocument()->m_vE = NC::sub_matrix(vE_aE, 0, nrow-1, 0, 2);
			GetRenderView()->GetDocument()->m_aE = NC::sub_matrix(vE_aE, 0, nrow-1, 3, 5);
			GetRenderView()->GetDocument()->m_OpenGLView.m_Objects.myRobot.loaiBaiToan = robot::DLHT;
			caption = _T("DLHT");
			break;
		}
	}
	MessageBox(_T("Successed!"), caption);
}


void CDialogControl::OnBnClickedBtnPlay()
{
	// TODO: Add your control notification handler code here
	m_bPlay = !m_bPlay;
	if (m_bPlay)
	{
		GetRenderView()->SetTimer(GetRenderView()->GetDocument()->m_OpenGLView.m_nTimerID,
		GetRenderView()->GetDocument()->m_OpenGLView.m_nTimerElapsed, NULL);
		GetDlgItem(IDC_BTN_PLAY)->SetWindowTextW(_T("Stop"));
	}
	else
	{
		GetRenderView()->KillTimer(GetRenderView()->GetDocument()->m_OpenGLView.m_nTimerID);
		GetDlgItem(IDC_BTN_PLAY)->SetWindowTextW(_T("Play"));
	}
}


char* CDialogControl::CString2string(CString cs)
{
	char* str;
	str = new char[cs.GetLength() + 1];
	str[cs.GetLength()]='\0';
	for (int i=0;i<cs.GetLength();i++)
	{
		str[i]=(char)(cs[i]);
	}
	return str;
}

void CDialogControl::OnBnClickedBtnConfiguration()
{
    // TODO: Add your control notification handler code here
    CDialogConfiurations dlg;    
    robotKukaData data;
    data.tx1 = KukaData.tx1; data.ty1 = KukaData.ty1; data.tz1 = KukaData.tz1;
    data.tx2 = KukaData.tx2; data.ty2 = KukaData.ty2; data.tz2 = KukaData.tz2;
    data.tx3 = KukaData.tx3; data.ty3 = KukaData.ty3; data.tz3 = KukaData.tz3;
    data.tx4 = KukaData.tx4; data.ty4 = KukaData.ty4; data.tz4 = KukaData.tz4;
    data.tx5 = KukaData.tx5; data.ty5 = KukaData.ty5; data.tz5 = KukaData.tz5;
    data.tx6 = KukaData.tx6; data.ty6 = KukaData.ty6; data.tz6 = KukaData.tz6; data.rx6 = KukaData.rx6;
    data.xE6 = KukaData.xE6; data.yE6 = KukaData.yE6; data.zE6 = KukaData.zE6;
    data.g1 = KukaData.g1;    data.g2 = KukaData.g2;    data.g3 = KukaData.g3;
    data.m1 = KukaData.m1;
    data.xCi1 = KukaData.xCi1; data.yCi1 = KukaData.yCi1; data.zCi1 = KukaData.zCi1;
    data.Ixx1 = KukaData.Ixx1;
    data.Ixy1 = KukaData.Ixy1; data.Iyy1 = KukaData.Iyy1;
    data.Ixz1 = KukaData.Ixz1; data.Iyz1 = KukaData.Iyz1; data.Izz1 = KukaData.Izz1;
    data.m2 = KukaData.m2;
    data.xCi2 = KukaData.xCi2; data.yCi2 = KukaData.yCi2; data.zCi2 = KukaData.zCi2;
    data.Ixx2 = KukaData.Ixx2;
    data.Ixy2 = KukaData.Ixy2; data.Iyy2 = KukaData.Iyy2;
    data.Ixz2 = KukaData.Ixz2; data.Iyz2 = KukaData.Iyz2; data.Izz2 = KukaData.Izz2;
    data.m3 = KukaData.m3;
    data.xCi3 = KukaData.xCi3; data.yCi3 = KukaData.yCi3; data.zCi3 = KukaData.zCi3;
    data.Ixx3 = KukaData.Ixx3;
    data.Ixy3 = KukaData.Ixy3; data.Iyy3 = KukaData.Iyy3;
    data.Ixz3 = KukaData.Ixz3; data.Iyz3 = KukaData.Iyz3; data.Izz3 = KukaData.Izz3;
    data.m4 = KukaData.m4;
    data.xCi4 = KukaData.xCi4; data.yCi4 = KukaData.yCi4; data.zCi4 = KukaData.zCi4;
    data.Ixx4 = KukaData.Ixx4;
    data.Ixy4 = KukaData.Ixy4; data.Iyy4 = KukaData.Iyy4;
    data.Ixz4 = KukaData.Ixz4; data.Iyz4 = KukaData.Iyz4; data.Izz4 = KukaData.Izz4;
    data.m5 = KukaData.m5;
    data.xCi5 = KukaData.xCi5; data.yCi5 = KukaData.yCi5; data.zCi5 = KukaData.zCi5;
    data.Ixx5 = KukaData.Ixx5;
    data.Ixy5 = KukaData.Ixy5; data.Iyy5 = KukaData.Iyy5;
    data.Ixz5 = KukaData.Ixz5; data.Iyz5 = KukaData.Iyz5; data.Izz5 = KukaData.Izz5;
    data.m6 = KukaData.m6;
    data.xCi6 = KukaData.xCi6; data.yCi6 = KukaData.yCi6; data.zCi6 = KukaData.zCi6;
    data.Ixx6 = KukaData.Ixx6;
    data.Ixy6 = KukaData.Ixy6; data.Iyy6 = KukaData.Iyy6;
    data.Ixz6 = KukaData.Ixz6; data.Iyz6 = KukaData.Iyz6; data.Izz6 = KukaData.Izz6;

    dlg.data = data;
    if (dlg.DoModal() == IDOK)
    {
        data = dlg.data;
        KukaData.xE6 = data.xE6; KukaData.yE6 = data.yE6; KukaData.zE6 = data.zE6;
        KukaData.tx1 = data.tx1; KukaData.ty1 = data.ty1; KukaData.tz1 = data.tz1;
        KukaData.m1 = data.m1;
        KukaData.xCi1 = data.xCi1; KukaData.yCi1 = data.yCi1; KukaData.zCi1 = data.zCi1;
        KukaData.Ixx1 = data.Ixx1;
        KukaData.Ixy1 = data.Ixy1; KukaData.Iyy1 = data.Iyy1;
        KukaData.Ixz1 = data.Ixz1; KukaData.Iyz1 = data.Iyz1; KukaData.Izz1 = data.Izz1;
        KukaData.tx2 = data.tx2; KukaData.ty2 = data.ty2; KukaData.tz2 = data.tz2;
        KukaData.m2 = data.m2;
        KukaData.xCi2 = data.xCi2; KukaData.yCi2 = data.yCi2; KukaData.zCi2 = data.zCi2;
        KukaData.Ixx2 = data.Ixx2;
        KukaData.Ixy2 = data.Ixy2; KukaData.Iyy2 = data.Iyy2;
        KukaData.Ixz2 = data.Ixz2; KukaData.Iyz2 = data.Iyz2; KukaData.Izz2 = data.Izz2;
        KukaData.tx3 = data.tx3; KukaData.ty3 = data.ty3; KukaData.tz3 = data.tz3;
        KukaData.m3 = data.m3;
        KukaData.xCi3 = data.xCi3; KukaData.yCi3 = data.yCi3; KukaData.zCi3 = data.zCi3;
        KukaData.Ixx3 = data.Ixx3;
        KukaData.Ixy3 = data.Ixy3; KukaData.Iyy3 = data.Iyy3;
        KukaData.Ixz3 = data.Ixz3; KukaData.Iyz3 = data.Iyz3; KukaData.Izz3 = data.Izz3;
        KukaData.tx4 = data.tx4; KukaData.ty4 = data.ty4; KukaData.tz4 = data.tz4;
        KukaData.m4 = data.m4;
        KukaData.xCi4 = data.xCi4; KukaData.yCi4 = data.yCi4; KukaData.zCi4 = data.zCi4;
        KukaData.Ixx4 = data.Ixx4;
        KukaData.Ixy4 = data.Ixy4; KukaData.Iyy4 = data.Iyy4;
        KukaData.Ixz4 = data.Ixz4; KukaData.Iyz4 = data.Iyz4; KukaData.Izz4 = data.Izz4;
        KukaData.tx5 = data.tx5; KukaData.ty5 = data.ty5; KukaData.tz5 = data.tz5;
        KukaData.m5 = data.m5;
        KukaData.xCi5 = data.xCi5; KukaData.yCi5 = data.yCi5; KukaData.zCi5 = data.zCi5;
        KukaData.Ixx5 = data.Ixx5;
        KukaData.Ixy5 = data.Ixy5; KukaData.Iyy5 = data.Iyy5;
        KukaData.Ixz5 = data.Ixz5; KukaData.Iyz5 = data.Iyz5; KukaData.Izz5 = data.Izz5;
        KukaData.tx6 = data.tx6; KukaData.ty6 = data.ty6; KukaData.tz6 = data.tz6;
        KukaData.m6 = data.m6;
        KukaData.xCi6 = data.xCi6; KukaData.yCi6 = data.yCi6; KukaData.zCi6 = data.zCi6;
        KukaData.Ixx6 = data.Ixx6;
        KukaData.Ixy6 = data.Ixy6; KukaData.Iyy6 = data.Iyy6;
        KukaData.Ixz6 = data.Ixz6; KukaData.Iyz6 = data.Iyz6; KukaData.Izz6 = data.Izz6; KukaData.rx6 = data.rx6;

        //GetRenderView()->GetDocument()->m_OpenGLView.m_Objects.myRobot = KukaData;        
    }
}