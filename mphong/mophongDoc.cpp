
// mophongDoc.cpp : implementation of the CMpDoc class
//

#include "stdafx.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
#include "mphong.h"
#endif

#include "mophongDoc.h"
/* Mycode begins*/
#include "MainFrm.h"
/* Mycode ends*/
#include <propkey.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CMpDoc

IMPLEMENT_DYNCREATE(CMpDoc, CDocument)

BEGIN_MESSAGE_MAP(CMpDoc, CDocument)
	ON_COMMAND(ID_OPTIONS_DLG, &CMpDoc::OnOptionsDlg)
	ON_COMMAND(ID_VIEW_VIEWTOP, &CMpDoc::OnViewViewtop)
	ON_COMMAND(ID_VIEW_VIEWBOTTOM, &CMpDoc::OnViewViewbottom)
	ON_COMMAND(ID_VIEW_VIEWLEFT, &CMpDoc::OnViewViewleft)
	ON_COMMAND(ID_VIEW_VIEWRIGHT, &CMpDoc::OnViewViewright)
	ON_COMMAND(ID_VIEW_VIEWFRONT, &CMpDoc::OnViewViewfront)
	ON_COMMAND(ID_VIEW_VIEWBACK, &CMpDoc::OnViewViewback)
	ON_COMMAND(ID_VIEW_AXIS, &CMpDoc::OnViewAxis)
	ON_UPDATE_COMMAND_UI(ID_VIEW_AXIS, &CMpDoc::OnUpdateViewAxis)
	ON_COMMAND(ID_VIEW_VIEWPERSPECTIVE, &CMpDoc::OnViewViewperspective)
END_MESSAGE_MAP()


// CMpDoc construction/destruction
/**/
/**/
CMpDoc::CMpDoc() : m_Plots(), m_OpenGLView()
{
	// TODO: add one-time construction code here
}

CMpDoc::~CMpDoc()
{
}

BOOL CMpDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}




// CMpDoc serialization

void CMpDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

#ifdef SHARED_HANDLERS

// Support for thumbnails
void CMpDoc::OnDrawThumbnail(CDC& dc, LPRECT lprcBounds)
{
	// Modify this code to draw the document's data
	dc.FillSolidRect(lprcBounds, RGB(255, 255, 255));

	CString strText = _T("TODO: implement thumbnail drawing here");
	LOGFONT lf;

	CFont* pDefaultGUIFont = CFont::FromHandle((HFONT) GetStockObject(DEFAULT_GUI_FONT));
	pDefaultGUIFont->GetLogFont(&lf);
	lf.lfHeight = 36;

	CFont fontDraw;
	fontDraw.CreateFontIndirect(&lf);

	CFont* pOldFont = dc.SelectObject(&fontDraw);
	dc.DrawText(strText, lprcBounds, DT_CENTER | DT_WORDBREAK);
	dc.SelectObject(pOldFont);
}

// Support for Search Handlers
void CMpDoc::InitializeSearchContent()
{
	CString strSearchContent;
	// Set search contents from document's data. 
	// The content parts should be separated by ";"

	// For example:  strSearchContent = _T("point;rectangle;circle;ole object;");
	SetSearchContent(strSearchContent);
}

void CMpDoc::SetSearchContent(const CString& value)
{
	if (value.IsEmpty())
	{
		RemoveChunk(PKEY_Search_Contents.fmtid, PKEY_Search_Contents.pid);
	}
	else
	{
		CMFCFilterChunkValueImpl *pChunk = NULL;
		ATLTRY(pChunk = new CMFCFilterChunkValueImpl);
		if (pChunk != NULL)
		{
			pChunk->SetTextValue(PKEY_Search_Contents, value, CHUNK_TEXT);
			SetChunkValue(pChunk);
		}
	}
}

#endif // SHARED_HANDLERS

// CMpDoc diagnostics

#ifdef _DEBUG
void CMpDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMpDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CMpDoc commands


void CMpDoc::OnOptionsDlg()
{
	// TODO: Add your command handler code here
	if ((m_OpenGLView.m_Objects.myRobot.loaiBaiToan == robot::DHT_VITRI) ||
		(m_OpenGLView.m_Objects.myRobot.loaiBaiToan == robot::DHN_VITRI) ||
		(m_OpenGLView.m_Objects.myRobot.loaiBaiToan == robot::DLHN))
		return;
	if (m_q.rows() < 5)
		return;
	int k_cl = 0;
	float diff_cl = 0.0;
	float color_index[][3] = {
							  {1.00f, 0.00f, 0.00f},		// red
							  {0.00f, 0.00f, 1.00f},		// blue
							  {0.00f, 0.00f, 0.00f},		// black
							  {0.69f, 0.45f, 0.97f},		// violet
							  {0.50f, 0.10f, 0.02f},		// chocolate
							  {0.16f, 0.07f, 0.02f},		// coffee
							  {0.35f, 0.23f, 0.48f},		// ca tim
							  {0.99f, 0.33f, 0.10f},
							  
							  {0.80f, 0.80f, 0.80f},		// red 2
							  {0.00f, 0.00f, 1.00f},		// blue 2
							  {0.50f, 0.50f, 0.50f},		// black 2
							  {0.60f, 0.45f, 0.70f},		// violet 2
							  {0.50f, 0.19f, 0.22f},		// chocolate 2
							  {0.16f, 0.17f, 0.12f},		// coffee 2
							  {0.65f, 0.23f, 0.48f},		// ca tim 2
							  {0.19f, 0.63f, 0.15f}};		// copper 2
	std::string comboList[] = {"t", "q","dq","ddq","rE","vE","aE","tau"};
	std::vector<int> m_X, m_Y;
	CPlotManagerDlg dlg;
	if (dlg.DoModal() == IDOK)
	{
		m_X = dlg.m_X;
		m_Y = dlg.m_Y;
		m_Plots.clear();
		for (int i = 0; i < (int)m_X.size(); i++)
		{
			std::vector<double> m_dX, m_dY;
			if (m_X[i] == 0 && m_Y[i] == 0) // time - time
			{
				m_dX = getVector(m_time);
				m_dY = getVector(m_time);
				m_Plots.AddPlot(m_dX, m_dY, comboList[m_Y[i]], color_index[k_cl][0]+diff_cl, color_index[k_cl][1]+diff_cl, color_index[k_cl][2] + diff_cl);
				k_cl++;	if (k_cl != 0 && k_cl % 16 == 0) diff_cl += 0.008;
			}
			if ((m_X[i]!=0) && (m_Y[i] ==0) || (m_X[i]==0) && (m_Y[i]!=0)) // time - (q,dq,ddq,rE,vE,aE,tau) - time
			{
				if (m_X[i]==0)
					m_dX = getVector(m_time);
				if (m_Y[i]==0)
					m_dY = getVector(m_time);
				int index = (m_X[i] != 0) ? m_X[i] : m_Y[i];
				char buf[30];
				switch (index)
				{
				default:
				case 1: // q
					{
						for (int j=0;j <m_q.cols(); j++)
						{
							if (m_Y[i]!=0)	m_dY = getVector(m_q.col(j));
							else			m_dX = getVector(m_q.col(j));
							sprintf(buf, "q%d(t)", j);
							m_Plots.AddPlot(m_dX, m_dY, buf, color_index[k_cl][0]+diff_cl, color_index[k_cl][1]+diff_cl, color_index[k_cl][2] + diff_cl);
							k_cl++;	if (k_cl != 0 && k_cl % 16 == 0) diff_cl += 0.008;
						}
						break;
					}
				case 2: // dq
					{
						for (int j=0;j <m_dq.cols(); j++)
						{
							if (m_Y[i]!=0)	m_dY = getVector(m_dq.col(j));
							else			m_dX=getVector(m_dq.col(j));
							sprintf(buf, "dq%d(t)", j);
							m_Plots.AddPlot(m_dX, m_dY, buf, color_index[k_cl][0]+diff_cl, color_index[k_cl][1]+diff_cl, color_index[k_cl][2] + diff_cl);
							k_cl++;	if (k_cl != 0 && k_cl % 16 == 0) diff_cl += 0.008;
						}
						break;
					}
				case 3: // ddq
					{
						for (int j=0;j <m_ddq.cols(); j++)
						{
							if (m_Y[i]!=0)		m_dY = getVector(m_ddq.col(j));
							else				m_dX = getVector(m_ddq.col(j));
							sprintf(buf, "ddq%d(t)", j);
							m_Plots.AddPlot(m_dX, m_dY, buf, color_index[k_cl][0]+diff_cl, color_index[k_cl][1]+diff_cl, color_index[k_cl][2] + diff_cl);
							k_cl++;	if (k_cl != 0 && k_cl % 16 == 0) diff_cl += 0.008;
						}
						break;
					}
				case 4: //rE
					{
						for (int j=0;j <m_rE.cols(); j++)
						{
							if (m_Y[i]!=0)		m_dY = getVector(m_rE.col(j));
							else				m_dX = getVector(m_rE.col(j));
							std::string legend = "rE";
							if (j==0) legend = "xE";
							if (j==1) legend = "yE";
							if (j==2) legend = "zE";
							m_Plots.AddPlot(m_dX, m_dY, legend, color_index[k_cl][0]+diff_cl, color_index[k_cl][1]+diff_cl, color_index[k_cl][2] + diff_cl);
							k_cl++;	if (k_cl != 0 && k_cl % 16 == 0) diff_cl += 0.008;
						}
						break;
					}
				case 5: // vE
					{
						for (int j=0;j <m_vE.cols(); j++)
						{
							if (m_Y[i]!=0)		m_dY = getVector(m_vE.col(j));
							else				m_dX = getVector(m_vE.col(j));
							std::string legend = "vE";
							if (j==0) legend = "vEx";
							if (j==1) legend = "vEy";
							if (j==2) legend = "vEz";
							m_Plots.AddPlot(m_dX, m_dY, legend, color_index[k_cl][0]+diff_cl, color_index[k_cl][1]+diff_cl, color_index[k_cl][2] + diff_cl);
							k_cl++;	if (k_cl != 0 && k_cl % 16 == 0) diff_cl += 0.008;
						}
						break;
					}
				case 6: // aE
					{
						for (int j=0;j <m_aE.cols(); j++)
						{
							if (m_Y[i]!=0)		m_dY = getVector(m_aE.col(j));
							else				m_dX = getVector(m_aE.col(j));
							std::string legend = "aE";
							if (j==0) legend = "aEx";
							if (j==1) legend = "aEy";
							if (j==2) legend = "aEz";
							m_Plots.AddPlot(m_dX, m_dY, legend, color_index[k_cl][0]+diff_cl, color_index[k_cl][1]+diff_cl, color_index[k_cl][2] + diff_cl);
							k_cl++;	if (k_cl != 0 && k_cl % 16 == 0) diff_cl += 0.008;
						}
						break;
					}
				case 7: //tau
					{
						for (int j=0;j <m_tau.cols(); j++)
						{
							if (m_Y[i]!=0)		m_dY = getVector(m_tau.col(j));
							else				m_dX = getVector(m_tau.col(j));
							char buf[30]; sprintf(buf, "tau_%d(t)", j);
							m_Plots.AddPlot(m_dX, m_dY, buf, color_index[k_cl][0]+diff_cl, color_index[k_cl][1]+diff_cl, color_index[k_cl][2] + diff_cl);
							k_cl++;	if (k_cl != 0 && k_cl % 16 == 0) diff_cl += 0.008;
						}
						break;
					}
				}
			}
			if (m_X[i] * m_Y[i] != 0)
				return;
		}
		CPlotDlg	dlgg;
		if (m_Plots.size() > 0)
		{
			dlgg.m_Plot.SetData(m_Plots);
			//dlgg.m_Plot.InitialAxis(-0.05, 2.01);
			dlgg.m_Plot.FitWindow();
		}
		dlgg.DoModal();
	}
	// Fucking Code Here
	switchOpenGLContext();
}


void CMpDoc::OnViewViewtop()
{
	// TODO: Add your command handler code here
	CMpApp* pApp=(CMpApp*) AfxGetApp();
	CMainFrame* pMainFrame = (CMainFrame*) pApp->m_pMainWnd;
	CStatusBar* pStatus= &pMainFrame->m_wndStatusBar;
	pStatus->SetPaneText(3, _T("View Top"));
	m_OpenGLView.OnViewTop();
}


void CMpDoc::OnViewViewbottom()
{
	// TODO: Add your command handler code here
	CMpApp* pApp=(CMpApp*) AfxGetApp();
	CMainFrame* pMainFrame = (CMainFrame*) pApp->m_pMainWnd;
	CStatusBar* pStatus= &pMainFrame->m_wndStatusBar;
	pStatus->SetPaneText(3, _T("View Bottom"));
	m_OpenGLView.OnViewBottom();
}


void CMpDoc::OnViewViewleft()
{
	// TODO: Add your command handler code here
	CMpApp* pApp=(CMpApp*) AfxGetApp();
	CMainFrame* pMainFrame = (CMainFrame*) pApp->m_pMainWnd;
	CStatusBar* pStatus= &pMainFrame->m_wndStatusBar;
	pStatus->SetPaneText(3, _T("View Left"));
	m_OpenGLView.OnViewLeft();
}


void CMpDoc::OnViewViewright()
{
	// TODO: Add your command handler code here
	CMpApp* pApp=(CMpApp*) AfxGetApp();
	CMainFrame* pMainFrame = (CMainFrame*) pApp->m_pMainWnd;
	CStatusBar* pStatus= &pMainFrame->m_wndStatusBar;
	pStatus->SetPaneText(3, _T("View Right"));
	m_OpenGLView.OnViewRight();
}


void CMpDoc::OnViewViewfront()
{
	// TODO: Add your command handler code here
	CMpApp* pApp=(CMpApp*) AfxGetApp();
	CMainFrame* pMainFrame = (CMainFrame*) pApp->m_pMainWnd;
	CStatusBar* pStatus= &pMainFrame->m_wndStatusBar;
	pStatus->SetPaneText(3, _T("View Front"));
	m_OpenGLView.OnViewFront();
}


void CMpDoc::OnViewViewback()
{
	// TODO: Add your command handler code here
	CMpApp* pApp=(CMpApp*) AfxGetApp();
	CMainFrame* pMainFrame = (CMainFrame*) pApp->m_pMainWnd;
	CStatusBar* pStatus= &pMainFrame->m_wndStatusBar;
	pStatus->SetPaneText(3, _T("View Back"));
	m_OpenGLView.OnViewBack();
}


void CMpDoc::OnViewAxis()
{
	// TODO: Add your command handler code here
	m_OpenGLView.m_Scene.m_bAxis = !m_OpenGLView.m_Scene.m_bAxis;
	SendMessage(m_OpenGLView.hWnd, WM_PAINT, 0, 0);
}


void CMpDoc::OnUpdateViewAxis(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->SetCheck(m_OpenGLView.m_Scene.m_bAxis);
}


void CMpDoc::OnViewViewperspective()
{
	// TODO: Add your command handler code here
	CMpApp* pApp=(CMpApp*) AfxGetApp();
	CMainFrame* pMainFrame = (CMainFrame*) pApp->m_pMainWnd;
	CStatusBar* pStatus= &pMainFrame->m_wndStatusBar;
	pStatus->SetPaneText(3, _T("View perspective"));
	m_OpenGLView.OnViewPerspective();
}


void CMpDoc::switchOpenGLContext(void)
{
	wglMakeCurrent(m_OpenGLView.hDC, m_OpenGLView.hRC);
}
