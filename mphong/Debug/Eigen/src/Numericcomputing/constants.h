/*
* Copyright (c) by Le Minh Nghia - KSTN CDT k54 - HUST - 26/11/2013
* Constants
*/
#ifndef CONSTANTS_H
#define CONSTANTS_H

#ifndef PI
#define PI					3.1415926535897932384626433832795
#endif //PI

#ifndef INF
#define INF					1e15
#endif //INF

#ifndef NEPE
#define NEPE				2.7182818284590452353602874713527
#endif //NEPE

#ifndef MIN
#define MIN(a,b)			(((a) < (b)) ? (a) : (b))
#endif //MIN

#ifndef MAX
#define MAX(a,b)			(((a) > (b)) ? (a) : (b))
#endif //MAX

#ifndef RAD2DEG
#define RAD2DEG(rad)		(rad)*(180.0)/(PI)
#endif //RAD2DEG

#ifndef DEG2RAD
#define DEG2RAD(deg)		(deg)*(PI)/(180.0)
#endif //DEG2RAD

#ifndef pi
#define pi					PI
#endif //pi

#ifndef inf
#define inf					INF
#endif //inf

#ifndef nepe
#define nepe				NEPE
#endif //nepe

#ifndef min
#define min(a,b)			(((a) < (b)) ? (a) : (b))
#endif //min

#ifndef max
#define max(a,b)			(((a) > (b)) ? (a) : (b))
#endif //max

#ifndef rad2deg
#define rad2deg(rad)		(rad)*(180.0)/(pi)
#endif //rad2deg

#ifndef deg2rad
#define deg2rad(deg)		(deg)*(pi)/(180.0)
#endif //deg2rad

#endif // CONSTANTS_H