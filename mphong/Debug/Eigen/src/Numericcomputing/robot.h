/*
* Copyright (c) Le Minh Nghia - KSTN CDT - k54 - HUST
* <26 Nov 2013>
* robot - Tinh toan dong hoc, dong luc hoc thuan nguoc
*/
#ifndef ROBOT_HEADER_H
#define ROBOT_HEADER_H

#include "numericcomputing.h"
#include <fstream>

namespace robot
{
	typedef Eigen::MatrixXd matrixXd;
	typedef Eigen::VectorXd vectorXd;
	enum LoaiBaiToan
	{
		DHT_VITRI = 0,	// Dong hoc thuan ve vi tri (3D only)
		DHN_VITRI,		// Dong hoc nguoc ve vi tri (3D only)
		DHT_VIPHAN,		// Dong hoc thuan ve van toc, gia toc (2D(3D))
		DHN_VIPHAN,		// Dong hoc nguoc ve van toc, gia toc (2D(3D))
		//DHN_HUONG,		// Dong hoc nguoc ve huong (2D(3D))  // chi co o ban may 2btd
		DLHT,			// Dong luc hoc thuan (2D, 3D)
		DLHN			// Dong luc hoc nguoc (2D(3D))
	};
	//
	template <class Model>
	matrixXd ForwardKinematics(Model model, matrixXd q)
	{
		vectorXd qi, rE;
		int nrow = q.rows();
		matrixXd ret(nrow, 3);
		for (int i=0;i<nrow; i++)
		{
			qi = q.row(i);
			rE = model(qi);		// tinh rE=[xE,yE,zE]
			ret(i,0)=rE(0); ret(i,1)=rE(1); ret(i,2)=rE(2);
		}
		return ret;
	}
	//
	template <class Model>
	matrixXd ForwardKinematics(Model model, matrixXd q, matrixXd dq, matrixXd ddq)
	{
		int nrow = q.rows();
		matrixXd ret(nrow, 6);
		vectorXd qi, dqi, ddqi, vE, aE;
		ret.fill(0.0);
		for (int i=0;i<nrow; i++)
		{
			qi = q.row(i);
			dqi = dq.row(i);
			ddqi = ddq.row(i);
			vE = model(qi,dqi);
			aE = model(qi,dqi,ddqi);
			ret(i,0) = vE(0);	ret(i,1) = vE(1);	ret(i,2) = vE(2);
			ret(i,3) = aE(0);	ret(i,4) = aE(1);	ret(i,5) = aE(2);
		}
		return ret;
	}
	//
	template <class Model>
	matrixXd inverseDynamics(Model model, matrixXd q, matrixXd dq, matrixXd ddq)
	{
		int nrow = q.rows(), ncol = q.cols();
		matrixXd ret(nrow, ncol); ret.fill(0.0);
		vectorXd	qi, dqi, ddqi, tau;
		for (int i=0; i < nrow; i++)
		{
			qi = q.row(i);
			dqi = dq.row(i);
			ddqi = ddq.row(i);
			tau = model(qi, dqi, ddqi);
			for (int j= 0; j < ncol; j++)
				ret(i,j)=tau(j);
		}
		return ret;
	}
	//
	template <class Model>
	matrixXd inverseKinematics(Model model, matrixXd End_position, vectorXd q0, int typeOfMethod = 4, int maxIter=20, double eps=1e-6, int getF=0)
	{
		int nrow = End_position.rows(), ncol = NC::length(q0);
		double norm = 1e-4;
		matrixXd q(nrow, ncol);
		vectorXd qi, posi, qo=q0, delta;
		for (int i = 0;i < nrow; i++)
		{
			posi = End_position.row(i);
			switch (typeOfMethod)
			{
			default:
			case 0:
				{
					qi = NC::fsolve(model, model, qo, posi, maxIter, eps);
					delta = model(qi) - posi;
					if (delta.norm() > norm)
						return matrixXd(0,0);
					break;
				}
			case 1:
				{
					qi = NC::fsolve1(model, model, qo, posi, maxIter, eps);
					model(qi, delta);
					delta = delta - posi;
					if (delta.norm() > norm)
						return matrixXd(0,0);
					break;
				}
			case 2:
				{
					qi = NC::fsolve2(model, model, qo, posi, maxIter, eps);
					model(qi, delta);
					delta = delta - posi;
					if (delta.norm() > norm)
						return matrixXd(0,0);
					break;
				}
			case 3:
				{
					qi = NC::fsolve3(model, model, qo, posi, maxIter, eps);
					delta = model(qi) - posi;
					if (delta.norm() > norm)
						return matrixXd(0,0);
					break;
				}
			case 4:
				{
					qi = NC::fsolve4(model, model, qo, posi, maxIter, eps, getF);
					delta = model(qi, getF) - posi;
					if (delta.norm() > norm)
					{
						std::ofstream f("error.txt", std::ios::app);
						f<<"qi = "<<qi.transpose()<<" \nposi = "<<posi.transpose()<<"\n";
						f<<" q0 = "<<qo.transpose()<<"\n";
						f << delta << "\n norm = "<< delta.norm() <<" at i = "<<i<<"\n";
						f<<"\n posi = " << posi.transpose()<<"\n";
						f.close();
						return matrixXd(0,0);
					}
					break;
				}
			}
			for (int j = 0; j < ncol; j++)
			{
				q(i,j) = qi(j);
				qo(j) = qi(j);
			}
		}
		return q;
	}
	template<class Model>
	matrixXd inverseKinematics(Model model, matrixXd q, matrixXd vE, matrixXd aE, int typeOfMethod = 4)
	{
		int nrow = q.rows(), ncol = q.cols();
		matrixXd jacobian, diff_jacobian;
		vectorXd qi, vEi, aEi, dqi, ddqi;
		matrixXd ret(nrow, ncol+ncol);
		for (int i=0;i<nrow; i++)
		{
			qi = q.row(i);
			vEi = vE.row(i);
			aEi = aE.row(i);
			switch (typeOfMethod)
			{
			default:
			case 0:
			case 2:
			case 4:
				{
					jacobian = model(qi);
					break;
				}
			case 1:
			case 3:
				{
					model(qi, jacobian);
					break;
				}
			}
			if (jacobian.rows() == jacobian.cols())
			{
				dqi = jacobian.colPivHouseholderQr().solve(vEi);
				diff_jacobian = model(qi, dqi);
				ddqi = jacobian.colPivHouseholderQr().solve(aEi - diff_jacobian * dqi);
			}
			else
			{
				matrixXd Jplus = (jacobian * jacobian.transpose()).colPivHouseholderQr().solve(jacobian);
				Jplus.transposeInPlace();
				dqi = Jplus * vEi;
				diff_jacobian = model(qi,dqi);
				ddqi = Jplus * (aE - diff_jacobian * dqi);
			}
			for (int j=0;j<ncol;j++)
			{
				ret(i,j)=dqi(j);
				ret(i,j+ncol)=ddqi(j);
			}
		}
		return ret;
	}
	//
	template <class Model>
	matrixXd ForwardDynamics(Model model, matrixXd Tau, vectorXd tspan, vectorXd q0, vectorXd dq0)
	{
		matrixXd ret = NC::ode_45(model, model, Tau, tspan, q0, dq0, true);
		return ret;
	}
	//
	template <class DataBlock, class Initial>
	bool ReadDataFromFile(char *filename, DataBlock &dataBlock, Initial &initial, std::string &msg)
	{
		std::ifstream file(filename, std::ios::in);
		if (!file)			// can not open the file
		{msg = "can't open the file"; return false;}
		int nrow, ncol, nsize = -1;
		if (file.eof())		// empty file
		{msg = "empty file"; file.close(); return false;}
		file >> nrow;		// read number of rows
		if (file.fail() || nrow < 0)
		{msg = "the number of rows must be a positive integer"; file.close(); return false;}
		file >> ncol;		// read numer of columns
		if (file.fail() || ncol < 0)
		{msg = "the number of columns must be a positive integer";file.close(); return false;}
		file >> nsize;		// read size of vector initial
		if (file.fail())
		{msg = "the size of initial vector must be a integer";file.close(); return false;}
		// read data
		dataBlock = DataBlock(nrow, ncol);
		for (int i=0; i<nrow; i++)
		{
			for (int j=0;j<ncol;j++)
			{
				if (file.eof())		// empty file
				{msg = "read dataBlock false"; dataBlock = DataBlock(0,0);file.close(); return false;}
				double data;
				file >> data;		// read a double number
				if (file.fail())
				{msg = "read dataBlock false"; dataBlock = DataBlock(0,0);file.close(); return false;}
				dataBlock(i,j) = data;
			}
		}
		// dieu kien dau
		if (nsize > 0)
		{	
			initial = Initial(nsize);
			for (int i=0;i<nsize;i++)
			{
				if (file.eof())		// empty file
				{msg = "read initialData false"; initial=Initial(0);file.close();return false;}
				double data;
				file >> data;
				if (file.fail())
				{msg = "read initialData false"; initial=Initial(0);file.close();return false;}
				initial(i)=data;
			}
		}
		file.close();
		return true;
	}
	//
}
#endif //ROBOT_HEADER_H