/*
	(c) by Le Minh Nghia - KSTN CDT k54 - HUST - 23/9/2013
	Giai hpt, tinh dinh thuc va ma tran nghich dao bang pp so
	ODE
*/

#ifndef NUMERIC_COMPUTING_DEFINE_H
#define NUMERIC_COMPUTING_DEFINE_H

#include <Eigen/Dense>
#include "constants.h"


namespace NC
{
	typedef Eigen::MatrixXd	Matrix;
	typedef Eigen::VectorXd	Vector;
	template <class VectorMatrix>
	int length(VectorMatrix v)
	{
		int r = v.rows(), c = v.cols();
		return ((r>c)?r:c);
	}
	template <class Function>
	double fsolve(
		Function	eqn,
		Function deqn,
		double x0,
		int maxIter = 50, 
		double eps = 1e-6)		// giai pt eqn(x) = 0, bang pp lap Newton
	{
		double x_k = x0, f_k, df_k;
		for (int i = 0; i < maxIter; i++)
		{
			f_k = eqn(x_k);
			df_k = deqn(x_k);
			if (abs(f_k) < eps)
				return x_k;
			if (df_k == 0.0)
				return x_k;
			x_k -= f_k / df_k;
		}
		return x_k;
	}
	//
	template <class Function>
	double fsolve(
		Function eqn,
		Function deqn,
		double x0, 
		double fE, 
		int maxIter = 20, 
		double eps = 1e-6)		// giai pt eqn(x) = fE, bang pp lap Newton
	{
		double x_k = x0, f_k, df_k;
		for (int i = 0; i < maxIter; i++)
		{
			f_k = eqn(x_k) - fE;
			df_k = deqn(x_k);
			if (abs(f_k) < eps)
				return x_k;
			if (df_k == 0.0)
				return x_k;
			x_k -= f_k / df_k;
		}
		return x_k;
	}
	//
	template <class System, class DSystem>
	Vector fsolve(
		System eqns,
		DSystem deqns,
		Vector x0, 
		int maxIter = 20, 
		double eps = 1e-6)
	{
		int dim = length(x0);		// so phuong trinh
		double det;					// dinh thuc cua ma tran
		Vector 
			x_k(x0), 
			x_temp(x0);
		Vector f_k(dim);
		Matrix df_k(dim, dim);
		for (int i = 0; i < maxIter; i++)
		{
			f_k = eqns(x_k);
			df_k = deqns(x_k);
			if (f_k.norm() < eps)
				return x_k;
			if (df_k.rows() == df_k.cols()) // matran vuong
				x_temp = df_k.colPivHouseholderQr().solve(f_k);
			else
			{
				// J+ = (J^T) * (J * (J^T))^(-1)
				Matrix Jplus = (df_k * df_k.transpose()).colPivHouseholderQr().solve(df_k);
				Jplus.transposeInPlace();
				x_temp = Jplus * f_k;
			}
			// kiem tra lai nghiem
			det = (df_k * x_temp - f_k).norm() / f_k.norm();
			if (det > eps)
				return x_k;										// thoat vi det = 0
			x_k = x_k - x_temp;
		}
		return x_k;
	}
	//
	template <class System, class DSystem>
	Vector fsolve(
		System eqns,
		DSystem deqns,
		Vector x0,  
		Vector fE, 
		int maxIter = 20, 
		double eps = 1e-6)
	{
		int dim = length(x0);
		double det;	
		Vector x_k(x0),
			x_temp(x0);
		Vector f_k(dim);
		Matrix df_k(dim, dim),
			inv_df_k(dim, dim);
		for (int i = 0; i < maxIter; i++)
		{
			f_k = eqns(x_k) - fE;
			df_k = deqns(x_k);
			if (f_k.norm() < eps)
				return x_k;
			if (df_k.rows() == df_k.cols())
				x_temp = df_k.colPivHouseholderQr().solve(f_k);
			else
			{
				// J+ = (J^T) * (J * (J^T))^(-1)
				Matrix Jplus = (df_k * df_k.transpose()).colPivHouseholderQr().solve(df_k);
				Jplus.transposeInPlace();
				x_temp = Jplus * f_k;
			}
			det = (df_k * x_temp - f_k).norm() / f_k.norm();
			if (det > eps)
				return x_k;	
			x_k = x_k - x_temp;
		}
		return x_k;	
	}
	//
	template <class Function>
	double fsolve1(
		Function	eqn,
		Function deqn,
		double x0,
		int maxIter = 50, 
		double eps = 1e-6)		// giai pt eqn(x) = 0, bang pp lap Newton
	{
		double x_k = x0, f_k, df_k;
		for (int i = 0; i < maxIter; i++)
		{
			//f_k = eqn(x_k);	df_k = deqn(x_k);
			eqn(x_k, f_k);	deqn(x_k, df_k);
			if (abs(f_k) < eps)
				return x_k;
			if (df_k == 0.0)
				return x_k;
			x_k -= f_k / df_k;
		}
		return x_k;
	}
	//
	template <class Function>
	double fsolve1(
		Function eqn,				// void (*eqn)(double, double&)
		Function deqn,				// void (*deqn)(double, double&)
		double x0, 
		double fE, 
		int maxIter = 20, 
		double eps = 1e-6)		// giai pt eqn(x) = fE, bang pp lap Newton
	{
		double x_k = x0, f_k, df_k;
		for (int i = 0; i < maxIter; i++)
		{
			//f_k = eqn(x_k) - fE;	df_k = deqn(x_k);
			eqn(x_k, f_k); f_k = f_k - fE;
			deqn(x_k, df_k);
			if (abs(f_k) < eps)
				return x_k;		// nghiem cua phuong trinh
			if (df_k == 0.0)
				return x_k;		// diem ki di, nghiem khong tin tuong
			x_k -= f_k / df_k;
		}
		return x_k;
	}
	//
	template <class System, class DSystem>
	Vector fsolve1(
		System eqns,				// void (*eqns)(Vector, Vector&)
		DSystem deqns,				// void (*deqns)(Vector, Matrix&)
		Vector x0, 
		int maxIter = 20, 
		double eps = 1e-6)
	{
		int dim = length(x0);		// so phuong trinh
		double det;					// dinh thuc cua ma tran
		Vector 
			x_k(x0), 
			x_temp(x0);
		Vector f_k(dim);
		Matrix df_k(dim, dim);
		for (int i = 0; i < maxIter; i++)
		{
			///f_k = eqns(x_k) - fE;	df_k = deqns(x_k);
			eqns(x_k, f_k); f_k = f_k - fE;
			deqns(x_k, df_k);
			if (f_k.norm() < eps)
				return x_k;
			if (df_k.rows() == df_k.cols()) // matran vuong
				x_temp = df_k.colPivHouseholderQr().solve(f_k);
			else
			{
				// J+ = (J^T) * (J * (J^T))^(-1)
				Matrix Jplus = (df_k * df_k.transpose()).colPivHouseholderQr().solve(df_k);
				Jplus.transposeInPlace();
				x_temp = Jplus * f_k;
			}
			// kiem tra lai nghiem
			det = (df_k * x_temp - f_k).norm() / f_k.norm();
			if (det > eps)
				return x_k;										// thoat vi det = 0
			x_k = x_k - x_temp;
		}
		return x_k;
	}
	//
	template <class System, class DSystem>
	Vector fsolve1(
		System eqns,				// void (*eqns)(Vector, Vector&) 
		DSystem deqns,				// void (*deqns)(Vector, Matrix&)
		Vector x0,  
		Vector fE, 
		int maxIter = 20, 
		double eps = 1e-6) // giai hpt eqns(vx) = fE
	{
		int dim = length(x0);		// so phuong trinh
		double det;					// dinh thuc cua ma tran
		Vector x_k(x0),
			x_temp(x0);
		Vector f_k(dim);
		Matrix df_k(dim, dim),
			inv_df_k(dim, dim);					// ma tran nghich dao cua ma tran jacobi df_k
		for (int i = 0; i < maxIter; i++)
		{
			eqns(x_k, f_k); f_k = f_k - fE;
			deqns(x_k, df_k);
			if (f_k.norm() < eps)				// tim thay nghiem
				return x_k;
			if (df_k.rows() == df_k.cols()) // matran vuong
				x_temp = df_k.colPivHouseholderQr().solve(f_k);	
			else
			{
				// J+ = (J^T) * (J * (J^T))^(-1)
				Matrix Jplus = (df_k * df_k.transpose()).colPivHouseholderQr().solve(df_k);
				Jplus.transposeInPlace();
				x_temp = Jplus * f_k;
			}
			det = (df_k * x_temp - f_k).norm() / f_k.norm();
			if (det > eps)		// det == 0
				return x_k;						// thoat vi det = 0;
			x_k = x_k - x_temp;
		}
		return x_k;								// nghiem khong tin tuong, i== maxIter
	}
	//
	template <class Function1, class Function2>
	double fsolve2(
		Function1	eqn,				// void (*eqn)(double x, double& fx)
		Function2 deqn,					// double (*deqn)(double x)
		double x0,
		int maxIter = 50, 
		double eps = 1e-6)		// giai pt eqn(x) = 0, bang pp lap Newton
	{
		double x_k = x0, f_k, df_k;
		for (int i = 0; i < maxIter; i++)
		{
			//f_k = eqn(x_k);
			eqn(x_k, f_k);
			df_k = deqn(x_k);
			if (abs(f_k) < eps)
				return x_k;		// nghiem cua phuong trinh
			if (df_k == 0.0)
				return x_k;		// diem ki di, nghiem khong tin tuong
			x_k -= f_k / df_k;
		}
		return x_k;
	}
	//
	template <class Function1, class Function2>
	double fsolve2(
		Function1 eqn,				// void (*eqn)(double x, double &fx);
		Function2 deqn,				// double (*deqn)(double x)
		double x0, 
		double fE, 
		int maxIter = 20, 
		double eps = 1e-6)		// giai pt eqn(x) = fE, bang pp lap Newton
	{
		double x_k = x0, f_k, df_k;
		for (int i = 0; i < maxIter; i++)
		{
			//f_k = eqn(x_k) - fE;
			eqn(x_k, f_k);	f_k = f_k - fE;
			df_k = deqn(x_k);
			if (abs(f_k) < eps)
				return x_k;		// nghiem cua phuong trinh
			if (df_k == 0.0)
				return x_k;		// diem ki di, nghiem khong tin tuong
			x_k -= f_k / df_k;
		}
		return x_k;
	}
	//
	template <class System, class DSystem>
	Vector fsolve2(
		System eqns,				// void (*eqns)(Vector q, Vector &f(q))
		DSystem deqns,				// Matrix (*deqns)(Vector)
		Vector x0, 
		int maxIter = 20, 
		double eps = 1e-6)
	{
		int dim = length(x0);		// so phuong trinh
		double det;					// dinh thuc cua ma tran
		Vector 
			x_k(x0), 
			x_temp(x0);
		Vector f_k(dim);
		Matrix df_k(dim, dim);
		for (int i = 0; i < maxIter; i++)
		{
			//f_k = eqns(x_k);
			eqns(x_k, f_k);
			df_k = deqns(x_k);
			if (f_k.norm() < eps)
				return x_k;
			if (df_k.rows() == df_k.cols()) // matran vuong
				x_temp = df_k.colPivHouseholderQr().solve(f_k);	
			else
			{
				// J+ = (J^T) * (J * (J^T))^(-1)
				Matrix Jplus = (df_k * df_k.transpose()).colPivHouseholderQr().solve(df_k);
				Jplus.transposeInPlace();
				x_temp = Jplus * f_k;
			}
			// kiem tra lai nghiem
			det = (df_k * x_temp - f_k).norm() / f_k.norm();
			if (det > eps)
				return x_k;										// thoat vi det = 0
			x_k = x_k - x_temp;
		}
		return x_k;
	}
	//
	template <class System, class DSystem>
	Vector fsolve2(		
		System eqns,				// void (*eqns) (Vector, Vector)
		DSystem deqns,				// Matrix (*deqns) (Vector)
		Vector x0,  
		Vector fE, 
		int maxIter = 20, 
		double eps = 1e-6) // giai hpt eqns(vx) = fE
	{
		int dim = length(x0);		// so phuong trinh
		double det;					// dinh thuc cua ma tran
		Vector x_k(x0),
			x_temp(x0);
		Vector f_k(dim);
		Matrix df_k(dim, dim),
			inv_df_k(dim, dim);					// ma tran nghich dao cua ma tran jacobi df_k
		for (int i = 0; i < maxIter; i++)
		{
			//f_k = eqns(x_k) - fE;
			eqns(x_k, f_k); f_k = f_k - fE;
			df_k = deqns(x_k);
			if (f_k.norm() < eps)				// tim thay nghiem
				return x_k;
			if (df_k.rows() == df_k.cols()) // matran vuong
				x_temp = df_k.colPivHouseholderQr().solve(f_k);
			else
			{
				// J+ = (J^T) * (J * (J^T))^(-1)
				Matrix Jplus = (df_k * df_k.transpose()).colPivHouseholderQr().solve(df_k);
				Jplus.transposeInPlace();
				x_temp = Jplus * f_k;
			}
			det = (df_k * x_temp - f_k).norm() / f_k.norm();
			if (det > eps)		// det == 0
				return x_k;						// thoat vi det = 0;
			x_k = x_k - x_temp;
		}
		return x_k;								// nghiem khong tin tuong, i== maxIter
	}
	//
	template <class Function1, class Function2>
	double fsolve3(
		Function1	eqn,				// double (*eqn)(double x)
		Function2 deqn,					// void (*deqn)(double x,, double& dfx)
		double x0,
		int maxIter = 50, 
		double eps = 1e-6)		// giai pt eqn(x) = 0, bang pp lap Newton
	{
		double x_k = x0, f_k, df_k;
		for (int i = 0; i < maxIter; i++)
		{
			f_k = eqn(x_k);
			//df_k = deqn(x_k);
			deqn(x_k, df_k);
			if (abs(f_k) < eps)
				return x_k;		// nghiem cua phuong trinh
			if (df_k == 0.0)
				return x_k;		// diem ki di, nghiem khong tin tuong
			x_k -= f_k / df_k;
		}
		return x_k;
	}
	
	template <class Function1, class Function2>
	double fsolve3(
		Function1 eqn,				// double (*eqn)(double x)
		Function2 deqn,				// void (*deqn)(double x, double& dfx)
		double x0, 
		double fE, 
		int maxIter = 20, 
		double eps = 1e-6)		// giai pt eqn(x) = fE, bang pp lap Newton
	{
		double x_k = x0, f_k, df_k;
		for (int i = 0; i < maxIter; i++)
		{
			f_k = eqn(x_k) - fE;
			//df_k = deqn(x_k);
			deqn(x_k, df_k);
			if (abs(f_k) < eps)
				return x_k;		// nghiem cua phuong trinh
			if (df_k == 0.0)
				return x_k;		// diem ki di, nghiem khong tin tuong
			x_k -= f_k / df_k;
		}
		return x_k;
	}
	//
	template <class System, class DSystem>
	Vector fsolve3(
		System eqns,				// Vector (*eqns)(Vector q)
		DSystem deqns,				// void (*deqns)(Vector, Matrix&)
		Vector x0, 
		int maxIter = 20, 
		double eps = 1e-6)
	{
		int dim = length(x0);		// so phuong trinh
		double det;					// dinh thuc cua ma tran
		Vector 
			x_k(x0), 
			x_temp(x0);
		Vector f_k(dim);
		Matrix df_k(dim, dim);
		for (int i = 0; i < maxIter; i++)
		{
			f_k = eqns(x_k);
			//df_k = deqns(x_k);
			deqns(x_k, df_k);
			if (f_k.norm() < eps)
				return x_k;
			if (df_k.rows() == df_k.cols()) // matran vuong
				x_temp = df_k.colPivHouseholderQr().solve(f_k);
			else
			{
				// J+ = (J^T) * (J * (J^T))^(-1)
				Matrix Jplus = (df_k * df_k.transpose()).colPivHouseholderQr().solve(df_k);
				Jplus.transposeInPlace();
				x_temp = Jplus * f_k;
			}
			// kiem tra lai nghiem
			det = (df_k * x_temp - f_k).norm() / f_k.norm();
			if (det > eps)
				return x_k;										// thoat vi det = 0
			x_k = x_k - x_temp;
		}
		return x_k;
	}
	//
	template <class System, class DSystem>
	Vector fsolve3(		
		System eqns,				// Vector (*eqns)(Vector q)
		DSystem deqns,				// void (*deqns)(Vector, Matrix&)
		Vector x0,  
		Vector fE, 
		int maxIter = 20, 
		double eps = 1e-6) // giai hpt eqns(vx) = fE
	{
		int dim = length(x0);		// so phuong trinh
		double det;					// dinh thuc cua ma tran
		Vector x_k(x0),
			x_temp(x0);
		Vector f_k(dim);
		Matrix df_k(dim, dim),
			inv_df_k(dim, dim);					// ma tran nghich dao cua ma tran jacobi df_k
		for (int i = 0; i < maxIter; i++)
		{
			f_k = eqns(x_k) - fE;
			//df_k = deqns(x_k);
			deqns(x_k, df_k);
			if (f_k.norm() < eps)				// tim thay nghiem
				return x_k;
			if (df_k.rows() == df_k.cols()) // matran vuong
				x_temp = df_k.colPivHouseholderQr().solve(f_k);
			else
			{
				// J+ = (J^T) * (J * (J^T))^(-1)
				
				Matrix Jplus = (df_k * df_k.transpose()).colPivHouseholderQr().solve(df_k);
				Jplus.transposeInPlace();
				x_temp = Jplus * f_k;
			}
			det = (df_k * x_temp - f_k).norm() / f_k.norm();
			if (det > eps)		// det == 0
				return x_k;						// thoat vi det = 0;
			x_k = x_k - x_temp;
		}
		return x_k;								// nghiem khong tin tuong, i== maxIter
	}
	//
	template <class System, class DSystem>
	Vector fsolve4(
		System eqns,				// Vector (*eqns)(Vector, int)
		DSystem deqns,				// Matrix (*deqns)(Vector)
		Vector x0, 
		int maxIter = 20, 
		double eps = 1e-6, int getF = 0)
	{
		int dim = length(x0);		// so phuong trinh
		double det;					// dinh thuc cua ma tran
		Vector 
			x_k(x0), 
			x_temp(x0);
		Vector f_k(dim);
		Matrix df_k(dim, dim);
		for (int i = 0; i < maxIter; i++)
		{
			f_k = eqns(x_k, getF);
			df_k = deqns(x_k);
			if (f_k.norm() < eps)
				return x_k;
			if (df_k.rows() == df_k.cols()) // matran vuong
				x_temp = df_k.colPivHouseholderQr().solve(f_k);	
			else
			{
				// J+ = (J^T) * (J * (J^T))^(-1)
				Matrix Jplus = (df_k * df_k.transpose()).colPivHouseholderQr().solve(df_k);
				Jplus.transposeInPlace();
				x_temp = Jplus * f_k;
			}
			// kiem tra lai nghiem
			det = (df_k * x_temp - f_k).norm() / f_k.norm();
			if (det > eps)
				return x_k;										// thoat vi det = 0
			x_k = x_k - x_temp;
		}
		return x_k;
	}
	//
	template <class System, class DSystem>
	Vector fsolve4(
		System eqns,				// Vector (*eqns)(Vector, int) 
		DSystem deqns,				// Matrix (*deqns)(Vector)
		Vector x0,  
		Vector fE, 
		int maxIter = 20, 
		double eps = 1e-6, int getF=0) // giai hpt eqns(vx) = fE
	{
		int dim = length(x0);		// so phuong trinh
		double det;					// dinh thuc cua ma tran
		Vector x_k(x0),
			x_temp(x0);
		Vector f_k(dim);
		Matrix df_k(dim, dim),
			inv_df_k(dim, dim);					// ma tran nghich dao cua ma tran jacobi df_k
		for (int i = 0; i < maxIter; i++)
		{
			f_k = eqns(x_k, getF) - fE;
			df_k = deqns(x_k);
			if (f_k.norm() < eps)				// tim thay nghiem
			{
				return x_k;
			}
			if (df_k.rows() == df_k.cols()) // matran vuong
				x_temp = df_k.colPivHouseholderQr().solve(f_k);
			else 
			{
				// J+ = (J^T) * (J * (J^T))^(-1)
				Matrix Jplus = (df_k * df_k.transpose()).colPivHouseholderQr().solve(df_k);
				Jplus.transposeInPlace();
				x_temp = Jplus * f_k;
			}
			det = (df_k * x_temp - f_k).norm() / f_k.norm();
			if (det > eps)						// det == 0
			{
				return x_k;						// thoat vi det = 0;
			}
			x_k = x_k - x_temp;
		}
		return x_k;								// nghiem khong tin tuong, i== maxIter
	}
	//
	//
	// y' = f(t,y)
	template <class Function>
	Matrix	ode_45(
		Function	fty,			// Vector (*fty)(double t, Vector y), 
		Vector	tspan, 
		Vector	y0) 
	{
		int tsize = length(tspan);
		assert (tsize >= 3);
		Vector tspan0;
		double t0, h, tf;
		if (tsize > 3)
		{
			tspan0 = tspan;
			t0 = tspan(0); tf = tspan(tsize - 1); h = tspan(1) - tspan(0);
		}
		else
		{
			t0 = tspan(0); tf = tspan(2); h = tspan(1);
			int N = (int)((tf-t0)/h);
			tspan0 = Vector(N+1);
			for (int i = 0; i < N; i++)
				tspan0(i) = t0 + i*h;
			tspan0(N) = tf;
		}
		Vector yi(y0);
		Vector k1, k2, k3, k4;
		int row = length(tspan0);
		int col = length(y0) + 1;
		Matrix result(row, col);
		result(0,0) = tspan0(0);
		for (int j = 1; j < col; j++)
			result(0,j) = yi(j-1);
		for (int i = 0; i < row - 1; i++)
		{
			k1 = fty(tspan0(i), yi);
			k2 = fty(tspan0(i) + h/2, yi + k1*h/2);
			k3 = fty(tspan0(i) + h/2, yi + k2*h/2);
			k4 = fty(tspan0(i) + h, yi + k3*h);
			yi = yi + (k1 + k2*2 + k3*2 + k4)*h/6;
			result(i+1,0) = tspan0(i+1);
			for (int j = 1; j < col; j++)
				result(i+1,j) = yi(j-1);
		}
		return result;
	}
	//
	// y' = g(y) + h(t)
	template <class Function1, class Function2>
	Matrix	ode_45(		
		Function1	gy,				// Vector (*gy)(Vector y), 
		Function2	ht,				// Vector (*ht)(double t), 
		Vector	tspan, 
		Vector	y0)
	{
		int tsize = length(tspan);
		assert (tsize > 3);
		double t0, h, tf;
		Vector tspan0;
		if (tsize > 3)
		{
			t0 = tspan(0); h = tspan(1) - tspan(0); tf = tspan(tsize - 1);
			tspan0 = tspan;
		}
		else
		{
			t0 = tspan(0); h = tspan(1); tf = tspan(2);
			int N = (int)((tf - t0) / h);
			tspan0 = Vector(N+1);
			for (int i = 0; i < N; i++)
				tspan0(i) = t0 + i * h;
			tspan0(N) = tf;
		}
		int row = length(tspan0);
		int col = length(y0) + 1;
		Matrix	result(row, col);
		Vector yi(y0);
		Vector k1, k2, k3, k4;
		double ti = 0.0;
		//
		result(0,0) = tspan0(0);
		for (int i = 1; i < col; i++)
			result(0,i) = yi(i-1);
		//
		for (int i = 0; i < row-1; i++)
		{
			ti = tspan0(i);
			k1 = gy(yi) + ht(ti);
			k2 = gy(yi + k1*h/2) + ht(ti + h/2);
			k3 = gy(yi + k2*h/2) + ht(ti + h/2);
			k4 = gy(yi + k3*h) + ht(ti + h);
		
			yi = yi + (k1 + k2*2 + k2*2 + k4) * h/ 6;

			result(i+1,0) = ti+h;
			for (int j = 1; j < col; j++)
				result(i+1,j) = yi(j-1);
		}
		return result;
	}
	//
	// y' = f(t,y) = g(y) + h(ti)
	template <class Function>
	Matrix	ode_45(
		Function	gy,				// Vector (*gy)(Vector y), 
		Matrix	ht, 
		Vector	tspan0, 
		Vector	y0) 
	{
		int y0size = length(y0),
			tsize0 = length(tspan0);
		assert(ht.cols() == y0size && tsize0 >= 3);
		double t0, tf, h;
		if (tsize0 > 3)
		{
			t0 = tspan0(0); tf = tspan0(tsize0 - 1); h = tspan0(2) - tspan0(0);
		}
		else
		{
			t0 = tspan0(0); h = tspan0(1); tf = tspan0(2);
		}
		int row = (ht.rows()+1)/2,
			col = ht.cols() + 1;		
		Matrix result(row, col);
		result(0,0) = t0;
		Vector yi(y0), hi;
		Vector k1, k2, k3, k4;

		for (int i = 1; i < col; i++)
			result(0,i) = yi(i-1);
		for (int i = 0; i < row-1; i++)
		{
			hi = (ht.row(2*i)).transpose();
			k1 = gy(yi) + hi;			hi = (ht.row(2*i+1)).transpose();		// ht(ti + h/2);
			k2 = gy(yi + k1*h/2) + hi;	
			k3 = gy(yi + k2*h/2) + hi;	hi = (ht.row(2*i+2)).transpose();		// ht(ti + h)
			k4 = gy(yi + k3*h) + hi;

			yi = yi + (k1 + k2*2 + k2*2 + k4) * h/ 6;

			result(i+1,0) = t0+(i+1)*h;
			for (int j = 1; j < col; j++)
				result(i+1,j) = yi(j-1);
		}
		return result;
	}
	// subs_vector
	template<class VectorMatrix>
	VectorMatrix sub_vector(const VectorMatrix &v_source, int from, int to)
	{
		// assert
		int size = to - from + 1;
		VectorMatrix rs(size);
		for (int i = from; i <= to; i++)
			rs(i - from) = v_source(i);
		return rs;
	}
	// sub_matrix
	template<class VectorMatrix>
	VectorMatrix sub_matrix(const VectorMatrix &m_source, int r_f, int r_t, int c_f, int c_t)
	{
		int row = r_t - r_f + 1,
			col = c_t - c_f + 1;
		VectorMatrix	rs(row, col);
		for (int r = r_f; r <= r_t; r++)
			for (int c = c_f; c <= c_t; c++)
				rs(r-r_f,c-c_f) = m_source(r,c);
		return rs;
	}
	// combine_vector
	template<class VectorMatrix>
	VectorMatrix combine_vector(const VectorMatrix &v1, const VectorMatrix &v2)
	{
		int size1 = length(v1), 
			size2 = length(v2);
		Vector rs(size1 + size2);
		for (int i = 0; i < size1; i++)
			rs(i) = v1(i);
		for (int i = 0; i < size2; i++)
			rs(i+size1) = v2(i);
		return rs;
	}
	//
	//
	// M(q).q" + C(q,q') + Gq(q) = tau(t)
	template <class MTKL, class CORIOLIS, class GQ, class TAU>
	Matrix	ode_45(		
		MTKL		M,			// Matrix (*M)(Vector q),							// ma tran khoi luong
		CORIOLIS	C,			// Vector (*C)(Vector q, Vector dq),				// ma tran coriolis
		GQ			Gq,			// Vector (*Gq)(Vector q),							// ma tran luc co the
		TAU			tau,		// Vector (*tau) (double t),						// ngoai luc
		Vector		tspan,															// khoang thoi gian
		Vector		q0,																// vi tri ban dau
		Vector		dq0,															// van toc ban dau
		bool out_ddq = false)							//
	{
		double t0, h, tf;
		Vector tspan0;
		if (length(tspan) > 3)
		{
			tspan0 = tspan;
			t0 = tspan(0); tf = tspan(tspan.size() - 1); h = tspan(1) - tspan(0);
		}
		else
		{
			t0 = tspan(0); tf = tspan(2); h= tspan(1);
			int N = (int)((tf-t0)/h);
			tspan0 = Vector(N+1);
			for (int i = 0; i < N; i++)
				tspan0(i) = t0 + i*h;
			tspan0(N) = tf;
		}

		int dim = length(q0);
		// out_ddq = true - default
		int col = length(q0) + length(dq0) + dim + 1; // t, q, dq, ddq
		if (!out_ddq)
			col = length(q0) + length(dq0) + 1;			// t, q, dq
		//
		int row = length(tspan0);
		Vector qi(q0), dqi(dq0), ddqi, k1(2*dim), k2(2*dim), k3(2*dim), k4(2*dim), taui, Vtmp, tmp, Gqi;
		Matrix result(row, col), Mtmp;
		tmp = combine_vector(qi, dqi);
		result(0,0) = tspan0(0);					// yi
		for (int i = 1; i < 2*dim + 1; i++)
			result(0,i) = tmp(i-1);					// q, dq
		//
		for (int i = 0; i < row - 1; i++)
		{
			// ti
			Mtmp = M(qi);
			Vtmp = C(qi, dqi);
			Gqi = Gq(qi);
			taui = tau(i*h);
			ddqi = Mtmp.colPivHouseholderQr().solve(taui - Gqi - Vtmp);
			// out-ddq
			if (out_ddq)
			{
				for (int j = 2*dim+1; j < col; j++)			// ddq
					result(i,j) = ddqi(j - 2 * dim - 1);
			}
			//
			k1 = combine_vector(dqi, ddqi);
			// ti + h / 2
			qi = sub_vector<Vector>(tmp + k1 * h / 2, 0, dim - 1);
			dqi = sub_vector<Vector>(tmp + k1 * h / 2, dim, 2 * dim - 1);
			Mtmp = M(qi);
			Vtmp = C(qi, dqi);
			Gqi = Gq(qi);
			taui = tau(i*h + h/2);
			ddqi = Mtmp.colPivHouseholderQr().solve(taui - Gqi - Vtmp);
			k2 = combine_vector(dqi, ddqi);
			// 
			qi = sub_vector<Vector>(tmp + k2 * h / 2, 0, dim - 1);
			dqi = sub_vector<Vector>(tmp + k2 * h / 2, dim, 2 * dim - 1);
			Mtmp = M(qi);
			Vtmp = C(qi, dqi);
			Gqi = Gq(qi);
			//taui = tau(i*h + h/2);		// not change
			ddqi = Mtmp.colPivHouseholderQr().solve(taui - Gqi - Vtmp);
			k3 = combine_vector(dqi, ddqi);
			// t + h
			qi = sub_vector<Vector>(tmp + k3 * h, 0, dim - 1);
			dqi = sub_vector<Vector>(tmp + k3 * h, dim, 2 * dim - 1);
			Mtmp = M(qi);
			Vtmp = C(qi, dqi);
			Gqi = Gq(qi);
			taui = tau(i*h + h);
			ddqi = Mtmp.colPivHouseholderQr().solve(taui - Gqi - Vtmp);
			k4 = combine_vector(dqi, ddqi);
			// 
			tmp = tmp + (k1 + k2*2 + k3*2 + k4)*h/6;
			qi = sub_vector<Vector>(tmp, 0, dim - 1);
			dqi = sub_vector<Vector>(tmp, dim, 2*dim-1);
			//
			result(i+1, 0) = tspan0(i+1);			// ti
			for (int j = 1; j < 2*dim + 1; j++)		// q, dq
				result(i+1,j) = tmp(j-1);
		}
		if (out_ddq)
		{
			// compute ddq at tf
			Mtmp = M(qi);	Vtmp = C(qi, dqi);	Gqi = Gq(qi);	taui = tau(tf);
			ddqi = Mtmp.colPivHouseholderQr().solve(taui - Gqi - Vtmp);
			for (int j = 2*dim+1; j < col; j++)			// ddq
				result(row - 1,j) = ddqi(j - 2 * dim - 1);
		}
		return result;
	}
	//
	// M(q).q" + C(q,q') + Gq(q) = Tau(ti)
	template <class MTKL, class CORIOLIS, class GQ>
	Matrix	ode_45(		
		MTKL			M,		//Matrix (*M)(Vector q),						// ma tran khoi luong
		CORIOLIS		C,		//Vector (*C)(Vector q, Vector dq),				// ma tran coriolis
		GQ				Gq,		//Vector (*Gq)(Vector q),						// ma tran luc co the
		Matrix			Tau,							// ngoai luc, tai cac thoi diem ti, ti+h/2, ti+h,
		Vector			tspan,							// khoang thoi gian, ti => size(tspan) = size(Tau) or size(tspan)==3
		Vector			q0,								// vi tri ban dau
		Vector			dq0,							// van toc ban dau
		bool out_ddq = false)							//
	{
		assert(length(q0)==length(dq0) && length(q0)==Tau.cols() && length(tspan)>=3 && Tau.rows()%2!=0/*odd*/);
		double t0, h, tf;
		if (length(tspan) > 3)
		{
			t0 = tspan(0); tf = tspan(length(tspan) - 1); h = tspan(2)-tspan(0);
		}
		else
		{
			t0 = tspan(0); tf = tspan(2); h = tspan(1);
		}

		int dim = length(q0);
		// out_ddq = true - default
		int col = length(q0) + length(dq0) + dim + 1;	// t, q, dq, ddq
		if (!out_ddq)
			col = length(q0) + length(dq0) + 1;			// t, q, dq
		//
		int row = (Tau.rows()+1)/2; // 2*row - 1 = Tau.rows()
		Vector qi(q0), dqi(dq0), ddqi, k1(2*dim), k2(2*dim), k3(2*dim), k4(2*dim), taui, Vtmp, tmp, Gqi;
		Matrix result(row, col), Mtmp;
		tmp = combine_vector(qi, dqi);
		result(0,0) = t0;							// yi
		for (int i = 1; i < 2*dim + 1; i++)
			result(0,i) = tmp(i-1);					// q, dq
		//
		for (int i = 0; i < row-1; i++)
		{
			// ti
			Mtmp = M(qi);
			Vtmp = C(qi, dqi);
			Gqi = Gq(qi);
			taui = Tau.row(2*i).transpose();
			ddqi = Mtmp.colPivHouseholderQr().solve(taui - Gqi - Vtmp);
			// out-ddq
			if (out_ddq)
			{
				for (int j = 2*dim+1; j < col; j++)			// ddq
					result(i,j) = ddqi(j - 2 * dim - 1);
			}
			//
			k1 = combine_vector(dqi, ddqi);
			// ti + h / 2
			qi = sub_vector<Vector>(tmp + k1 * h / 2, 0, dim - 1);
			dqi = sub_vector<Vector>(tmp + k1 * h / 2, dim, 2 * dim - 1);
			Mtmp = M(qi);
			Vtmp = C(qi, dqi);
			Gqi = Gq(qi);
			taui = Tau.row(2*i+1).transpose();
			ddqi = Mtmp.colPivHouseholderQr().solve(taui - Gqi - Vtmp);
			k2 = combine_vector(dqi, ddqi);
			// 
			qi = sub_vector<Vector>(tmp + k2 * h / 2, 0, dim - 1);
			dqi = sub_vector<Vector>(tmp + k2 * h / 2, dim, 2 * dim - 1);
			Mtmp = M(qi);
			Vtmp = C(qi, dqi);
			Gqi = Gq(qi);
			//taui = Tau.row(2*i+1).transpose();		// not change
			ddqi = Mtmp.colPivHouseholderQr().solve(taui - Gqi - Vtmp);
			k3 = combine_vector(dqi, ddqi);
			// t + h
			qi = sub_vector<Vector>(tmp + k3 * h, 0, dim - 1);
			dqi = sub_vector<Vector>(tmp + k3 * h, dim, 2 * dim - 1);
			Mtmp = M(qi);
			Vtmp = C(qi, dqi);
			Gqi = Gq(qi);
			taui = Tau.row(2*i+2).transpose();
			ddqi = Mtmp.colPivHouseholderQr().solve(taui - Gqi - Vtmp);
			k4 = combine_vector(dqi, ddqi);
			// 
			tmp = tmp + (k1 + k2*2 + k3*2 + k4)*h/6;
			qi = sub_vector<Vector>(tmp, 0, dim - 1);
			dqi = sub_vector<Vector>(tmp, dim, 2*dim-1);
			//
			result(i+1,0) = t0+(i+1)*h;				// t(i+1)
			for (int j = 1; j < 2*dim + 1; j++)		// q, dq
				result(i+1,j) = tmp(j-1);
		}
		if (out_ddq)
		{
			// compute ddq at tf
			Mtmp = M(qi);	Vtmp = C(qi, dqi);	Gqi = Gq(qi);	taui = Tau.row(Tau.rows()-1).transpose();
			ddqi = Mtmp.colPivHouseholderQr().solve(taui - Gqi - Vtmp);
			for (int j = 2*dim+1; j < col; j++)			// ddq
				result(row - 1,j) = ddqi(j - 2 * dim - 1);
		}
		return result;
	}
    //
	// M(q).q" + V(q,q') = tau(t)
	template <class MTKL, class CG, class TAU>
	Matrix	ode_45(		
		MTKL	M,				//Matrix (*M)(Vector q),						// ma tran khoi luong
		CG		V,				//Vector (*V)(Vector q, Vector dq),				// ma tran coriolis + luc co the
		TAU		tau,			//Vector (*tau) (double t),						// ngoai luc
		Vector tspan,									// khoang thoi gian
		Vector q0,										// vi tri ban dau
		Vector dq0,										// van toc ban dau
		bool out_ddq = false)							//
	{
		double t0, h, tf;
		Vector tspan0;
		if (length(tspan) > 3)
		{
			tspan0 = tspan;
			t0 = tspan(0); tf = tspan(tspan.size() - 1); h = tspan(1) - tspan(0);
		}
		else
		{
			t0 = tspan(0); tf = tspan(2); h= tspan(1);
			int N = (int)((tf-t0)/h);
			tspan0 = Vector(N+1);
			for (int i = 0; i < N; i++)
				tspan0(i) = t0 + i*h;
			tspan0(N) = tf;
		}

		int dim = length(q0);
		// out_ddq = true - default
		int col = length(q0) + length(dq0) + dim + 1; // t, q, dq, ddq
		if (!out_ddq)
			col = length(q0) + length(dq0) + 1;			// t, q, dq
		//
		int row = length(tspan0);
		Vector qi(q0), dqi(dq0), ddqi, k1(2*dim), k2(2*dim), k3(2*dim), k4(2*dim), taui, Vtmp, tmp;
		Matrix result(row, col), Mtmp;
		tmp = combine_vector(qi, dqi);
		result(0,0) = tspan0(0);					// yi
		for (int i = 1; i < 2*dim + 1; i++)
			result(0,i) = tmp(i-1);					// q, dq
		//
		for (int i = 0; i < row - 1; i++)
		{
			// ti
			Mtmp = M(qi);
			Vtmp = V(qi, dqi);
			taui = tau(i*h);
			ddqi = Mtmp.colPivHouseholderQr().solve(taui - Vtmp);
			// out-ddq
			if (out_ddq)
			{
				for (int j = 2*dim+1; j < col; j++)			// ddq
					result(i,j) = ddqi(j - 2 * dim - 1);
			}
			//
			k1 = combine_vector(dqi, ddqi);
			// ti + h / 2
			qi = sub_vector<Vector>(tmp + k1 * h / 2, 0, dim - 1);
			dqi = sub_vector<Vector>(tmp + k1 * h / 2, dim, 2 * dim - 1);
			Mtmp = M(qi);
			Vtmp = V(qi, dqi);
			taui = tau(i*h + h/2);
			ddqi = Mtmp.colPivHouseholderQr().solve(taui - Vtmp);
			k2 = combine_vector(dqi, ddqi);
			// 
			qi = sub_vector<Vector>(tmp + k2 * h / 2, 0, dim - 1);
			dqi = sub_vector<Vector>(tmp + k2 * h / 2, dim, 2 * dim - 1);
			Mtmp = M(qi);
			Vtmp = V(qi, dqi);
			//taui = tau(i*h + h/2);		// not change
			ddqi = Mtmp.colPivHouseholderQr().solve(taui - Vtmp);
			k3 = combine_vector(dqi, ddqi);
			// t + h
			qi = sub_vector<Vector>(tmp + k3 * h, 0, dim - 1);
			dqi = sub_vector<Vector>(tmp + k3 * h, dim, 2 * dim - 1);
			Mtmp = M(qi);
			Vtmp = V(qi, dqi);
			taui = tau(i*h + h);
			ddqi = Mtmp.colPivHouseholderQr().solve(taui - Vtmp);
			k4 = combine_vector(dqi, ddqi);
			// 
			tmp = tmp + (k1 + k2*2 + k3*2 + k4)*h/6;
			qi = sub_vector<Vector>(tmp, 0, dim - 1);
			dqi = sub_vector<Vector>(tmp, dim, 2*dim-1);
			//
			result(i+1, 0) = tspan0(i+1);			// ti
			for (int j = 1; j < 2*dim + 1; j++)		// q, dq
				result(i+1,j) = tmp(j-1);
		}
		if (out_ddq)
		{
			// compute ddq at tf
			Mtmp = M(qi);	Vtmp = V(qi, dqi);	taui = tau(tf);
			ddqi = Mtmp.colPivHouseholderQr().solve(taui - Vtmp);
			for (int j = 2*dim+1; j < col; j++)			// ddq
				result(row - 1,j) = ddqi(j - 2 * dim - 1);
		}
		return result;
	}
	//
	// M(q).q" + V(q,q') = Tau(ti);
	template <class MTKL, class CG>
	Matrix	ode_45(		
		MTKL	M,				//Matrix (*M)(Vector q),						// ma tran khoi luong
		CG		V,				//Vector (*V)(Vector q, Vector dq),				// ma tran coriolis + Gq
		Matrix Tau,										// ngoai luc
		Vector tspan,									// khoang thoi gian
		Vector q0,										// vi tri ban dau
		Vector dq0,										// van toc ban dau
		bool out_ddq = false)							//
	{
		assert(length(q0) == length (dq0) && length(q0) == Tau.cols() && length(tspan) >= 3 && Tau.rows()%2!=0/*odd*/);
		double t0, h, tf;
		if (length(tspan) > 3)
		{
			t0 = tspan(0); tf = tspan(length(tspan) - 1); h = tspan(2) - tspan(0);
		}
		else
		{
			t0 = tspan(0); tf = tspan(2); h= tspan(1);
		}
		// assert(2*length(tspan0) - 1 == Taus.rows());
		int dim = length(q0);
		// out_ddq = true - default
		int col = length(q0) + length(dq0) + dim + 1; // t, q, dq, ddq
		if (!out_ddq)
			col = length(q0) + length(dq0) + 1;			// t, q, dq
		//
		int	row = (Tau.rows()+1)/2;
		Vector qi(q0), dqi(dq0), ddqi, k1(2*dim), k2(2*dim), k3(2*dim), k4(2*dim), taui, Vtmp, tmp;
		Matrix result(row, col), Mtmp;
		tmp = combine_vector(qi, dqi);
		result(0,0) = t0;							// yi
		for (int i = 1; i < 2*dim + 1; i++)
			result(0,i) = tmp(i-1);					// q, dq
		//
		for (int i = 0; i < row-1; i++)
		{
			// ti
			Mtmp = M(qi);
			Vtmp = V(qi, dqi);
			taui = Tau.row(2*i).transpose();
			ddqi = Mtmp.colPivHouseholderQr().solve(taui - Vtmp);
			// out-ddq
			if (out_ddq)
			{
				for (int j = 2*dim+1; j < col; j++)			// ddq
					result(i,j) = ddqi(j - 2 * dim - 1);
			}
			//
			k1 = combine_vector(dqi, ddqi);
			// ti + h / 2
			qi = sub_vector<Vector>(tmp + k1 * h / 2, 0, dim - 1);
			dqi = sub_vector<Vector>(tmp + k1 * h / 2, dim, 2 * dim - 1);
			Mtmp = M(qi);
			Vtmp = V(qi, dqi);
			taui = Tau.row(2*i+1).transpose();
			ddqi = Mtmp.colPivHouseholderQr().solve(taui - Vtmp);
			k2 = combine_vector(dqi, ddqi);
			// 
			qi = sub_vector<Vector>(tmp + k2 * h / 2, 0, dim - 1);
			dqi = sub_vector<Vector>(tmp + k2 * h / 2, dim, 2 * dim - 1);
			Mtmp = M(qi);
			Vtmp = V(qi, dqi);
			//taui = Tau.row(2*i+1).transpose();		// not change
			ddqi = Mtmp.colPivHouseholderQr().solve(taui - Vtmp);
			k3 = combine_vector(dqi, ddqi);
			// t + h
			qi = sub_vector<Vector>(tmp + k3 * h, 0, dim - 1);
			dqi = sub_vector<Vector>(tmp + k3 * h, dim, 2 * dim - 1);
			Mtmp = M(qi);
			Vtmp = V(qi, dqi);
			taui = Tau.row(2*i+2).transpose();
			ddqi = Mtmp.colPivHouseholderQr().solve(taui - Vtmp);
			k4 = combine_vector(dqi, ddqi);
			// 
			tmp = tmp + (k1 + k2*2 + k3*2 + k4)*h/6;
			qi = sub_vector<Vector>(tmp, 0, dim - 1);
			dqi = sub_vector<Vector>(tmp, dim, 2*dim-1);
			//
			result(i+1,0) = t0+(i+1)*h;				// ti
			for (int j = 1; j < 2*dim + 1; j++)		// q, dq
				result(i+1,j) = tmp(j-1);
		}
		if (out_ddq)
		{
			// compute ddq at tf
			Mtmp = M(qi);	Vtmp = V(qi, dqi);	taui = Tau.row(Tau.rows()-1).transpose();
			ddqi = Mtmp.colPivHouseholderQr().solve(taui - Vtmp);
			for (int j = 2*dim+1; j < col; j++)			// ddq
				result(row - 1,j) = ddqi(j - 2 * dim - 1);
		}
		return result;
	}
}

#endif