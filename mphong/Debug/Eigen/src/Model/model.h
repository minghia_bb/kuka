/*
	* Modeling
	* Copyright (c) by Le Minh Nghia - KSTN CDT k54 - HUST - Nov 2013
	* <minghiale.m54@gmail.com>
	* Mo hinh chuoi - nhieu chuoi
	* 
*/

#ifndef MODEL_HEADER_H
#define MODEL_HEADER_H
#include "Joint.h"
#include "RigidBody.h"

namespace LMN
{
	class model
	{
	public:
		typedef std::vector<Joint>		vJoints;
		typedef std::vector<RigidBody>	vRigidBodys;
		typedef std::vector<int>		Indexs;
		typedef	std::vector<int>		Chain;			// chua cac vi tri cac khau trong m_RigidBodys
		typedef std::vector<Chain>		Chains;			// so khau
		typedef std::vector<double>		values;
		//
		// methods
	public:
		model() : m_b3D(false){m_Joints.clear(); m_RigidBodys.clear();}
		// add joint
		void AddJoint(const Joint& joint)
		{
			m_Joints.push_back(joint);
		}
		// add link
		void AddLink(const RigidBody& link)
		{
			m_RigidBodys.push_back(link);
		}
		// tim link co ID=LinkID trong danh sach cac Link
		int getLink(int linkID)
		{
			for (int i = 0; i < (int)m_RigidBodys.size(); i++)
				if (m_RigidBodys[i].m_RigidBodyID == linkID)
					return i;
			return -1;
		}
		// tim Joint co ID=JointID trong dsach cac khop
		int getJoint(int jointID)
		{
			for (int i = 0; i < (int)m_Joints.size(); i++)
				if (m_Joints[i].m_JointID == jointID)
					return i;
			return -1;
		}
		// phan tich mo hinh thanh cac vong kin - vong ho
		// build trees - di lan luot tu RBody-Joint_RBody-...
		// dung lai khi khong ton tai joint nao tiep theo
		bool AutoTrees(int &i, Chain& Achain)
		{
			// Neu ton tai khau i trong dsach cac khau
			if (i < (int)m_RigidBodys.size() && i >= 0)
				Achain.push_back(i);
			// het 1 chuoi
			int jointSize = m_RigidBodys[i].m_ref_edJointIDs.size();
			if (jointSize <= 0)
			{
				m_Chains.push_back(Achain);
				//Achain.clear();
				return true;
			}
			for (int k = 0; k < jointSize; k++)
			{
				Chain ichain = Achain;
				int jointID = m_RigidBodys[i].m_ref_edJointIDs[k];
				int joint_pos = getJoint(jointID); // vi tri cua joint trong dsach cac joints
				if (joint_pos < 0)
					return false; // build failed
				int linkSize = m_Joints[joint_pos].m_ref_edLinkIDs.size();
				if (linkSize <= 0) // ket thuc phai la 1 khau chu ko phai 1 khop (joint)
					return false;	// build trees failed
				for (int p = 0; p < linkSize; p++)
				{
					int linkID = m_Joints[joint_pos].m_ref_edLinkIDs[p];
					int linkPos = getLink(linkID);
					AutoTrees(linkPos, ichain);
				}
			}
			return true;
		}
		bool AutoTrees()
		{
			Chain Achain;
			int root = 0;
			return AutoTrees(root, Achain);
		}
		// get dofs
		int getDofs()
		{
			int dofs = 0;
			for (int i = 0 ; i < (int) m_Joints.size(); i++)
				if (m_Joints[i].m_IsActive)
					++dofs;
			return dofs;
		}
		// getTodoSuyRong - tinh toan dua vao cac bien khop
		int getToaDoSuyRong()
		{
			int tdsr = 0;
			for (int i = 0; i < (int) m_Joints.size(); i++)
			{
				bool b1 = ((int)m_Joints[i].m_ref_edLinkIDs.size() > 1);
				if (b1)
					tdsr += m_Joints[i].GetToaDoSuyRong();
				else
				{
					int ith = getLink(m_Joints[i].m_ref_edLinkIDs[0]);
					bool b2 = m_RigidBodys[ith].IsFixedBody();
					if (!b2)
						tdsr += m_Joints[i].GetToaDoSuyRong();
				}
			}
			return tdsr;
		}
		// getRangBuoc = tinh toan tu so vong ki'n = N*l, khong gian, N=6, phang, N=3, l - so vong kin
		// so phuong trinh rang buoc
		int getNumberRangBuoc()
		{
			int l = getNumberChainsClosed();
			if (!m_b3D)
				return (3*l);
			return (6*l);
		}
		// 
		// so btd + so pt rang buoc = so toa do suy rong
		//
		// so chuoi cua mo hinh -  goi ham AutoTrees truoc
		int getNumberChains()
		{
			return ((int)m_Chains.size());
		}
		// so vong kin = so fixedBody - 1
		int getNumberChainsClosed()
		{
			int loopClosed = -1;
			for (int i = 0; i<(int)m_RigidBodys.size(); i++)
				if (m_RigidBodys[i].IsFixedBody())
					++loopClosed;
			return loopClosed;
		}
		// getChainsClosed - tra ve vi tri cua cac vong kin trong m_Chains
		Indexs getChainsClosed()
		{
			Indexs chainClosedIndexs;
			for (int i = 0; i < (int)m_Chains.size(); i++)
			{
				int size = m_Chains[i].size();
				int link0 = m_Chains[i][0],
					link1 = m_Chains[i][size-1];
				if (m_RigidBodys[link0].IsFixedBody() && m_RigidBodys[link1].IsFixedBody())
					chainClosedIndexs.push_back(i);
			}
			return chainClosedIndexs;
		}
		~model() {m_Joints.clear(); m_RigidBodys.clear();}
		//
		bool			m_b3D;			// mo hinh 3D or 2D
	protected:
		public:
		Frame			m_WCS;			// world coordinate system
		vJoints			m_Joints;		// Danh sach cac Joint
		vRigidBodys		m_RigidBodys;	// danh sach cac vat ran
		values			varLinks;		// gia tri cua cac bien khop tai thoi diem i dang xet
	//private:
		Chains			m_Chains;		// cac chuoi~
	};
}

#endif // MODEL_HEADER_H