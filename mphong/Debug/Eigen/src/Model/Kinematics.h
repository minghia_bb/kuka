/*
	* Modeling
	* Copyright (c) by Le Minh Nghia - KSTN CDT k54 - HUST - Nov 2013
	* <minghiale.m54@gmail.com>
	* Giai bai toan dong hoc
	* 
*/

#ifndef KINEMATICS_HEADER_H
#define KINEMATICS_HEADER_H
#include "model.h"
#include <Eigen/Dense>

namespace LMN
{
	using namespace Eigen;
	class Kinematics
	{
	public:
		Matrix4d	MatranThuanNhat(double mttn[4][4])
		{
			Matrix4d mt;
			for (int i= 0; i<4; i++)
				for (int j=0;j<4;j++)
					mt(i,j)=mttn[i][j];
			return mt;
		}
		MatrixXd	ForwardKinematics(model& Model, VectorXd (*q)(double t), double t0, double h, double tf)
		{
			double ti = 0.1;
			VectorXd vtmp = q(ti);
			int dim = vtmp.rows() > vtmp.cols() ? vtmp.rows() : vtmp.cols();
			
			int n = 2; // phang
			if (Model.m_b3D)
				n=3;
			int N = (int)((tf-t0)/h);
			MatrixXd ret(N, n);
			cout << "\n dim = " <<dim<<"\t N = "<<N << "\t n = "<<n <<"\n";
			Matrix4d mttn0, mttn1, mttn2, Tend;
			for (int i = 0; i < N; i++)
			{
				ti = i*h;
				vtmp = q(ti);
				Tend << 1,0,0,0,	0,1,0,0,	0,0,1,0,	0,0,0,1;
				for (int j = 0; j < dim; j++) // j so khop, chay lan luot trong dsach m_chains
				{
					double mtt[4][4];
					int ith = Model.m_Chains[0][j];
					int jointID = Model.m_RigidBodys[ith].m_ref_edJointIDs[0];
					int jth = Model.getJoint(jointID);
					// mttn tu khau i toi khop i
					Model.m_Joints[jth].MatranThuanNhat(mtt);
					mttn0 = MatranThuanNhat(mtt);
					// mttn tai khop i
					Model.m_Joints[jth].MatranChuyen(mtt, vtmp[j], Model.m_Joints[jth].m_JointType);
					mttn1 = MatranThuanNhat(mtt);
					// mttn tu khop i toi khau i+1
					Model.m_RigidBodys[Model.m_Chains[0][j+1]].MatranThuanNhat(mtt);
					mttn2 = MatranThuanNhat(mtt);
					//
					Tend = Tend * mttn0 * mttn1 * mttn2;
					//
					for (int k= 0; k<n; k++)
						ret(i, k) = Tend(k, 3);
				}
			}
			return ret;
		}
	};
}
#endif //KINEMATICS_HEADER_H