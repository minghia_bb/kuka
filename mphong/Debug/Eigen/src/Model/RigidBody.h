/*
	* Modeling
	* Copyright (c) by Le Minh Nghia - KSTN CDT k54 - HUST - Nov 2013
	* <minghiale.m54@gmail.com>
	* Mo hinh hoa VatRan
	* 
*/

#ifndef RIGID_BODY_HEADER_H
#define RIGID_BODY_HEADER_H
#include <vector>
#define INF				10000000			//[[kg]]
#define EPS				1e-8
namespace LMN
{

	enum AnglesType
	{
		CardanAngles,		// x,y,z
		EulerAngles,		// z,x,z
		RollPitchYallAngles, // z,y,x
		RotZ,
		RotX,
		RotY
	};
	// Mo hinh hoa hetoa do
	class Frame
	{
	public:
		// constructor
		Frame() : m_FrameID(0), m_refID(-1) 
		{
			for (int i = 0; i < 3; i++)
			{
				m_GocToaDo[i] = 0.0;
				for (int j = 0; j < 3; j++)
					m_Orientation[i][j] = 0.0;
				m_Orientation[i][i] = 1.0;
			}
		}
		// constructor
		Frame(int frameID, int refID) : m_FrameID(frameID), m_refID(refID) 
		{
			for (int i = 0; i < 3; i++)
			{
				m_GocToaDo[i] = 0.0;
				for (int j = 0; j < 3; j++)
					m_Orientation[i][j] = 0.0;
				m_Orientation[i][i] = 1.0;
			}
		}
		// copy constructor for
		Frame(const Frame& frame) : m_FrameID(frame.m_FrameID), m_refID(frame.m_refID)
		{
			for (int i = 0; i < 3; i++)
			{
				m_GocToaDo[i] = frame.m_GocToaDo[i];
				for (int j = 0; j < 3; j++)
					m_Orientation[i][j] = frame.m_Orientation[i][j];
			}
		}
		// operator=
		Frame& operator=(const Frame& frame)
		{
			m_FrameID = frame.m_FrameID;
			m_refID = frame.m_refID;
			for (int i = 0; i < 3; i++)
			{
				m_GocToaDo[i] = frame.m_GocToaDo[i];
				for (int j = 0; j < 3; j++)
					m_Orientation[i][j] = frame.m_Orientation[i][j];
			}
			return *this;
		}
		
		void SetGocToaDo(double goctoado[3]) {for(int i=0;i<3;i++) m_GocToaDo[i]=goctoado[i];}
		~Frame() {}
	public:
		int			m_FrameID;						// ID he toa do
		double		m_GocToaDo[3];					// goc toa do
		double		m_Orientation[3][3];			// dinh huong
		int			m_refID;						// ID cua htd tham chieu, goc td va huong cac truc so voi htd nay
	private:

	};
	//
	//
	void SetOrientation(double Orientation[3][3], double angles[], AnglesType anglesType, bool bRad = true)
	{
		double pi1=3.1415926535897932384626433832795;
		switch (anglesType)
		{
		case CardanAngles://Dong luc hoc he nhieu vat - p78
			{
				double alp = angles[0], beta=angles[1], gamma=angles[2];
				if (!bRad)
				{alp*=pi1/180.0; beta*=pi1/180.0; gamma*=pi1/180.0;}
				Orientation[0][0]=cos(beta)*cos(gamma);
				Orientation[0][1]=-cos(beta)*sin(gamma);
				Orientation[0][2]=sin(beta);

				Orientation[1][0]=cos(alp)*sin(gamma)+sin(alp)*sin(beta)*cos(gamma);
				Orientation[1][1]=cos(alp)*cos(gamma)-sin(alp)*sin(beta)*sin(gamma);
				Orientation[1][2]=-sin(alp)*cos(beta);

				Orientation[2][0]=sin(alp)*sin(gamma)-cos(alp)*sin(beta)*cos(gamma);
				Orientation[2][1]=sin(alp)*cos(gamma)+cos(alp)*sin(beta)*sin(gamma);
				Orientation[2][2]=cos(alp)*cos(beta);
			}
			break;
		case EulerAngles: // Dong Luc Hoc He Nhieu Vat - p75
			{
				double psi = angles[0], theta = angles[1], phiv = angles[2];
				if (!bRad)
				{psi *= pi1/180.0; theta *= pi1/180.0; phiv *= pi1/180.0;}
				Orientation[0][0]=cos(psi)*cos(phiv)-sin(psi)*cos(theta)*sin(phiv);
				Orientation[0][1]=-cos(psi)*sin(phiv)-sin(psi)*cos(theta)*cos(phiv);
				Orientation[0][2]=sin(psi)*sin(theta);

				Orientation[1][0]=sin(psi)*cos(phiv)+cos(psi)*cos(theta)*sin(phiv);
				Orientation[1][1]=-sin(psi)*sin(phiv)+cos(psi)*cos(theta)*cos(phiv);
				Orientation[1][2]=-cos(psi)*sin(theta);

				Orientation[2][0]=sin(theta)*sin(phiv);
				Orientation[2][1]=sin(theta)*cos(phiv);
				Orientation[2][2]=cos(theta);
			}
			break;
		case RollPitchYallAngles:// dlh hnv 86
			{
				double phiv=angles[0], theta=angles[1], psi=angles[2];
				if (!bRad)
				{phiv*=pi1/180.0; theta*=pi1/180.0; psi*=pi1/180.0;}
				Orientation[0][0]=cos(phiv)*cos(theta);
				Orientation[0][1]=cos(phiv)*sin(theta)*sin(psi)-sin(phiv)*cos(psi);
				Orientation[0][2]=cos(phiv)*sin(theta)*cos(psi)+sin(phiv)*sin(psi);

				Orientation[1][0]=sin(phiv)*cos(theta);
				Orientation[1][1]=sin(phiv)*sin(theta)*sin(psi)+cos(phiv)*cos(psi);
				Orientation[1][2]=sin(phiv)*sin(theta)*cos(psi)-cos(phiv)*sin(psi);

				Orientation[2][0]=-sin(theta);
				Orientation[2][1]=cos(theta)*sin(psi);
				Orientation[2][2]=cos(theta)*cos(psi);
			}
			break;
		case RotX:
			{
				double a = angles[0];
				if (!bRad) a *= pi1/180.0;
				Orientation[0][0]=1.0;Orientation[0][1]=Orientation[0][2]=0.0;
				Orientation[1][0]=0.0;Orientation[1][1]=cos(a);Orientation[1][2]=-sin(a);
				Orientation[2][0]=0.0;Orientation[2][1]=sin(a);Orientation[2][2]=cos(a);
				break;
			}
		case RotY:
			{
				double a = angles[0];
				if (!bRad) a *= pi1/180.0;
				Orientation[0][0]=cos(a);Orientation[0][1]=0.0;Orientation[0][2]=sin(a);
				Orientation[1][0]=0.0;Orientation[1][1]=1.0;Orientation[1][2]=0.0;
				Orientation[2][0]=-sin(a);Orientation[2][1]=0.0;Orientation[2][2]=cos(a);
				break;
			}
		case RotZ:
			{
				double a = angles[0];
				if (!bRad) a *= pi1/180.0;
				Orientation[0][0]=cos(a);Orientation[0][1]=-sin(a);Orientation[0][2]=0;
				Orientation[1][0]=sin(a);Orientation[1][1]=cos(a);Orientation[1][2]=0;
				Orientation[2][0]=Orientation[2][1]=0.0;Orientation[2][2]=1.0;
				break;
			}
		default:{
				for (int i=0;i<3;i++){
					for (int j=0;j<3;j++)	Orientation[i][j]=0.0;
					Orientation[i][i]=1.0;}}
			break;
		}
	}
	// theta-d-a-alpha
	void SetOrientation(double Orientation[3][3], double params_dh[4], bool bRad = true) // thong so dh
	{
		double pi1=3.1415926535897932384626433832795;
		double theta=params_dh[0], d=params_dh[1], a=params_dh[2], alpha=params_dh[3];
		if (!bRad) {theta *= pi1/180.0; alpha *= pi1/180.0;}
		Orientation[0][0]=cos(theta);
		Orientation[0][1]=-sin(theta)*cos(alpha);
		Orientation[0][2]=sin(theta)*sin(alpha);
		
		Orientation[1][0]=sin(theta);
		Orientation[1][1]=cos(theta)*cos(alpha);
		Orientation[1][2]=-cos(theta)*sin(alpha);

		Orientation[2][0]=0.0;
		Orientation[2][1]=sin(alpha);
		Orientation[2][2]=cos(alpha);
	}
	
	// mo hinh hoa vat-ran
	// chua ID cua khop truoc no
	// chua IDs cua cac joint noi tiep voi no
	class RigidBody
	{
		typedef std::vector<int>		v_int;
	public:
		// mo hinh vat-ran
		// constructor - fixed body
		RigidBody(int bodyID = 0) : m_RigidBodyID(bodyID), m_Mass(INF), m_refJointID(-1)
		{
			for (int i=0;i<3;i++)
			{
				m_rCoM[i] = 0.0;
				for (int j=0;j<3;j++)
					m_Inertial[i][j] = 0.0;
			}
		}
		// constructor
		RigidBody(int rigidBodyID, double mass, double CoM[3], double Inertial[3][3], int refJointID) 
			: m_RigidBodyID(rigidBodyID), m_Mass(mass), m_refJointID(refJointID)
		{
			for (int i=0;i<3;i++)
			{
				m_rCoM[i] = CoM[i];
				for (int j=0;j<3;j++)
					m_Inertial[i][j] = Inertial[i][j];
			}
		}
		// constructor
		RigidBody(int rigidBodyID, double mass, int refJointID = -1) : m_RigidBodyID(rigidBodyID), m_Mass(mass), m_refJointID(refJointID)
		{
			for (int i=0;i<3;i++)
			{
				m_rCoM[i] = 0.0;
				for (int j=0;j<3;j++)
					m_Inertial[i][j] = 0.0;
			}
		}
		// constructor
		RigidBody(int rigidBodyID, double mass, double CoM[3], double Inertial[3][3], const Frame &frame, int refJointID) 
			: m_RigidBodyID(rigidBodyID), m_Mass(mass), m_CS0(frame), m_refJointID(refJointID)
		{
			for (int i=0;i<3;i++)
			{
				m_rCoM[i] = CoM[i];
				for (int j=0;j<3;j++)
					m_Inertial[i][j] = Inertial[i][j];
			}
		}
		// copy constructor
		RigidBody(const RigidBody& rigidBody)
		{
			m_RigidBodyID = rigidBody.m_RigidBodyID;
			m_refJointID = rigidBody.m_refJointID;
			m_Mass = rigidBody.m_Mass;
			m_CS0 = rigidBody.m_CS0;
			m_ref_edJointIDs = rigidBody.m_ref_edJointIDs;
			for (int i=0;i<3;i++)
			{
				m_rCoM[i] = rigidBody.m_rCoM[i];
				for (int j=0;j<3;j++)
					m_Inertial[i][j] = rigidBody.m_Inertial[i][j];
			}
		}
		// operator=
		RigidBody& operator=(const RigidBody& rigidBody)
		{
			m_RigidBodyID = rigidBody.m_RigidBodyID;
			m_refJointID = rigidBody.m_refJointID;
			m_Mass = rigidBody.m_Mass;
			m_CS0 = rigidBody.m_CS0;
			m_ref_edJointIDs = rigidBody.m_ref_edJointIDs;
			for (int i=0;i<3;i++)
			{
				m_rCoM[i] = rigidBody.m_rCoM[i];
				for (int j=0;j<3;j++)
					m_Inertial[i][j] = rigidBody.m_Inertial[i][j];
			}
			return (*this);
		} 
		void print()
		{
			cout <<"\n";
			cout << "\t ID = " << m_RigidBodyID<<"\n";
			cout <<"\t mass = "<<m_Mass<<"\n"; 
			cout <<"\t refJointID = " << m_refJointID <<"\n";
			int si = m_ref_edJointIDs.size();
			if (si <= 0)
				cout <<"\t m_ref_ed size = 0\n";
			else
				for (int i = 0; i < si; i++)
					cout <<"\t "<<i<<" th = "<<m_ref_edJointIDs[i];
			cout <<"\n";
		}

		// fixedBody or moveable Body
		bool IsFixedBody()
		{
			return ((m_Mass >= INF));
		}
		void MatranThuanNhat(double out[4][4])
		{
			for (int i = 0; i < 3; i++)
			{
				out[3][i] = 0.0;
				out[i][3] = m_CS0.m_GocToaDo[i];
				for (int j = 0;j < 3; j++)
					out[i][j] = m_CS0.m_Orientation[i][j];
			}
			out[3][3] = 1.0;
		}

		~RigidBody() {m_ref_edJointIDs.clear();}
	public:
		int				m_RigidBodyID;
		double			m_Mass;						// khoi luong
		double			m_rCoM[3];					// vi tri khoi tam trong htd khau
		double			m_Inertial[3][3];			// mtqt trong htdqt chinh trung tam
		Frame			m_CS0;						// htd khau, tam va huong so voi htd gan tai khop truoc do
		int				m_refJointID;				// ID cua khop truoc do
	//protected:
		v_int			m_ref_edJointIDs;			// danh sach cac khop co tham chieu den htd gan voi khau nay, <> m_refJointID
	};
}
#endif // RIGID_BODY_HEADER_H