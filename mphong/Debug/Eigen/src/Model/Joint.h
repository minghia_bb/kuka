/*
	* Modeling
	* Copyright (c) by Le Minh Nghia - KSTN CDT k54 - HUST - Nov 2013
	* <minghiale.m54@gmail.com>
	* Mo hinh hoa Lien Ket Khop
	* 
*/
#ifndef JOINT_HEADER_H
#define JOINT_HEADER_H
//#include <vector>
#include "RigidBody.h"
namespace LMN
{
	using namespace std;
	// loai khop
	enum JointType
	{
		JointTypeUndefined = 0,
		JointTypeRevolute,
		JointTypePrismatic,
		JointTypeSphere,
		JointTypeCylinder
	};
	// khop
	// chua ID cua khau chua no
	// chua IDs cua cac khau noi voi khau chua no thong qua no
	class Joint
	{	
	public:
		typedef vector<int>		v_int;
		// methods
	public:
		// constructor
		Joint() : m_JointType(JointTypeUndefined), m_JointID(0), m_refID(0), m_IsActive(false){m_JointVars.clear(); m_ref_edLinkIDs.clear();}
		// constructor
		Joint(JointType	jointtype, int jointID, bool bActive = false) : 
			m_JointType(jointtype), m_JointID(jointID), m_refID(0), m_IsActive(bActive)
		{
			for(int i=0;i<3;i++) 
			{	
				m_pos[i]=0.0;
				for (int j = 0; j < 3; j++)
					m_Orientation[i][j]=0.0;
				m_Orientation[i][i] = 1.0;
			}
		}
		// constructor
		Joint(JointType	jointtype, int jointID, int refFrame, bool bActive = false) : 
			m_JointType(jointtype), m_JointID(jointID), m_refID(refFrame), m_IsActive(bActive)
		{
			for(int i=0;i<3;i++) 
			{	
				m_pos[i] = 0.0;
				for (int j = 0; j < 3; j++)
					m_Orientation[i][j]=0.0;
				m_Orientation[i][i] = 1.0;
			}
		}
		// constructor
		Joint(JointType	jointtype, int jointID, int refFrame, double pos[3], bool bActive = false) : 
			m_JointType(jointtype), m_JointID(jointID), m_refID(refFrame), m_IsActive(bActive)
		{
			for(int i=0;i<3;i++) 
			{	
				m_pos[i]=pos[i];
				for (int j = 0; j < 3; j++)
					m_Orientation[i][j]=0.0;
				m_Orientation[i][i] = 1.0;
			}
		}
		// constructor
		Joint(JointType	jointtype, int jointID, int refFrame, double pos[3], double Orientation[3][3], bool bActive = false) : 
			m_JointType(jointtype), m_JointID(jointID), m_refID(refFrame), m_IsActive(bActive)
		{
			for(int i=0;i<3;i++) 
			{
				m_pos[i]=pos[i];	
				for (int j = 0; j < 3; j++)
					m_Orientation[i][j]=Orientation[i][j];
			}
		}
		// copyconstructor
		Joint (const Joint& joint)
		{
			m_JointType=joint.m_JointType;
			m_JointID = joint.m_JointID;
			m_refID = joint.m_refID;
			for (int i=0;i<3;i++)
			{
				m_pos[i]=joint.m_pos[i];
				for (int j = 0; j < 3; j++)
					m_Orientation[i][j]=joint.m_Orientation[i][j];
			}
			m_JointVars = joint.m_JointVars;
			m_ref_edLinkIDs = joint.m_ref_edLinkIDs;
			m_IsActive = joint.m_IsActive;
		}
		// operator=
		Joint operator= (Joint joint)
		{
			m_JointType=joint.m_JointType;
			m_JointID = joint.m_JointID;
			m_refID = joint.m_refID;
			for (int i=0;i<3;i++)
			{
				m_pos[i]=joint.m_pos[i];
				for (int j = 0; j < 3; j++)
					m_Orientation[i][j]=joint.m_Orientation[i][j];
			}
			m_JointVars = joint.m_JointVars;
			m_ref_edLinkIDs = joint.m_ref_edLinkIDs;
			m_IsActive = joint.m_IsActive;
			return (*this);
		}
		~Joint() {m_JointVars.clear(); m_ref_edLinkIDs.clear();}
		//										so toa do suy rong
		int GetToaDoSuyRong()
		{
			if (m_JointType == JointTypeUndefined) return 0;
			if (m_JointType == JointTypeRevolute || m_JointType == JointTypePrismatic) return 1;
			if (m_JointType == JointTypeCylinder || m_JointType == JointTypeSphere) return 2;
			return -1;
		}
		void MatranThuanNhat(double mttn[4][4])
		{
			for (int i=0;i<3;i++)
			{
				mttn[i][3]=m_pos[i];
				for (int j=0;j<3;j++)
					mttn[i][j]=m_Orientation[i][j];
			}
			mttn[3][0]=mttn[3][1]=mttn[3][2]=0.0;
			mttn[3][3]=1.0;
		}
		//
		void MatranThuanNhat(double mttn[4][4], double orient[3][3])
		{
			for (int i=0;i<3;i++)
				for(int j=0;j<3;j++)
					mttn[i][j]=orient[i][j];
			mttn[3][0]=mttn[3][1]=mttn[3][2]=0.0;
			mttn[0][3]=mttn[1][3]=mttn[2][3]=0.0;
			mttn[3][3]=1.0;
		}
		// matran chuyen
		void MatranChuyen(double mttn[4][4], double value, JointType type)
		{
			switch (type)
			{
			case JointTypeRevolute:
				{
					double orient[3][3], 
						angles[1]={value};
					SetOrientation(orient, angles, RotZ);
					MatranThuanNhat(mttn, orient);
				}
				break;
			case JointTypePrismatic:
				for (int i=0;i<4;i++)
				{
					for(int j=0;j<4;j++)
						mttn[i][j]=0.0;
					mttn[i][i]=1.0;
				}
				mttn[2][3] = value;
				break;
			default:
				for (int i=0;i<4;i++)
				{
					for(int j=0;j<4;j++)
						mttn[i][j]=0.0;
					mttn[i][i]=1.0;
				}
				break;
			}
		}
		// vi phan cap 1 ma tran thuannhat
		// d[T(q)] = (T1-T2)/h
		void ViphanMatranChuyen(double viphanMttn[4][4], double q0, JointType type, double h = 1e-6)
		{
			double mttn1[4][4], mttn2[4][4];
			MatranChuyen(mttn1, q0+h, type);
			MatranChuyen(mttn2, q0, type);
			for (int i = 0; i < 4; i++)
				for (int j = 0; j < 4; j++)
					viphanMttn[i][j] = (mttn1[i][j]-mttn2[i][j]) /h;
		}
		// vi phan cap 2 ma tran thuannhat
		// dd[T(q)] 
		void ViphanMatranChuyen2(double viphanMttn[4][4], double q0, JointType type, double h = 1e-6)
		{
			double mttn1[4][4], mttn2[4][4], mttn3[4][4];
			MatranChuyen(mttn1, q0+h, type);
			MatranChuyen(mttn2, q0-h, type);
			MatranChuyen(mttn3, q0, type);
			for (int i = 0; i < 4; i++)
				for (int j = 0; j < 4; j++)
					viphanMttn[i][j] = (mttn1[i][j]+mttn2[i][j]-2*mttn3[i][j]) / (h*h);
		}
		// mo hinh hoa lien ket khop
	protected:
	public:
		// can nhap
		int			m_JointID;
		JointType	m_JointType;					// loai khop
		double		m_pos[3];						// vi tri cua khop so voi htd khau no gan voi
		int			m_refID;						// htd khau no gan voi khau m_refID
		double		m_Orientation[3][3];			// dinh huong truc cua khop (xyz) so voi he toa do cua khau truoc (m_refID)
													// tai khop co 1 htd khau cua khau chua khop
		bool		m_IsActive;						// khop chu dong
	//protected:
		// xu li
		v_int		m_JointVars;					// cac bien khop trong danh sach cac bien khop
		v_int		m_ref_edLinkIDs;				// 1 khop co the co nhieu khau noi chung, <> m_refID
	};
}

#endif // JOINT_HEADER_H