#include <iostream>
#include <Eigen\src\Numericcomputing\numericcomputing.h>
using namespace std;
using namespace Eigen;

typedef Eigen::MatrixXd	matrix;
typedef Eigen::VectorXd	Vector;

Vector tspan2(double t0, double h, double tf)
{
	int N = (int)((tf-t0)/h);
	Vector vec(N+1);
	for (int i = 0; i < N; i++)
		vec(i) = t0 + i*h;
	vec(N) = tf;
	return vec;
}

// y' = -y + 1
Vector ht1(double t)
{
	Vector v(1);
	v(0)= 1.0;
	return v;
}
Vector gy1(Vector y)
{
	return Vector(y*(-1));
}
double nghiem1(double t)
{
	return 1.0-exp(-t);
}
void test_ode45_1()
{
	cout<<"\n-----test 1\n";
	Vector ts;
	ts = tspan2(0.0, 0.05, 2.0);
	matrix y;
	Vector y0(1);
	y0(0) = 0.0;
	y = NC::ode_45(gy1, ht1, ts, y0);
	cout.precision(10);
	cout<<"\n";
	for (int i = 0; i < 10; i++)
		cout<<i*0.05<<"\t"<<nghiem1(i*0.05)<<"\n";
	cout<<2.0<<"\t"<<nghiem1(2.0);
	cout<<"\n----\n";
	cout<<y;
}

void test_ode45_2(void)
{
	cout<<"\n-----test 2\n";
	Vector ts;
	double t0=0.0, tf=2.0, h=0.05;
	ts = tspan2(0.0, 0.05, 2.0);
	matrix 
		Mht(2 * NC::length(ts) + 1, 1);
	Vector tmp(1);
	for (int i = 0; i < Mht.rows(); i++)
	{
		tmp = ht1(t0+i*h/2);
		Mht(i,0) = tmp(0);
	}
	
	Vector y0(1);
	y0(0) =  0.0;
	matrix 
		y = NC::ode_45(gy1, Mht, ts, y0);
	cout<<"\n";
	for (int i = 0; i < 10; i++)
		cout<<i*h<<"\t"<<nghiem1(i*h)<<"\n";
	cout << y;
	cout<<"\n";
}

// y1 = exp(-2x) + exp(k*x), 
// y2 = x^2 + exp(-k2*x^2)
// dy1 = -2*exp(-2x) + k*exp(k*x),
// dy2 = 2*x - 2*x * k2*exp(-k2*x^2)
double k = 0.1,
	k2 = 0.5;
Vector ht2(double t)
{
	Vector dy(2);
	dy(0) = -2*exp(-2*t) + k*exp(k*t);
	dy(1) = 2*t - 2*t*k2*exp(-k2*t*t);
	return dy;
}

Vector gy2(Vector y)
{
	Vector v(2);
	v(0) = v(1) = 0.0;
	return v;
}

Vector nghiem2(double x)
{
	Vector y(2);
	y(0) = exp(-2*x) + exp(k*x);
	y(1) = x*x + exp(-k2*x*x);
	
	return y;
}
void test_ode45_3(void)
{
	cout<<"\n-----test 3\n";
	Vector ts, tmp;
	double t0 = 0.0, tf = 10.0, h = 0.01;
	ts = tspan2(t0, h, tf);
	matrix y;
	Vector y0(2);
	y0(0) = 2.0; y0(1) = 1.0;
	y = NC::ode_45(gy2, ht2, ts, y0);
	cout.precision(15);
	for (int i = 10; i >= 0; i--)
	{
		double ti = tf - i*h;
		tmp = nghiem2(ti);
		cout<<ti<<"\t"<<tmp(0)<<"\t"<<tmp(1)<<"\n";
	}
	cout<<"\n";
	//cout<<y;
	for (int i = y.rows()-11; i < y.rows(); i++)
	{
		for (int j = 0; j < y.cols(); j++)
			cout<<y(i,j)<<"\t";
		cout<<"\n";
	}
}

// y = e^(t*t) - 1
// dy = 2*t*(y +1)
Vector fty(double t, Vector y)
{
	int ysize = NC::length(y);
	Vector ones(ysize);
	ones.fill(1.0);
	Vector dy = (y + ones) * 2 * t;
	return dy;
}

double nghiem4(double t)
{
	return exp(t*t) - 1;
}
void test_ode45_4(void)
{
	cout<<"\n test ode_45_4\n";
	Vector ts, y0(1);
	y0(0) = 0.0;
	double t0 = 0.0, tf= 2.0, h= 0.05;
	ts = tspan2(t0, h, tf);
	matrix y;
	y = NC::ode_45(fty, ts, y0);
	for (int i = 0; i < 10;i++)
		cout<<i*h<<"\t"<<nghiem4(i*h) <<"\n";
	cout<<y;
}


// MTKL . q" + V(q, dq) + G(q) = f
matrix MTKL(Vector q)
{
	matrix M(2,2);
	M(0,0) = 2.0;
	M(1,1) = 1.0;
	M(0,1) = M(1,0) = 0.0;
	return M;
}

Vector Vv(Vector q, Vector dq)
{
	matrix B(2,2);
	B(0,0) = 3.0;
	B(0,1) = B(1,0) = -0.5;
	B(1,1) = 0.5;
	return B*dq;
}

Vector Gq(Vector q)
{
	matrix C(2,2);
	C(0,0) = 3.0;
	C(0,1) = C(1,0) = -1;
	C(1,1) = 1;
	return C*q;
}

Vector Ft(double t)
{
	double Omega = 2;
	Vector f(2);
	f(0) = 4*sin(Omega*t);
	f(1) = 0.5*sin(2*Omega*t);
	return f;
}

// ha bac, tinh y = [q,q'], dy = [q', q"]
Vector dy(double t, Vector y)
{
	int size = NC::length(y);
	Vector q = NC::sub_vector(y, 0, size / 2 - 1);
	Vector dq = NC::sub_vector(y, size / 2, size - 1);
	matrix M = MTKL(q);
	//Vector ddq = M.colPivHouseholderQr().solve((Ft(t) - Vv(q, dq) - Gq(q)));
	Vector ddq = M.fullPivLu().solve(Ft(t) - Vv(q,dq) - Gq(q));
	Vector _dy = NC::combine_vector(dq, ddq);
	return _dy;
}

// cach giai 1
void test_ode45_5()
{
	cout<<"\n test ode_45_5\n";
	Vector y0(2), dy0(2), dq0(4);
	y0(0) = y0(1) = dy0(0) = dy0(1) = 0.0;
	dq0.fill(0.0);
	double t0 = 0.0, tf= 15.0, h= 0.01;
	Vector ts = tspan2(t0, h, tf);
	matrix y = 
		NC::ode_45(dy, ts, dq0);
	for (int i = 0; i < 10; i++)
	{
		double ti = i * h;
		cout << ti;
		for (int j = 0; j < y.cols(); j++)
			cout << "\t"<< y(i,j) ;
		cout <<"\n";
	}
	cout << "\n ---------------\n";
	for (int i = y.rows() - 10; i < y.rows(); i++)
	{
		double ti = t0 + i * h;
		cout << ti;
		for (int j = 0; j < y.cols(); j++)
			cout<< "\t" << y(i,j);
		cout <<"\n";
	}
	

}

// cach giai 2
void test_ode45_52()
{
	cout<<"\n test ode_45_52\n";
	Vector y0(2), dy0(2), dq0(4);
	y0(0) = y0(1) = dy0(0) = dy0(1) = 0.0;
	dq0.fill(0.0);
	double t0 = 0.0, tf= 15.0, h= 0.01;
	Vector ts = tspan2(t0, h, tf);
	matrix y = 
		NC::ode_45(MTKL, Vv, Gq, Ft, ts, y0, dy0);
	for (int i = 0; i < 10; i++)
	{
		double ti = i * h;
		cout << ti;
		for (int j = 0; j < y.cols(); j++)
			cout << "\t"<< y(i,j) ;
		cout <<"\n";
	}
	cout << "\n ---------------\n";
	for (int i = y.rows() - 10; i < y.rows(); i++)
	{
		double ti = t0 + i * h;
		cout << ti;
		for (int j = 0; j < y.cols(); j++)
			cout<< "\t" << y(i,j);
		cout <<"\n";
	}
}

// cach giai 3
void test_ode45_53()
{
	cout<<"\n test ode_45_53\n";
	Vector y0(2), dy0(2), dq0(4);
	y0(0) = y0(1) = dy0(0) = dy0(1) = 0.0;
	dq0.fill(0.0);
	double t0 = 0.0, tf= 15.0, h= 0.01;
	Vector ts = tspan2(t0, h, tf);
	matrix Tau(2 * NC::length(ts) - 1, NC::length(y0));
	Vector tmp;
	for (int i = 0; i < Tau.rows(); i++)
	{
		double ti = t0 + i*h/2;
		tmp = Ft(ti);
		for (int j = 0; j < NC::length(tmp); j++)
			Tau(i,j) = tmp(j);
	}
	matrix y = 
		NC::ode_45(MTKL, Vv, Gq, Tau, ts, y0, dy0);
	for (int i = 0; i < 10; i++)
	{
		double ti = i * h;
		cout << ti;
		for (int j = 0; j < y.cols(); j++)
			cout << "\t"<< y(i,j) ;
		cout <<"\n";
	}
	cout << "\n ---------------\n";
	for (int i = y.rows() - 10; i < y.rows(); i++)
	{
		double ti = t0 + i * h;
		cout << ti;
		for (int j = 0; j < y.cols(); j++)
			cout<< "\t" << y(i,j);
		cout <<"\n";
	}
}