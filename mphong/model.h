/*
*
*
*/
#ifndef MDL_HEADER_H
#define MDL_HEADER_H

#include "C3dofs.h"
#include "model_banmay.h"

#include "StlReader.h"
#include "Scene.h"
#include <Eigen\src\Numericcomputing\robot.h>
#include <Eigen\Dense>

typedef Eigen::MatrixXd matrixXd;
typedef Eigen::VectorXd vectorXd;
using namespace opengl;

namespace mdl {

//using namespace robot;
/*
enum LoaiBaiToan
	{
		DHT_VITRI = 0,	// Dong hoc thuan ve vi tri (3D only)
		DHN_VITRI,		// Dong hoc nguoc ve vi tri (3D only)
		DHT_VIPHAN,		// Dong hoc thuan ve van toc, gia toc (2D(3D))
		DHN_VIPHAN,		// Dong hoc nguoc ve van toc, gia toc (2D(3D))
		//DHN_HUONG,		// Dong hoc nguoc ve huong (2D(3D))  // chi co o ban may 2btd
		DLHT,			// Dong luc hoc thuan (2D, 3D)
		DLHN			// Dong luc hoc nguoc (2D(3D))
	};*/

class CRRR : public robotData::C3dofs
{
public:
	CRRR() : C3dofs() {loaiBaiToan = robot::DHT_VITRI;}
	~CRRR() {}
	//
	robot::LoaiBaiToan		loaiBaiToan;
	
	// vitri diem thao tac
	vectorXd	rE(vectorXd q)
	{
		vectorXd ret(3);
		double xe, ye, ze;
		xe = xE_RRR(q(0), q(1), q(2));
		ye = yE_RRR(q(0), q(1), q(2));
		ze = zE_RRR(q(0), q(1), q(2));
		ret(0) = xe; ret(1) = ye; ret(2) = ze;
		return ret;
	}
	// vantoc diem thao tac
	vectorXd	vE(vectorXd	q, vectorXd dq)
	{
		vectorXd ret(3);
		ret(0) = vEx_RRR(q(0),q(1),q(2),dq(0), dq(1),dq(2));
		ret(1) = vEy_RRR(q(0),q(1),q(2),dq(0), dq(1),dq(2));
		ret(2) = vEz_RRR(q(0),q(1),q(2),dq(0), dq(1),dq(2));
		return ret;
	}
	// giatoc diem thao tac
	vectorXd	aE(vectorXd	q, vectorXd dq, vectorXd ddq)
	{
		vectorXd ret(3);
		ret(0) = aEx_RRR(q(0),q(1),q(2),dq(0), dq(1),dq(2), ddq(0), ddq(1), ddq(2));
		ret(1) = aEy_RRR(q(0),q(1),q(2),dq(0), dq(1),dq(2), ddq(0), ddq(1), ddq(2));
		ret(2) = aEz_RRR(q(0),q(1),q(2),dq(0), dq(1),dq(2), ddq(0), ddq(1), ddq(2));
		return ret;
	}
	// hept rang buoc dong hoc ve vi tri
	vectorXd funcsys(vectorXd q)
	{
		return (rE(q));
	}
	// matran Jacobian
	matrixXd Jacobian(vectorXd q)
	{
		double jac[3][3];
		Jacobi_RRR(q(0), q(1), q(2), jac);
		matrixXd ret(3,3);
		for (int i=0;i<3;i++)
			for (int j=0;j<3;j++)
				ret(i,j)=jac[i][j];
		return ret;
	}
	// daoham matranJacobian
	matrixXd diff_Jacobian(vectorXd q, vectorXd dq)
	{
		double diff_jac[3][3];
		diff_Jacobi_RRR(q(0),q(1),q(2),dq(0),dq(1),dq(2),diff_jac);
		matrixXd ret(3,3);
		for (int i=0;i<3;i++)
			for (int j=0;j<3;j++)
				ret(i,j)=diff_jac[i][j];
		return ret;
	}
	// matran khoiluong
	matrixXd MTKL(vectorXd q)
	{
		double m_rrr[3][3];
		M_RRR(q(0),q(1),q(2),m_rrr);
		matrixXd ret(3,3);
		for (int i=0;i<3;i++)
			for (int j=0;j<3;j++)
				ret(i,j)=m_rrr[i][j];
		return ret;
	}
	// 
	vectorXd Vv(vectorXd q, vectorXd dq)
	{
		double vV[3];
		H_RRR(q(0),q(1),q(2),dq(0),dq(1),dq(2),vV);
		vectorXd ret(3);
		for (int i=0;i<3;i++)
			ret(i)=vV[i];
		return ret;
	}
	// Tau
	vectorXd Tau(vectorXd q, vectorXd dq, vectorXd ddq)
	{
		return (MTKL(q)*ddq + Vv(q,dq));
	}
	// vector
	matrixXd operator()(vectorXd q)
	{
		/*std::ofstream f("log.txt", std::ios::app);
		f << "Loai bai toan = "<<loaiBaiToan;
		f.close();*/
		switch(loaiBaiToan)
		{
		default:
		case robot::DHT_VITRI:
			{
				return (rE(q));
			}
		case robot::DHN_VITRI: // diff_funcsys
			{
				return (Jacobian(q));
			}
		//case robot::DHT_VIPHAN:
		case robot::DHN_VIPHAN: // Jacobian
			{
				return (Jacobian(q));
			}
		case robot::DLHT:
			{
				return (MTKL(q));
			}
		//case robot::DLHN:
		}
	}
	// vectorXd
	matrixXd operator()(vectorXd q, vectorXd dq)
	{
		switch(loaiBaiToan)
		{
		default:
		//case robot::DHT_VITRI:
		//case robot::DHN_VITRI:
		case robot::DHT_VIPHAN:
			{
				return (vE(q, dq));
			}
		case robot::DHN_VIPHAN:
			{
				return (diff_Jacobian(q,dq));
			}
		case robot::DLHT:
			{
				return (Vv(q,dq));
			}
		//case robot::DLHN:
		}
	}
	//
	vectorXd operator()(vectorXd q, vectorXd dq, vectorXd ddq)
	{
		switch(loaiBaiToan)
		{
		default:
		case robot::DHT_VIPHAN:
			{
				return (aE(q,dq,ddq));
			}
		case robot::DLHN:
			{
				return (Tau(q,dq,ddq));
			}
		}
	}
	vectorXd operator()(vectorXd q, int func)
	{
		return (funcsys(q));
	}
};

class CmdlRRR
{
	std::vector<std::string> searchPaths;
public:
	// data 
	CRRR			myRobot;
	int numLinks;
	int numStlFiles;
	CStlReader			stlFiles[4];
	GLuint				displayLists[4];
	CColor4f			colors[4];
	// scene
	CScene				m_Scene;
	//
	CmdlRRR() : myRobot(), m_Scene(200,90,200)
	{
		numLinks = 4;
		numStlFiles = 4;
		stlFiles[0].ObjectName = "de.stl";
		stlFiles[1].ObjectName = "khau1.stl";
		stlFiles[2].ObjectName = "khau2.stl";
		stlFiles[3].ObjectName = "khau3.stl";
		//
		searchPaths.push_back("stl/");
		searchPaths.push_back("F:/Documents/SolidWorks 2010/Do-an-2/");
		searchPaths.push_back("../Debug/stl/");
		//
		colors[0] = CColor4f(.2, .2, .8);
		colors[1] = CColor4f(.5, .8, .1);
		colors[2] = CColor4f(.8, .8, 1.);
		colors[3] = CColor4f(.1, .8, .2);
		displayLists[0]=displayLists[1]=displayLists[2]=displayLists[3]=0;
	}
	~CmdlRRR() {}
	void Create()
	{
		// Load stlFiles
		for (int i=0; i< numStlFiles; i++)
		{
			FILE* f = fopen(stlFiles[i].ObjectName.c_str(), "r");
			if (f)
			{
				fclose(f);
				continue;
			}
			for (int j=0;j<(int)searchPaths.size();j++)
			{
				std::string filePath;
				filePath.append(searchPaths[j]);
				filePath.append(stlFiles[i].ObjectName);
				FILE* ff = fopen(filePath.c_str(), "r");
				if (!ff)
					continue;
				stlFiles[i].ObjectName = filePath;
				fclose(ff);
				break;
			}
		}
		// load stlfiles
		for (int i =0;i < numStlFiles ;i++)
			stlFiles[i].Load(stlFiles[i].ObjectName);
		// create display list
		displayLists[0] = glGenLists(numStlFiles);
		for (int i=0; i < numStlFiles;i++)
		{
			if (i!=0)
				displayLists[i] = displayLists[i-1] + 1; // xac dinh dia chi cua list tiep theo
			glNewList(displayLists[i], GL_COMPILE);
			stlFiles[i].Draw(false, true);
			glEndList();
		}
	}
	void Transform(int i, vectorXd q)
	{
		CMatrix4x4d m;
		double a[] = {myRobot.a1, myRobot.a2, myRobot.a3};
		double d[] = {myRobot.d1, myRobot.d2, myRobot.d3};
		double alp[] = {myRobot.alp1, myRobot.alp2, myRobot.alp3};
		m.MatrixDH(q(i), d[i], a[i], alp[i]);
		glMultMatrixd(m.getm());
	}
	void draw(vectorXd q)
	{
		glCallList(displayLists[0]);
		for (int i=0;i<numLinks-1; i++)
		{
			Transform(i, q);
			glMaterialfv(GL_FRONT, GL_DIFFUSE, colors[i].getColor());
			glMaterialfv(GL_FRONT, GL_SPECULAR, colors[i].getColor());
			glCallList(displayLists[1+i]);
		}
	}
	void drawTrace(int i, matrixXd rE, bool bLighting = true)
	{
		int nrow = rE.rows(), ncol = rE.cols();
		int ith = min(i, nrow-1);
		if (bLighting)
		{
			glDisable(GL_LIGHTING);
			glDisable(GL_LIGHT0);
		}
		glLineWidth(2.0);
		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		glBegin(GL_LINE_STRIP);
		for (int j = 0; j<ith;j++)
		{
			double xe,ye, ze;
			xe=rE(j,0); ye=rE(j,1); ze=rE(j,2);
			glVertex3d(xe, ye, ze);
		}
		glEnd();
		if (bLighting)
		{
			glEnable(GL_LIGHTING);
			glEnable(GL_LIGHT0);
		}
	}
};

// Ban may 2 bac tu do
// DHT_VIPHAN
// DHN_HUONG <-> DHN_VIPHAN
// DLHT
class CBanmay2dofs : public robotData::CBanmay
{
public:
	CBanmay2dofs() : CBanmay() {loaiBaiToan = robot::DHT_VIPHAN;}
	//
	robot::LoaiBaiToan		loaiBaiToan;
	// vitri diem thao tac
	vectorXd	rE(vectorXd q)
	{
		vectorXd ret(3);
		double xyzE[3];
		rE_RR(q(0), q(1), xyzE);
		ret(0) = xyzE[0]; ret(1) = xyzE[1]; ret(2) = xyzE[2];
		return ret;
	}
	// vantoc diem thao tac
	vectorXd	vE(vectorXd	q, vectorXd dq)
	{
		vectorXd ret(3);
		double vExyz[3];
		vE_RR(q(0), q(1), dq(0), dq(1), vExyz);
		ret(0) = vExyz[0]; ret(1) = vExyz[1]; ret(2) = vExyz[2];
		return ret;
	}
	// giatoc diem thao tac
	vectorXd	aE(vectorXd	q, vectorXd dq, vectorXd ddq)
	{
		vectorXd ret(3);
		double aExyz[3];
		aE_RR(q(0), q(1), dq(0), dq(1), ddq(0), ddq(1), aExyz);
		ret(0) = aExyz[0]; ret(1) = aExyz[1]; ret(2) = aExyz[2];
		return ret;
	}
	// matran khoiluong
	matrixXd MTKL(vectorXd q)
	{
		double m_rrr[2][2];
		M_RR(q(0),q(1),m_rrr);
		matrixXd ret(2,2);
		for (int i=0;i<2;i++)
			for (int j=0;j<2;j++)
				ret(i,j)=m_rrr[i][j];
		return ret;
	}
	// 
	vectorXd Vv(vectorXd q, vectorXd dq)
	{
		double vV[2];
		V_RR(q(0), q(1), dq(0), dq(1), vV);
		vectorXd ret(2);
		for (int i=0;i<2;i++)
			ret(i)=vV[i];
		return ret;
	}
	// Tau
	vectorXd Tau(vectorXd q, vectorXd dq, vectorXd ddq)
	{
		return (MTKL(q)*ddq + Vv(q,dq));
	}
	// vector
	matrixXd operator()(vectorXd q)
	{
		switch(loaiBaiToan)
		{
		default:
		case robot::DHT_VITRI:
			{
				return (rE(q));
			}
		//case robot::DHN_VITRI: // diff_funcsys
		//case robot::DHT_VIPHAN:
		//case robot::DHN_VIPHAN: // Jacobian
		case robot::DLHT:
			{
				return (MTKL(q));
			}
		//case robot::DLHN:
		}
	}
	// vectorXd
	matrixXd operator()(vectorXd q, vectorXd dq)
	{
		switch(loaiBaiToan)
		{
		default:
		//case robot::DHT_VITRI:
		//case robot::DHN_VITRI:
		case robot::DHT_VIPHAN:
			{
				return (vE(q, dq));
			}
		//case robot::DHN_VIPHAN:
		case robot::DLHT:
			{
				return (Vv(q,dq));
			}
		//case robot::DLHN:
		}
	}
	//
	vectorXd operator()(vectorXd q, vectorXd dq, vectorXd ddq)
	{
		switch(loaiBaiToan)
		{
		default:
		//case robot::DHT_VITRI:
		//case robot::DHN_VITRI:
		case robot::DHT_VIPHAN:
			{
				return (aE(q,dq,ddq));
			}
		//case robot::DHN_VIPHAN:
		//case robot::DLHT:
		case robot::DLHN:
			{
				return (Tau(q,dq,ddq));
			}
		}
	}
	// dong hoc nguoc ve huong
	matrixXd dhn_huong_viphan(matrixXd ort2, matrixXd dort2, matrixXd ddort2)
	{
		int nrow = ort2.rows(),
			ncol = ort2.cols(); // =3
		matrixXd ret(nrow, 6); // q1,q2,dq1,dq2,ddq1,ddq2
		for (int i = 0;i < nrow;i++)
		{
			double qi[2], dqi[2], ddqi[2];
			dhn_huong(ort2(i,0), ort2(i,1), ort2(i,2), qi);
			dhn_viphan(qi[0], qi[1], ort2(i,0), ort2(i,1), ort2(i,2), dort2(i,0), dort2(i,1), dort2(i,2), dqi);
			dhn_viphan2(qi[0], qi[1], ort2(i,0), ort2(i,1), ort2(i,2), 
				dqi[0], dqi[1], dort2(i,0), dort2(i,1), dort2(i,2), 
				ddort2(i,0), ddort2(i,1), ddort2(i,2), ddqi);
			ret(i,0) = qi[0]; ret(i,1) = qi[1];
			ret(i,2) = dqi[0]; ret(i,3) = dqi[1];
			ret(i,4) = ddqi[0]; ret(i,5) = ddqi[1];
		}
		return ret;
	}
};

class CmdlBanmay
{
	int numStlFiles;		// 4
	// stlFiles
	CStlReader			stlFiles[4];  // + 1 khau co dinh (base)
	GLuint				displayLists[4];
	CColor4f			colors[4];
	CColor4f			ambient[4];
	// search path
	std::vector<std::string>	searchPaths;
public:
	// data 
	CBanmay2dofs			myRobot;
	int numLinks;			// so khau = so khau dong + so khau co dinh (de^') = 3
	// scene
	CScene				m_Scene;
	double				donvi;				// draw trace, don vi
	//
public: // methods
	CmdlBanmay() : myRobot(), m_Scene(1800, 600, -940, 350, 100, -250), donvi(1000.0)
	{
		numLinks = 3;		numStlFiles = 4;
		stlFiles[0].ObjectName = "ban0.stl";
		stlFiles[1].ObjectName = "ban1.stl";
		stlFiles[2].ObjectName = "ban2.stl";
		stlFiles[3].ObjectName = "base.stl";
		//
		searchPaths.push_back("stl/");
		searchPaths.push_back("F:/Documents/SolidWorks 2010/Do-an-2/");
		searchPaths.push_back("../Debug/stl/");
		//
		colors[0] = CColor4f(.2, .2, .8);
		colors[1] = CColor4f(.5, .8, .1);
		colors[2] = CColor4f(.8, .8, 1.);
		colors[3] = CColor4f(.1, .8, .2);
		ambient[0] = CColor4f(0.1, 0.3, 0.8);
		ambient[1] = CColor4f(0.6, 0.3, 0.3);
		ambient[2] = CColor4f(0.1, 0.6, 0.3);
		ambient[3] = CColor4f(0.5, 0.5, 0.5);
		displayLists[0]=displayLists[1]=displayLists[2]=displayLists[3]=0;
	}
	~CmdlBanmay() {}
	void Create()
	{
		// Load stlFiles
		for (int i=0; i< numStlFiles; i++)
		{
			FILE* f = fopen(stlFiles[i].ObjectName.c_str(), "r");
			if (f)
			{
				fclose(f);
				continue;
			}
			for (int j=0;j<(int)searchPaths.size();j++)
			{
				std::string filePath;
				filePath.append(searchPaths[j]);
				filePath.append(stlFiles[i].ObjectName);
				FILE* ff = fopen(filePath.c_str(), "r");
				if (!ff)
					continue;
				stlFiles[i].ObjectName = filePath;
				fclose(ff);
				break;
			}
		}
		// load stlfiles
		for (int i =0;i < numStlFiles ;i++)
			stlFiles[i].Load(stlFiles[i].ObjectName);
		// create display list
		displayLists[0] = glGenLists(numStlFiles);
		for (int i=0; i < numStlFiles;i++)
		{
			if (i!=0)
				displayLists[i] = displayLists[i-1] + 1; // xac dinh dia chi cua list tiep theo
			glNewList(displayLists[i], GL_COMPILE);
			stlFiles[i].Draw(false, true);
			glEndList();
		}
	}
	// i=0, i<q.size()
	void Transform(int i, vectorXd q)
	{
		switch (i)
		{
		case 0: // q(0) -> ban0->ban1
			{
				CMatrix4x4d m1, m2, m3, m;
				m1.Translate(100.0, 160.0, 120.0);
				m2.Rotate_X(90.0, false);
				m3.Rotate_Z(q(0));  // q(0)
				m = (m1*m2)*m3;
				glMultMatrixd(m.getm());
				break;
			}
		case 1: // q(1) -> ban1->ban2
			{
				CMatrix4x4d m1, m2, m3, m;
				m1.Translate(0, 55.0, 0.0);
				m2.Rotate_X(-90.0, false);
				m3.Rotate_Z(q(1));  // q(1)
				m = (m1*m2)*m3;
				glMultMatrixd(m.getm());
				break;
			}
		default:
			break;
		}
	}
	void draw(vectorXd q)
	{
		// draw base
		glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, colors[3].getColor());
		glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, colors[3].getColor());
		glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ambient[3].getColor());
		glCallList(displayLists[3]);
		// draw ban0
		glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, colors[0].getColor());
		glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, colors[0].getColor());
		glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ambient[0].getColor());

		glTranslated(donvi*myRobot.tx0, donvi*myRobot.ty0, donvi*myRobot.tz0);
		glCallList(displayLists[0]);
		for (int i=0; i < numLinks-1; i++)
		{
			Transform(i, q);
			glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, colors[i+1].getColor());
			glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, colors[i+1].getColor());
			glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ambient[i+1].getColor());
			glCallList(displayLists[1+i]);
		}
		glTranslatef(donvi*myRobot.xe, donvi*myRobot.ye, donvi*myRobot.ze);
	}
	void drawTrace(int i, matrixXd rE)
	{
		int nrow = rE.rows(), ncol = rE.cols();
		int ith = min(i, nrow-1);
		glBegin(GL_LINE_STRIP);
		glMaterialfv(GL_FRONT, GL_SPECULAR, colors[0].getColor());
		glMaterialfv(GL_FRONT, GL_DIFFUSE, colors[0].getColor());
		for (int j = 0; j < ith;j++)
		{
			double xe, ye, ze;
			xe = rE(j,0); ye = rE(j,1); ze = rE(j,2);
			xe *= donvi; ye *= donvi; ze *= donvi;
			glVertex3d(xe, ye, ze);
		}
		glEnd();
	}
};

} // end mdl
#endif //MDL_HEADER_H
