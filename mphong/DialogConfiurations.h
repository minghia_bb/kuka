#pragma once
#include "resource.h"

struct robotKukaData
{
    double tx1, ty1, tz1,
		tx2, ty2, tz2,
		tx3, ty3, tz3,
		tx4, ty4, tz4,
		tx5, ty5, tz5,
		tx6, ty6, tz6, rx6;
	double xE6, yE6, zE6;
	double m1, m2, m3, m4, m5, m6;
	double g1, g2, g3;
	double xCi1, yCi1, zCi1,
		xCi2, yCi2, zCi2,
		xCi3, yCi3, zCi3,
		xCi4, yCi4, zCi4,
		xCi5, yCi5, zCi5,
		xCi6, yCi6, zCi6;
	double Ixx1, Ixy1, Ixz1, Iyy1, Iyz1, Izz1,
		Ixx2, Ixy2, Ixz2, Iyy2, Iyz2, Izz2,
		Ixx3, Ixy3, Ixz3, Iyy3, Iyz3, Izz3,
		Ixx4, Ixy4, Ixz4, Iyy4, Iyz4, Izz4,
		Ixx5, Ixy5, Ixz5, Iyy5, Iyz5, Izz5,
		Ixx6, Ixy6, Ixz6, Iyy6, Iyz6, Izz6;
};

// DialogConfiurations dialog

class CDialogConfiurations : public CDialogEx
{
	DECLARE_DYNAMIC(CDialogConfiurations)

public:
	CDialogConfiurations(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDialogConfiurations();

// Dialog Data
	enum { IDD = IDD_DLG_CONFIGURATION };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
    int m_nNumOfLink;
    double m_dIxx;
    double m_dIxy;
    double m_dIxz;
    double m_dIyx;
    double m_dIyy;
    double m_dIyz;
    double m_dIzx;
    double m_dIzy;
    double m_dIzz;
    double m_dMass;
    double m_drCx;
    double m_drCy;
    double m_drCz;
    double m_dTx;
    double m_dTy;
    double m_dTz;
    double m_drx6;
    virtual BOOL OnInitDialog();
    struct robotKukaData data;
    double m_dxE;
    double m_dyE;
    double m_dzE;
    CEdit m_ctrlQuay;
    CStatic m_ctrlStaticQuay;
    void GetData(int nNumOfLink);
    int SetData(int nNumOfLink);
    afx_msg void OnBnClickedRadio123456();    
    afx_msg void OnBnClickedOk();
};
