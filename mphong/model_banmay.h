/*
*
*
* robot_h.h
*/
#ifndef ROBOT_H
#define ROBOT_H
#include "robotdata.h"
#include "Matrix4x4d.h"

/* Ban may 2 bac tu do
* Base -> Ban0 -> Tran(tx0, ty0, tz0)
* Ban0 -> Ban1 -> Tran(tx1,ty1,tz1), Rot(x, rx1), Rot(z,q1)
* Ban1 -> Ban2 -> Tran(tx2,ty2,tz2), Rot(x, -rx2), Rot(z,q2)
*/
namespace robotData {
class CBanmay: public Robot
{
public:
	// data
	double tx0, ty0, tz0;
	double tx1, ty1, tz1, rx1;
	double tx2, ty2, tz2, rx2;
	//
	double xe, ye, ze;		// diem thao tac trong htd khau cuoi
	double orx0, ory0, orz0;// huong cua 1vector nao do trong htd nen
	// 
	double m1, m2;
	double g1, g2, g3;
	double xCi1, yCi1, zCi1;
	double xCi2, yCi2, zCi2;
	double Ix1, Iy1, Iz1, Ixy1, Ixz1, Iyz1;
	double Ix2, Iy2, Iz2, Ixy2, Ixz2, Iyz2;
public:
	// methods
	CBanmay();
	~CBanmay() {}
	//
	void rE_RR(double q1, double q2, double cgret[3]);
	void vE_RR(double q1, double q2, double dq1, double dq2, double cgret[3]);
	void aE_RR(double q1, double q2, double dq1, double dq2, double ddq1, double ddq2, double cgret[3]);
	// dong hoc nguoc ve huong <return> <q1,q2>
	void dhn_huong1 (double orx, double ory, double orz, double cgret[2]);
	void dhn_huong2 (double orx, double ory, double orz, double cgret[2]);
	void dhn_huong (double orx, double ory, double orz, double cgret[2])
	{dhn_huong2(orx, ory, orz, cgret);}
	// dong hoc nguoc vi phan ve huong <return> <dq1,dq2>
	void dhn_viphan (double q1, double q2, double orx, double ory, double orz, double dorx, double dory, double dorz, double cgret[2]);
	// dong hoc nguoc vi phan ve huong <return> <ddq1,ddq2>
	void dhn_viphan2 (double q1, double q2, double orx, double ory, double orz, double dq1, double dq2, double dorx, double dory, double dorz, double ddorx, double ddory, double ddorz, double cgret[2]);
	//
	void M_RR(double q1, double q2, double cgret[2][2]);
	void V_RR(double q1, double q2, double dq1, double dq2, double cgret[2]);
};
} // end namespace
#endif //ROBOT_H
