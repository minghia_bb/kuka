/*******************************************************************************/
/* Le Minh Nghia KSTN CDT 54 *26/09/2013* **************************************/
/* Class CPlotView -   *********************************************************/
// Khoi tao moi truong OpenGL cho dialog Plot
// Chua du lieu cua do thi ham so (nhieu lines = PlotData)
// hien thi ra cua so Dialog ( CPlotDlg)
/*******************************************************************************/
#pragma once
#include "openglwnd.h"
#include "Plots.h"
namespace opengl
{
	namespace plot
	{
class CPlotView :
	public COpenGLWnd
{
public:
	int		m_nResize;
	bool	bCursorInfor;
private:
	CPlots	plots;
	int		xpos, ypos;
	//
	double xO, yO;	
	double scaleX, scaleY;
	bool	m_bLeftDown, m_bRightDown;
public:
	CPlotView(void);
	~CPlotView(void);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnPaint();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	DECLARE_MESSAGE_MAP()
public:
	void drawNumber(double num);
	void drawTic(double tic, double atXY, int XY, double big);
	void drawAxes(void);
	void drawPosition(void);
	void drawGrid(int xmark = 40, int ymark = 40);
	void VeDoThiHamSo(void);
	void InitialAxis(double min_x, double max_x, double min_y, double max_y);
	void InitialAxis(double min_x, double max_x);
	void InitialAxis();
	void max_min(std::vector<double> XY, double &minv, double &maxv);
	void FitWindow(void);
	std::string getCaption(void);
	void SetData(const CPlots& Plots);
	CPlots GetData(void);
	void SetBackGroundColor(COLORREF ref);
	void SetBackGroundColor(double r, double g, double b);
	COLORREF GetBackGroundColor(void);
	void SetDrawGridAxis(bool bGrid, bool bAxis);
};

	}}