// DialogConfiurations.cpp : implementation file
//

#include "stdafx.h"
#include "DialogConfiurations.h"
#include "afxdialogex.h"


// DialogConfiurations dialog

IMPLEMENT_DYNAMIC(CDialogConfiurations, CDialogEx)

CDialogConfiurations::CDialogConfiurations(CWnd* pParent /*=NULL*/)
	: CDialogEx(CDialogConfiurations::IDD, pParent)
{
    m_nNumOfLink = 0;
    m_dIxx = 0.0;
    m_dIxy = 0.0;
    m_dIxz = 0.0;
    m_dIyx = 0.0;
    m_dIyy = 0.0;
    m_dIyz = 0.0;
    m_dIzx = 0.0;
    m_dIzy = 0.0;
    m_dIzz = 0.0;
    m_dMass = 0.0;
    m_drCx = 0.0;
    m_drCy = 0.0;
    m_drCz = 0.0;
    m_dTx = 0.0;
    m_dTy = 0.0;
    m_dTz = 0.0;
    m_dxE = 0.0;
    m_dyE = 0.0;
    m_dzE = 0.0;
    m_drx6 = 0.0;
}

CDialogConfiurations::~CDialogConfiurations()
{
}

void CDialogConfiurations::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
    DDX_Radio(pDX, IDC_RADIO1, m_nNumOfLink);
    DDX_Text(pDX, IDC_IXX, m_dIxx);
    DDX_Text(pDX, IDC_IXY, m_dIxy);
    DDX_Text(pDX, IDC_IXZ, m_dIxz);
    DDX_Text(pDX, IDC_IYX, m_dIyx);
    DDX_Text(pDX, IDC_IYY, m_dIyy);
    DDX_Text(pDX, IDC_IYZ, m_dIyz);
    DDX_Text(pDX, IDC_IZX, m_dIzx);
    DDX_Text(pDX, IDC_IZY, m_dIzy);
    DDX_Text(pDX, IDC_IZZ, m_dIzz);
    DDX_Text(pDX, IDC_KHOILUONG, m_dMass);
    DDX_Text(pDX, IDC_rC_X, m_drCx);
    DDX_Text(pDX, IDC_rC_Y, m_drCy);
    DDX_Text(pDX, IDC_rC_Z, m_drCz);
    DDX_Text(pDX, IDC_TINHTIEN_X, m_dTx);
    DDX_Text(pDX, IDC_TINHTIEN_Y, m_dTy);
    DDX_Text(pDX, IDC_TINHTIEN_Z, m_dTz);
    DDX_Text(pDX, IDC_XE, m_dxE);
    DDX_Text(pDX, IDC_YE, m_dyE);
    DDX_Text(pDX, IDC_ZE, m_dzE);
    DDX_Control(pDX, IDC_QUAY, m_ctrlQuay);
    DDX_Control(pDX, IDC_STATIC_QUAY, m_ctrlStaticQuay);
    DDX_Text(pDX, IDC_QUAY, m_drx6);
}


BEGIN_MESSAGE_MAP(CDialogConfiurations, CDialogEx)
    ON_BN_CLICKED(IDC_RADIO1, &CDialogConfiurations::OnBnClickedRadio123456)
    ON_BN_CLICKED(IDC_RADIO2, &CDialogConfiurations::OnBnClickedRadio123456)
    ON_BN_CLICKED(IDC_RADIO3, &CDialogConfiurations::OnBnClickedRadio123456)
    ON_BN_CLICKED(IDC_RADIO4, &CDialogConfiurations::OnBnClickedRadio123456)
    ON_BN_CLICKED(IDC_RADIO5, &CDialogConfiurations::OnBnClickedRadio123456)
    ON_BN_CLICKED(IDC_RADIO6, &CDialogConfiurations::OnBnClickedRadio123456)
    ON_BN_CLICKED(IDOK, &CDialogConfiurations::OnBnClickedOk)
END_MESSAGE_MAP()


// DialogConfiurations message handlers


BOOL CDialogConfiurations::OnInitDialog()
{
    CDialogEx::OnInitDialog();

    // TODO:  Add extra initialization here
    this->m_dTx = data.tx1 ;this->m_dTy = data.ty1 ; this->m_dTz = data.tz1 ;    
    this->m_dxE = data.xE6 ; this->m_dyE = data.yE6; this->m_dzE = data.zE6;
    this->m_drCx = data.xCi1; this->m_drCy = data.yCi1; this->m_drCz = data.zCi1;
    this->m_dMass = data.m1;
    this->m_dIxx = data.Ixx1; 
    this->m_dIxy = this->m_dIyx = data.Ixy1;
    this->m_dIxz = this->m_dIzx = data.Ixz1;
    this->m_dIyy = data.Iyy1;
    this->m_dIyz = this->m_dIzy = data.Iyz1;
    this->m_dIzz = data.Izz1;
    UpdateData (0);
    return TRUE;  // return TRUE unless you set the focus to a control
    // EXCEPTION: OCX Property Pages should return FALSE
}

void CDialogConfiurations::GetData(int m_nOldNumOfLink)
{
    switch (m_nOldNumOfLink)
    {
    case 0:
        {            
            data.tx1 = this->m_dTx; data.ty1 = this->m_dTy; data.tz1 = this->m_dTz;
            data.xE6 = this->m_dxE; data.yE6 = this->m_dyE; data.zE6 = this->m_dzE;
            data.xCi1 = this->m_drCx; data.yCi1 = this->m_drCy; data.zCi1 = this->m_drCz;
            data.m1 = this->m_dMass;
            data.Ixx1 = this->m_dIxx; data.Ixy1 = this->m_dIxy; data.Ixz1 = this->m_dIxz; 
            data.Iyy1 = this->m_dIyy; data.Iyz1 = this->m_dIyz;
            data.Izz1 = this->m_dIzz;
            break;
        }
    case 1:
         {
             data.tx2 = this->m_dTx; data.ty2 = this->m_dTy; data.tz2 = this->m_dTz;
             data.xE6 = this->m_dxE; data.yE6 = this->m_dyE; data.zE6 = this->m_dzE;
             data.xCi2 = this->m_drCx; data.yCi2 = this->m_drCy; data.zCi2 = this->m_drCz;
             data.m2 = this->m_dMass;            
             data.Ixx2 = this->m_dIxx; data.Ixy2 = this->m_dIxy; data.Ixz2 = this->m_dIxz; 
             data.Iyy2 = this->m_dIyy; data.Iyz2 = this->m_dIyz;
             data.Izz2 = this->m_dIzz;
             break;
         }
    case 2:
         {
            data.tx3 = this->m_dTx; data.ty3 = this->m_dTy; data.tz3 = this->m_dTz;
            data.xE6 = this->m_dxE; data.yE6 = this->m_dyE; data.zE6 = this->m_dzE;
            data.xCi3 = this->m_drCx; data.yCi3 = this->m_drCy; data.zCi3 = this->m_drCz;
            data.m3 = this->m_dMass;
            data.Ixx3 = this->m_dIxx; data.Ixy3 = this->m_dIxy; data.Ixz3 = this->m_dIxz; 
            data.Iyy3 = this->m_dIyy; data.Iyz3 = this->m_dIyz;
            data.Izz3 = this->m_dIzz;
             break;
         }
    case 3:
        {
            data.tx4 = this->m_dTx; data.ty4 = this->m_dTy; data.tz4 = this->m_dTz;
            data.xE6 = this->m_dxE; data.yE6 = this->m_dyE; data.zE6 = this->m_dzE;
            data.xCi4 = this->m_drCx; data.yCi4 = this->m_drCy; data.zCi4 = this->m_drCz;
            data.m4 = this->m_dMass;
            data.Ixx4 = this->m_dIxx; data.Ixy4 = this->m_dIxy; data.Ixz4 = this->m_dIxz; 
            data.Iyy4 = this->m_dIyy; data.Iyz4 = this->m_dIyz;
            data.Izz4 = this->m_dIzz;
            break;
         }
    case 4:
         {
            data.tx5 = this->m_dTx; data.ty5 = this->m_dTy; data.tz5 = this->m_dTz;
            data.xE6 = this->m_dxE; data.yE6 = this->m_dyE; data.zE6 = this->m_dzE;
            data.xCi5 = this->m_drCx; data.yCi5 = this->m_drCy; data.zCi5 = this->m_drCz;
            data.m5 = this->m_dMass;
            data.Ixx5 = this->m_dIxx; data.Ixy5 = this->m_dIxy; data.Ixz5 = this->m_dIxz; 
            data.Iyy5 = this->m_dIyy; data.Iyz5 = this->m_dIyz;
            data.Izz5 = this->m_dIzz;
            break;
         }
    case 5:
        {
            data.tx6 = this->m_dTx; data.ty6 = this->m_dTy; data.tz6 = this->m_dTz;
            data.xE6 = this->m_dxE; data.yE6 = this->m_dyE; data.zE6 = this->m_dzE;
            data.xCi6 = this->m_drCx; data.yCi6 = this->m_drCy; data.zCi6 = this->m_drCz;
            data.m6 = this->m_dMass;
            data.Ixx6 = this->m_dIxx; data.Ixy6 = this->m_dIxy; data.Ixz6 = this->m_dIxz; 
            data.Iyy6 = this->m_dIyy; data.Iyz6 = this->m_dIyz;
            data.Izz6 = this->m_dIzz;         
            data.rx6 = this->m_drx6;
            break;
         }
    default:
        break;
    }

}

int CDialogConfiurations::SetData(int nNumOfLink)
{
    int m_nCmdShow = SW_HIDE;
    switch (nNumOfLink)
    {
    case 0:
        {
            this->m_dTx = data.tx1 ;this->m_dTy = data.ty1 ; this->m_dTz = data.tz1 ;    
            this->m_dxE = data.xE6 ; this->m_dyE = data.yE6; this->m_dzE = data.zE6;
            this->m_drCx = data.xCi1; this->m_drCy = data.yCi1; this->m_drCz = data.zCi1;
            this->m_dMass = data.m1;
            this->m_dIxx = data.Ixx1; this->m_dIxy = this->m_dIyx = data.Ixy1; this->m_dIxz = this->m_dIzx = data.Ixz1;
            this->m_dIyy = data.Iyy1; this->m_dIyz = this->m_dIzy = data.Iyz1;
            this->m_dIzz = data.Izz1;
            break;
        }
    case 1:
        {
            this->m_dTx = data.tx2 ;this->m_dTy = data.ty2 ; this->m_dTz = data.tz2 ;    
            this->m_dxE = data.xE6 ; this->m_dyE = data.yE6; this->m_dzE = data.zE6;
            this->m_drCx = data.xCi2; this->m_drCy = data.yCi2; this->m_drCz = data.zCi2;
            this->m_dMass = data.m2;
            this->m_dIxx = data.Ixx2; this->m_dIxy = this->m_dIyx = data.Ixy2; this->m_dIxz = this->m_dIzx = data.Ixz2;
            this->m_dIyy = data.Iyy2; this->m_dIyz = this->m_dIzy = data.Iyz2;
            this->m_dIzz = data.Izz2;
            break;
        }
    case 2:
        {
            this->m_dTx = data.tx3 ;this->m_dTy = data.ty3 ; this->m_dTz = data.tz3 ;    
            this->m_dxE = data.xE6 ; this->m_dyE = data.yE6; this->m_dzE = data.zE6;
            this->m_drCx = data.xCi3; this->m_drCy = data.yCi3; this->m_drCz = data.zCi3;
            this->m_dMass = data.m3;
            this->m_dIxx = data.Ixx3; this->m_dIxy = this->m_dIyx = data.Ixy3; this->m_dIxz = this->m_dIzx = data.Ixz3;
            this->m_dIyy = data.Iyy3; this->m_dIyz = this->m_dIzy = data.Iyz3;
            this->m_dIzz = data.Izz3;
            break;
        }
    case 3:
        {
            this->m_dTx = data.tx4 ;this->m_dTy = data.ty4 ; this->m_dTz = data.tz4 ;    
            this->m_dxE = data.xE6 ; this->m_dyE = data.yE6; this->m_dzE = data.zE6;
            this->m_drCx = data.xCi4; this->m_drCy = data.yCi4; this->m_drCz = data.zCi4;
            this->m_dMass = data.m4;
            this->m_dIxx = data.Ixx4; this->m_dIxy = this->m_dIyx = data.Ixy4; this->m_dIxz = this->m_dIzx = data.Ixz4;
            this->m_dIyy = data.Iyy4; this->m_dIyz = this->m_dIzy = data.Iyz4;
            this->m_dIzz = data.Izz4;
            break;
        }
    case 4:
        {
            this->m_dTx = data.tx5 ;this->m_dTy = data.ty5 ; this->m_dTz = data.tz5 ;    
            this->m_dxE = data.xE6 ; this->m_dyE = data.yE6; this->m_dzE = data.zE6;
            this->m_drCx = data.xCi5; this->m_drCy = data.yCi5; this->m_drCz = data.zCi5;
            this->m_dMass = data.m5;
            this->m_dIxx = data.Ixx5; this->m_dIxy = this->m_dIyx = data.Ixy5; this->m_dIxz = this->m_dIzx = data.Ixz5;
            this->m_dIyy = data.Iyy5; this->m_dIyz = this->m_dIzy = data.Iyz5;
            this->m_dIzz = data.Izz5;
            break;
        }
    case 5:
        {
            this->m_dTx = data.tx6 ;this->m_dTy = data.ty6 ; this->m_dTz = data.tz6 ;    
            this->m_drx6 = data.rx6;
            this->m_dxE = data.xE6 ; this->m_dyE = data.yE6; this->m_dzE = data.zE6;
            this->m_drCx = data.xCi6; this->m_drCy = data.yCi6; this->m_drCz = data.zCi6;
            this->m_dMass = data.m6;
            this->m_dIxx = data.Ixx6; this->m_dIxy = this->m_dIyx = data.Ixy6; this->m_dIxz = this->m_dIzx = data.Ixz6;
            this->m_dIyy = data.Iyy6; this->m_dIyz = this->m_dIzy = data.Iyz6;
            this->m_dIzz = data.Izz6;
            m_nCmdShow = SW_SHOW;
            break;
        }
    default:
        break;
    }
    return m_nCmdShow;
}
    
void CDialogConfiurations::OnBnClickedRadio123456()
{
    // TODO: Add your control notification handler code here
    int m_nOldNumOfLink = m_nNumOfLink;
    UpdateData(1);
    int m_nCmdShow = SW_HIDE;
    GetData(m_nOldNumOfLink);
    m_nCmdShow = SetData(m_nNumOfLink);
    UpdateData(0);
    this->m_ctrlStaticQuay.ShowWindow(m_nCmdShow);
    this->m_ctrlQuay.ShowWindow(m_nCmdShow);
}


void CDialogConfiurations::OnBnClickedOk()
{
    // TODO: Add your control notification handler code here
    UpdateData(1);    
    GetData(m_nNumOfLink);
    CDialogEx::OnOK();
}
