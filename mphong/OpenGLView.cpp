#include "stdafx.h"
#include "OpenGLView.h"

namespace opengl
{
COpenGLView::COpenGLView(void) : m_Objects()
{
	Reset();
	m_bInitialation = false;
	m_nTimerID = 10;
	m_nTimerElapsed = 10;
	m_Scene = m_Objects.m_Scene;
}


COpenGLView::~COpenGLView(void)
{
}

// reset ve trang thai ban dau
void COpenGLView::Reset()
{
	m_dZooming = 0.0;
	m_v3dScale = Vec3D(1.0, 1.0, 1.0);
	m_v3dTranslate = Vec3D();
	m_v3dRotate = Vec3D();
	m_bLMouseDown = false;
	m_bRMouseDown = false;
	m_bMMouseDown = false;
	m_v3d_Up = m_Scene.m_v3dUp;
	m_v3d_Out = m_Scene.m_v3dEye.Normalized();
	m_v3d_Left = m_v3d_Up.Cross(m_v3d_Out);
}

void COpenGLView::OnPaint(void)
{
	Vec3D m_v3dZoom = m_v3d_Out * m_dZooming;
	if (m_Scene.m_bPerspective)
	{
		glTranslatef(m_v3dZoom.x, m_v3dZoom.y, m_v3dZoom.z);
	}
	else
		glScalef(m_v3dScale.x, m_v3dScale.y, m_v3dScale.z);
	Vec3D
		m_v3dTrans = m_v3dTranslate.y * m_v3d_Left + m_v3dTranslate.x * m_v3d_Up;
	glTranslatef(m_v3dTrans.x, m_v3dTrans.y, m_v3dTrans.z);
	glRotatef(m_v3dRotate.x, m_v3d_Left.x, m_v3d_Left.y, m_v3d_Left.z);
	glRotatef(m_v3dRotate.y, m_v3d_Up.x, m_v3d_Up.y, m_v3d_Up.z);
}


void COpenGLView::OnSize(UINT nType, int cx, int cy)
{
	m_nHeight = cy, m_nWidth = cx;
	glViewport(0, 0, m_nWidth, m_nHeight);
	if (cx <=0 || cy <= 0 || nType == SIZE_MINIMIZED)
		return;
	float aspect;
	aspect= (float)m_nWidth / (float)m_nHeight;
	glMatrixMode(GL_PROJECTION); 
	glLoadIdentity();							// Start using Projection Matrix
	gluPerspective(m_Scene.m_dFovy, aspect, m_Scene.m_dNear, m_Scene.m_dFar);
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();							// Return to Model Matrix
	gluLookAt(m_Scene.m_v3dEye.x, m_Scene.m_v3dEye.y, m_Scene.m_v3dEye.z,
		m_Scene.m_v3dCenter.x, m_Scene.m_v3dCenter.y, m_Scene.m_v3dCenter.z,
		m_Scene.m_v3dUp.x, m_Scene.m_v3dUp.y, m_Scene.m_v3dUp.z);
	if (!m_bInitialation)
	{
		OnViewPerspective();
		m_bInitialation = true;
	}
}


void COpenGLView::drawAxis(double lengthAxis)
{
	glPushMatrix();
	if (m_Scene.m_bAxis)
	{
		glDisable(GL_LIGHTING);
		glDisable(GL_LIGHT0);
		// x
		glColor3f(1.0, 0.0, 0.0);
		glLineWidth(3.0);
		glBegin(GL_LINES);
		glVertex3f(0.0, 0.0, 0.0);
		glVertex3f(lengthAxis, 0.0, 0.0);
		// y
		glColor3f(0.0, 1.0, 0.0);
		glVertex3f(0.0, 0.0, 0.0);
		glVertex3f(0.0, lengthAxis, 0.0);
		// z
		glColor3f(0.0, 0.0, 1.0);
		glVertex3f(0.0, 0.0, 0.0);
		glVertex3f(0.0, 0.0, lengthAxis);
		glEnd();	
		if (m_Scene.m_bLight)
		{
			glEnable(GL_LIGHTING);
			glEnable(GL_LIGHT0);
		}
	}
	glPopMatrix();
}

void COpenGLView::OnViewTop(void)
{
	double m_dMaxEye;
	m_dMaxEye = ((abs(m_Scene.m_v3dEye.x) >= abs(m_Scene.m_v3dEye.y)) ? abs(m_Scene.m_v3dEye.x) : abs(m_Scene.m_v3dEye.y));
	m_dMaxEye = ((m_dMaxEye >= abs(m_Scene.m_v3dEye.z)) ? m_dMaxEye : abs(m_Scene.m_v3dEye.z));
	double x = 350, z = -250;
	z = m_Scene.m_pCenter.z;
	x = m_Scene.m_pCenter.x;
	m_Scene.m_v3dEye = Vec3D(x, m_dMaxEye, z);
	m_Scene.m_v3dCenter = Vec3D(x, 0, z);
	m_Scene.m_v3dUp = Vec3D(0.0, 0.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(m_Scene.m_v3dEye.x, m_Scene.m_v3dEye.y, m_Scene.m_v3dEye.z,
			m_Scene.m_v3dCenter.x, m_Scene.m_v3dCenter.y, m_Scene.m_v3dCenter.z,
			m_Scene.m_v3dUp.x, m_Scene.m_v3dUp.y, m_Scene.m_v3dUp.z);
	Reset();
	SendMessage(hWnd, WM_PAINT, 0, 0);
}


void COpenGLView::OnViewBottom(void)
{
	double m_dMaxEye;
	m_dMaxEye = ((abs(m_Scene.m_v3dEye.x) >= abs(m_Scene.m_v3dEye.y)) ? abs(m_Scene.m_v3dEye.x) : abs(m_Scene.m_v3dEye.y));
	m_dMaxEye = ((m_dMaxEye >= abs(m_Scene.m_v3dEye.z)) ? m_dMaxEye : abs(m_Scene.m_v3dEye.z));
	double x, z;
	z = m_Scene.m_pCenter.z;
	x = m_Scene.m_pCenter.x;
	m_Scene.m_v3dEye = Vec3D(x, -m_dMaxEye, z);
	m_Scene.m_v3dCenter = Vec3D(x, 0, z);
	m_Scene.m_v3dUp = Vec3D(0.0, 0.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(m_Scene.m_v3dEye.x, m_Scene.m_v3dEye.y, m_Scene.m_v3dEye.z,
			m_Scene.m_v3dCenter.x, m_Scene.m_v3dCenter.y, m_Scene.m_v3dCenter.z,
			m_Scene.m_v3dUp.x, m_Scene.m_v3dUp.y, m_Scene.m_v3dUp.z);
	Reset();
	SendMessage(hWnd, WM_PAINT, 0, 0);
}


void COpenGLView::OnViewLeft(void)
{
	double m_dMaxEye;
	m_dMaxEye = ((abs(m_Scene.m_v3dEye.x) >= abs(m_Scene.m_v3dEye.y)) ? abs(m_Scene.m_v3dEye.x) : abs(m_Scene.m_v3dEye.y));
	m_dMaxEye = ((m_dMaxEye >= abs(m_Scene.m_v3dEye.z)) ? m_dMaxEye : abs(m_Scene.m_v3dEye.z));
	double y,z;
	z = m_Scene.m_pCenter.z;
	y = m_Scene.m_pCenter.y;
	m_Scene.m_v3dEye = Vec3D(-m_dMaxEye, y,z);
	m_Scene.m_v3dCenter = Vec3D(0,y,z);
	m_Scene.m_v3dUp = Vec3D(0.0, 1.0, 0.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(m_Scene.m_v3dEye.x, m_Scene.m_v3dEye.y, m_Scene.m_v3dEye.z,
			m_Scene.m_v3dCenter.x, m_Scene.m_v3dCenter.y, m_Scene.m_v3dCenter.z,
			m_Scene.m_v3dUp.x, m_Scene.m_v3dUp.y, m_Scene.m_v3dUp.z);
	Reset();
	SendMessage(hWnd, WM_PAINT, 0, 0);
}


void COpenGLView::OnViewRight(void)
{
	double m_dMaxEye;
	m_dMaxEye = ((abs(m_Scene.m_v3dEye.x) >= abs(m_Scene.m_v3dEye.y)) ? abs(m_Scene.m_v3dEye.x) : abs(m_Scene.m_v3dEye.y));
	m_dMaxEye = ((m_dMaxEye >= abs(m_Scene.m_v3dEye.z)) ? m_dMaxEye : abs(m_Scene.m_v3dEye.z));
	double y, z;
	z = m_Scene.m_pCenter.z;
	y = m_Scene.m_pCenter.y;
	m_Scene.m_v3dEye = Vec3D(m_dMaxEye, y,z);
	m_Scene.m_v3dCenter = Vec3D(0,y,z);
	m_Scene.m_v3dUp = Vec3D(0.0, 1.0, 0.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(m_Scene.m_v3dEye.x, m_Scene.m_v3dEye.y, m_Scene.m_v3dEye.z,
			m_Scene.m_v3dCenter.x, m_Scene.m_v3dCenter.y, m_Scene.m_v3dCenter.z,
			m_Scene.m_v3dUp.x, m_Scene.m_v3dUp.y, m_Scene.m_v3dUp.z);
	Reset();
	SendMessage(hWnd, WM_PAINT, 0, 0);
}


void COpenGLView::OnViewFront(void)
{
	double m_dMaxEye;
	m_dMaxEye = ((abs(m_Scene.m_v3dEye.x) >= abs(m_Scene.m_v3dEye.y)) ? abs(m_Scene.m_v3dEye.x) : abs(m_Scene.m_v3dEye.y));
	m_dMaxEye = ((m_dMaxEye >= abs(m_Scene.m_v3dEye.z)) ? m_dMaxEye : abs(m_Scene.m_v3dEye.z));
	double x,y;
	x = m_Scene.m_pCenter.x;
	y = m_Scene.m_pCenter.y;
	m_Scene.m_v3dEye = Vec3D(x, y, m_dMaxEye);
	m_Scene.m_v3dCenter = Vec3D(x, y , 0);
	m_Scene.m_v3dUp = Vec3D(0.0, 1.0, 0.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(m_Scene.m_v3dEye.x, m_Scene.m_v3dEye.y, m_Scene.m_v3dEye.z,
			m_Scene.m_v3dCenter.x, m_Scene.m_v3dCenter.y, m_Scene.m_v3dCenter.z,
			m_Scene.m_v3dUp.x, m_Scene.m_v3dUp.y, m_Scene.m_v3dUp.z);
	Reset();
	SendMessage(hWnd, WM_PAINT, 0, 0);
}


void COpenGLView::OnViewBack(void)
{
	double m_dMaxEye;
	m_dMaxEye = ((abs(m_Scene.m_v3dEye.x) >= abs(m_Scene.m_v3dEye.y)) ? abs(m_Scene.m_v3dEye.x) : abs(m_Scene.m_v3dEye.y));
	m_dMaxEye = ((m_dMaxEye >= abs(m_Scene.m_v3dEye.z)) ? m_dMaxEye : abs(m_Scene.m_v3dEye.z));
	double x,y;
	x = m_Scene.m_pCenter.x;
	y = m_Scene.m_pCenter.y;
	m_Scene.m_v3dEye = Vec3D(x, y, -m_dMaxEye);
	m_Scene.m_v3dCenter = Vec3D(x, y , 0);
	m_Scene.m_v3dUp = Vec3D(0.0, 1.0, 0.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(m_Scene.m_v3dEye.x, m_Scene.m_v3dEye.y, m_Scene.m_v3dEye.z,
			m_Scene.m_v3dCenter.x, m_Scene.m_v3dCenter.y, m_Scene.m_v3dCenter.z,
			m_Scene.m_v3dUp.x, m_Scene.m_v3dUp.y, m_Scene.m_v3dUp.z);
	Reset();
	SendMessage(hWnd, WM_PAINT, 0, 0);
}


void COpenGLView::OnViewPerspective(void)
{
	m_Scene.m_v3dEye = m_Objects.m_Scene.m_v3dEye;
	m_Scene.m_v3dCenter = m_Objects.m_Scene.m_v3dCenter;//Vec3D(0.0, 10.0, 0.0);
	m_Scene.m_v3dUp = m_Objects.m_Scene.m_v3dUp;//Vec3D(0.0, 1.0, 0.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(m_Scene.m_v3dEye.x, m_Scene.m_v3dEye.y, m_Scene.m_v3dEye.z,
			m_Scene.m_v3dCenter.x, m_Scene.m_v3dCenter.y, m_Scene.m_v3dCenter.z,
			m_Scene.m_v3dUp.x, m_Scene.m_v3dUp.y, m_Scene.m_v3dUp.z);
	Reset();
	SendMessage(hWnd, WM_PAINT, 0, 0);
}
}