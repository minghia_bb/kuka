/*
	(c) by Le Minh Nghia - KSTN CDT k54 - HUST - 26/9/2013
	Khoi tao moi truong OpenGL cho Class CMpView va mo phong
*/

#pragma once
#include "OpenGL.h"
#include "Scene.h"
#include "model.h"
#include "model_kuka.h"

namespace opengl
{
class COpenGLView :
	public COpenGL
{
public:
	CScene					m_Scene;			
	int						m_nTimerID;			
	int						m_nTimerElapsed;	
	bool					m_bInitialation;
	//mdl::CmdlRRR			m_Objects;
	//mdl::CmdlBanmay			m_Objects;
	mdl::CmdlKuka			m_Objects;
public:
	double	m_dZooming;				
	Vec3D	m_v3dScale;				
	Vec3D	m_v3dTranslate;			
	Vec3D	m_v3dRotate;			
	CPoint	m_pOldMouse;			
	bool m_bLMouseDown,				
		m_bRMouseDown,				
		m_bMMouseDown;				
private:
	Vec3D	m_v3d_Left, m_v3d_Up, m_v3d_Out;
public:
	COpenGLView(void);
	~COpenGLView(void);
	void Reset();
	void OnPaint(void);
	void OnSize(UINT nType, int cx, int cy);
	void drawAxis(double lengthAxis = 100.0);
	void OnViewTop(void);
	void OnViewBottom(void);
	void OnViewLeft(void);
	void OnViewRight(void);
	void OnViewFront(void);
	void OnViewBack(void);
	void OnViewPerspective(void);
};
}
