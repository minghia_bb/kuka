
// mophongView.cpp : implementation of the CMpView class
//

#include "stdafx.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
#include "mphong.h"
#endif

#include "mophongDoc.h"
#include "mophongView.h"
/* Mycode begins*/
#include "MainFrm.h"
/* Mycode ends*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMpView

IMPLEMENT_DYNCREATE(CMpView, CView)

BEGIN_MESSAGE_MAP(CMpView, CView)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_ERASEBKGND()
	ON_WM_PAINT()
	ON_WM_SIZE()
	ON_WM_TIMER()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_RBUTTONDOWN()
	ON_WM_RBUTTONUP()
	ON_WM_MBUTTONDOWN()
	ON_WM_MBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()

// CMpView construction/destruction

CMpView::CMpView() : i_th(-1)
{
	// TODO: add construction code here
}

CMpView::~CMpView()
{
}

BOOL CMpView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs
	cs.style |= WS_CLIPSIBLINGS | WS_CLIPCHILDREN;
	return CView::PreCreateWindow(cs);
}

// CMpView drawing

void CMpView::OnDraw(CDC* /*pDC*/)
{
	CMpDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: add draw code for native data here
}


// CMpView diagnostics

#ifdef _DEBUG
void CMpView::AssertValid() const
{
	CView::AssertValid();
}

void CMpView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CMpDoc* CMpView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMpDoc)));
	return (CMpDoc*)m_pDocument;
}
#endif //_DEBUG


// CMpView message handlers


int CMpView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CView::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here

	/* MyCode Begins*/
	GetDocument()->m_OpenGLView.hWnd = m_hWnd;
	GetDocument()->m_OpenGLView.OnCreate(::GetDC(m_hWnd));
	//
	GetDocument()->m_OpenGLView.m_Objects.Create();
	// setting viewing
	GetDocument()->m_OpenGLView.m_Scene.m_dFovy = 18.0;
	/* MyCode Ends*/

	return 0;
}


void CMpView::OnDestroy()
{
	CView::OnDestroy();

	// TODO: Add your message handler code here
	/* MyCodes Begin*/
	GetDocument()->m_OpenGLView.OnDestroy();
	/* MyCodes End*/
}


BOOL CMpView::OnEraseBkgnd(CDC* pDC)
{
	// TODO: Add your message handler code here and/or call default

	//return CView::OnEraseBkgnd(pDC);
	/* MyCodes Begin*/
	return TRUE;
	/* MyCodes End*/
}

/* Ve cac doi tuong here */
void CMpView::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: Add your message handler code here
	// Do not call CView::OnPaint() for painting messages
	glClearColor(GetDocument()->m_OpenGLView.m_Scene.m_v3dColorBgrd.x, 
		GetDocument()->m_OpenGLView.m_Scene.m_v3dColorBgrd.y, 
		GetDocument()->m_OpenGLView.m_Scene.m_v3dColorBgrd.z, 
		GetDocument()->m_OpenGLView.m_Scene.m_dAlpha);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//
	glPushMatrix();
		GetDocument()->m_OpenGLView.OnPaint();			// xu li chuot
		glRotatef(-90.0, 1.0, 0.0, 0.0);				// quay quanh x 1 goc -90, z - is up
		glRotatef(-100, 0,0,1);
		GetDocument()->m_OpenGLView.drawAxis();
		glPushMatrix();
		if (i_th < 0)
		{
			qi = vectorXd(GetDocument()->m_OpenGLView.m_Objects.numLinks - 1);
			qi.fill(0.0);
		}
		GetDocument()->m_OpenGLView.m_Objects.draw(qi);
		GetDocument()->m_OpenGLView.drawAxis(20);
		glPopMatrix();
		if (i_th > 0 && GetDocument()->m_q.rows() > 5 && i_th < GetDocument()->m_q.rows())
			GetDocument()->m_OpenGLView.m_Objects.drawTrace(i_th, GetDocument()->m_rE, GetDocument()->m_OpenGLView.m_Scene.m_bLight);
	glPopMatrix();
	// Double buffer
	//glFlush();
	SwapBuffers(GetDocument()->m_OpenGLView.hDC);
}


void CMpView::OnSize(UINT nType, int cx, int cy)
{
	CView::OnSize(nType, cx, cy);

	// TODO: Add your message handler code here
	GetDocument()->m_OpenGLView.OnSize(nType, cx, cy);
}


void CMpView::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: Add your message handler code here and/or call default
	if (nIDEvent == GetDocument()->m_OpenGLView.m_nTimerID)
	{
		if (GetDocument()->m_q.rows() < 5)
			return;
		i_th++;
		if (i_th >= GetDocument()->m_q.rows())
			i_th = 0;
		else
			qi = GetDocument()->m_q.row(i_th);
		SendMessage(WM_PAINT, 0, 0);
	}
	CView::OnTimer(nIDEvent);
}


void CMpView::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	GetDocument()->m_OpenGLView.m_bLMouseDown = true;	// chuot trai duoc an
	GetDocument()->m_OpenGLView.m_pOldMouse = point;
	CView::OnLButtonDown(nFlags, point);
}


void CMpView::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	GetDocument()->m_OpenGLView.m_bLMouseDown = false;		// chuot trai duoc nha ra
	CView::OnLButtonUp(nFlags, point);
}


void CMpView::OnRButtonDown(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	GetDocument()->m_OpenGLView.m_bRMouseDown = true;
	GetDocument()->m_OpenGLView.m_pOldMouse = point;
	CView::OnRButtonDown(nFlags, point);
}


void CMpView::OnRButtonUp(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	GetDocument()->m_OpenGLView.m_bRMouseDown = false;
	CView::OnRButtonUp(nFlags, point);
}


void CMpView::OnMButtonDown(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	GetDocument()->m_OpenGLView.m_bMMouseDown = true;
	CView::OnMButtonDown(nFlags, point);
}


void CMpView::OnMButtonUp(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	GetDocument()->m_OpenGLView.m_bMMouseDown = false;
	CView::OnMButtonUp(nFlags, point);
}


void CMpView::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	/* Mycodes Begin */
	// an 2 chuot de zooming
	if (GetDocument()->m_OpenGLView.m_bLMouseDown && GetDocument()->m_OpenGLView.m_bRMouseDown)
	{
		if (GetDocument()->m_OpenGLView.m_Scene.m_bPerspective)
		{
			GetDocument()->m_OpenGLView.m_dZooming += (point.y - GetDocument()->m_OpenGLView.m_pOldMouse.y) / 10.0;
		}
		else
		{
			double m_dScale = (point.y - GetDocument()->m_OpenGLView.m_pOldMouse.y) / 10.0;
			if (m_dScale != 0.0)
				GetDocument()->m_OpenGLView.m_v3dScale += opengl::Vec3D(m_dScale);
			if (GetDocument()->m_OpenGLView.m_v3dScale.x <= 0.0001)  // scale < 0
				GetDocument()->m_OpenGLView.m_v3dScale = opengl::Vec3D(0.01);
		}
		GetDocument()->m_OpenGLView.m_pOldMouse = point;
		SendMessage(WM_PAINT, 0, 0);
		return;
	}
	// rotation
	if (GetDocument()->m_OpenGLView.m_bLMouseDown && GetDocument()->m_OpenGLView.m_Scene.m_bPerspective)
	{
		if (abs(GetDocument()->m_OpenGLView.m_v3dRotate.x) >= 360.0)
			GetDocument()->m_OpenGLView.m_v3dRotate.x = 0.0;
		if (abs(GetDocument()->m_OpenGLView.m_v3dRotate.y) >= 360.0)
			GetDocument()->m_OpenGLView.m_v3dRotate.y = 0.0;
		GetDocument()->m_OpenGLView.m_v3dRotate.x += (point.y - GetDocument()->m_OpenGLView.m_pOldMouse.y) / 10.0;
		GetDocument()->m_OpenGLView.m_v3dRotate.y += (point.x - GetDocument()->m_OpenGLView.m_pOldMouse.x) / 10.0;
		// update position of mouse
		GetDocument()->m_OpenGLView.m_pOldMouse = point;
		SendMessage(WM_PAINT, 0, 0);
	}
	// moving
	if (GetDocument()->m_OpenGLView.m_bRMouseDown)
	{
		GetDocument()->m_OpenGLView.m_v3dTranslate.x += (-point.y + GetDocument()->m_OpenGLView.m_pOldMouse.y) / 10.0;
		GetDocument()->m_OpenGLView.m_v3dTranslate.y += (point.x - GetDocument()->m_OpenGLView.m_pOldMouse.x) / 10.0;
		// update position of mouse
		GetDocument()->m_OpenGLView.m_pOldMouse = point;
		SendMessage(WM_PAINT, 0, 0);
	}
	
	CMpApp* pApp=(CMpApp*) AfxGetApp();
	CMainFrame* pMainFrame=(CMainFrame*) pApp->m_pMainWnd;
	CStatusBar* pStatus= &pMainFrame->m_wndStatusBar;
	CString str;
	str.Format(_T("X: %d"),point.x);
	pStatus->SetPaneText(1, str);
	str.Format(_T("Y: %d"),point.y);
	pStatus->SetPaneText(2, str);
	/* Mycodes End */
	CView::OnMouseMove(nFlags, point);
}

BOOL CMpView::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	// TODO: Add your message handler code here and/or call default
	// zooming for perspective
	if (GetDocument()->m_OpenGLView.m_Scene.m_bPerspective)
	{
		GetDocument()->m_OpenGLView.m_dZooming += (float) zDelta / 10.0;
	}
	else	// zooming for othorgonal
	{
		double m_dScale = zDelta / 100.0;
		if (m_dScale > 0)
		{
			GetDocument()->m_OpenGLView.m_v3dScale /= opengl::Vec3D(m_dScale);
		}
		else
		{
			GetDocument()->m_OpenGLView.m_v3dScale *= opengl::Vec3D(-m_dScale);
		}
	}
	SendMessage(WM_PAINT, 0, 0);
	return CView::OnMouseWheel(nFlags, zDelta, pt);
}
