/*
*
*
*
* robot_cpp.cpp
*/
#include "stdafx.h" // precomplied header
#include "model_banmay.h"
#include <math.h>

// Default 
namespace robotData {
CBanmay::CBanmay()
{
	tx0 = 0.300; ty0 = 0.090; tz0 = 0.000; //[m]
	tx1 = 0.100; ty1 = 0.160; tz1 = 0.120; //[m]
	rx1 = PI/2;
	tx2 = 0.000; ty2 = 0.055; tz2 = 0.000; //[m]
	rx2 = -PI/2;
	//
	xe = 0.010; ye = 0.010; ze = 0.010;  //[m]
	orx0 = ory0 = 1e-9; orz0 = 1.0;
	//
	m1 = 6.1854; m2 = 1.3572;
	g1 = g2 = 0.0; g3 = -9.81;
	xCi1 = 0.0; yCi1 = -0.01588; zCi1 = 0.0;
	xCi2 = 0.0; yCi2 = 0.0; zCi2 = -0.0091;
	Ix1 = 0.5309021e-1; Iy1 = 0.5739411e-1; Iz1 = 0.2863782e-1;
	Ixy1 = Ixz1 = Iyz1 = 0.0;
	Ix2 = 0.513011e-2; Iy2 = 0.513011e-2; Iz2 = 0.630321e-2;
	Ixy2 = Ixz2 = Iyz2 = 0.0;
}

void CBanmay::rE_RR (double q1, double q2, double cgret[3])
{
  double t16;
  double t2;
  double t21;
  double t22;
  double t24;
  double t25;
  double t27;
  double t28;
  double t3;
  double t33;
  double t45;
  double t47;
  double t49;
  double t5;
  double t6;
  double t7;
  double t8;
  double A[3];
  A[0] = 0;
  A[1] = 0;
  A[2] = 0;
  t2 = cos(q1);
  t3 = cos(q2);
  t5 = sin(q1);
  t6 = cos(rx2);
  t7 = t5 * t6;
  t8 = sin(q2);
  t16 = sin(rx2);
  A[0] = (t2 * t3 - t7 * t8) * xe + (-t2 * t8 - t7 * t3) * ye + t5 * t16 * ze + t2 * tx2 - t5 * ty2 + tx1 + tx0;
  t21 = cos(rx1);
  t22 = t21 * t5;
  t24 = t2 * t21;
  t25 = t6 * t8;
  t27 = sin(rx1);
  t28 = t27 * t16;
  t33 = t6 * t3;
  A[1] = (t22 * t3 + t24 * t25 - t28 * t8) * xe + (-t22 * t8 + t24 * t33 - t28 * t3) * ye + (-t24 * t16 - t27 * t6) * ze + t22 * tx2 + t24 * ty2 - t27 * tz2 + ty1 + ty0;
  t45 = t27 * t5;
  t47 = t27 * t2;
  t49 = t21 * t16;
  A[2] = (t45 * t3 + t47 * t25 + t49 * t8) * xe + (-t45 * t8 + t47 * t33 + t49 * t3) * ye + (-t47 * t16 + t21 * t6) * ze + t45 * tx2 + t47 * ty2 + t21 * tz2 + tz1 + tz0;
  cgret[0] = A[0];
  cgret[1] = A[1];
  cgret[2] = A[2];
}


void CBanmay::vE_RR (
  double q1,
  double q2,
  double dq1,
  double dq2,
  double cgret[3])
{
 double t16;
  double t2;
  double t24;
  double t3;
  double t34;
  double t35;
  double t37;
  double t38;
  double t43;
  double t47;
  double t5;
  double t55;
  double t56;
  double t6;
  double t67;
  double t69;
  double t7;
  double t8;
  double t84;
  double A[3];
  A[0] = 0;
  A[1] = 0;
  A[2] = 0;
  t2 = sin(q1);
  t3 = cos(q2);
  t5 = cos(q1);
  t6 = cos(rx2);
  t7 = t5 * t6;
  t8 = sin(q2);
  t16 = sin(rx2);
  t24 = t2 * t6;
  A[0] = ((-t2 * t3 - t7 * t8) * xe + (t2 * t8 - t3 * t7) * ye + t5 * t16 * ze - t2 * tx2 - t5 * ty2) * dq1 + ((-t5 * t8 - t24 * t3) * xe + (-t5 * t3 + t24 * t8) * ye) * dq2;
  t34 = cos(rx1);
  t35 = t34 * t5;
  t37 = t34 * t2;
  t38 = t6 * t8;
  t43 = t6 * t3;
  t47 = t16 * ze;
  t55 = sin(rx1);
  t56 = t55 * t16;
  A[1] = ((t35 * t3 - t37 * t38) * xe + (-t35 * t8 - t37 * t43) * ye + t37 * t47 + t35 * tx2 - t37 * ty2) * dq1 + ((-t37 * t8 + t35 * t43 - t56 * t3) * xe + (-t3 * t37 - t35 * t38 + t56 * t8) * ye) * dq2;
  t67 = t55 * t5;
  t69 = t55 * t2;
  t84 = t34 * t16;
  A[2] = ((t67 * t3 - t69 * t38) * xe + (-t67 * t8 - t69 * t43) * ye + t69 * t47 + t67 * tx2 - t69 * ty2) * dq1 + ((-t69 * t8 + t67 * t43 + t84 * t3) * xe + (-t69 * t3 - t67 * t38 - t84 * t8) * ye) * dq2;
  cgret[0] = A[0];
  cgret[1] = A[1];
  cgret[2] = A[2];
}


void CBanmay::aE_RR (
  double q1,
  double q2,
  double dq1,
  double dq2,
  double ddq1,
  double ddq2,
  double cgret[3])
{
   double t10;
  double t11;
  double t14;
  double t15;
  double t16;
  double t2;
  double t24;
  double t26;
  double t3;
  double t30;
  double t32;
  double t5;
  double t55;
  double t56;
  double t57;
  double t58;
  double t59;
  double t6;
  double t60;
  double t63;
  double t64;
  double t65;
  double t68;
  double t7;
  double t76;
  double t8;
  double t80;
  double t82;
  double t87;
  double t88;
  double t90;
  double t93;
  double t112;
  double t113;
  double t114;
  double t115;
  double t118;
  double t119;
  double t129;
  double t133;
  double t135;
  double t140;
  double t142;
  double t145;
  double A[3];
  A[0] = 0;
  A[1] = 0;
  A[2] = 0;
  t2 = cos(q1);
  t3 = cos(q2);
  t5 = sin(q1);
  t6 = cos(rx2);
  t7 = t5 * t6;
  t8 = sin(q2);
  t10 = -t2 * t3 + t7 * t8;
  t11 = t10 * xe;
  t14 = t2 * t8 + t7 * t3;
  t15 = t14 * ye;
  t16 = sin(rx2);
  t24 = t2 * t6;
  t26 = t5 * t8 - t24 * t3;
  t30 = t5 * t3 + t24 * t8;
  t32 = t26 * xe + t30 * ye;
  A[0] = ((t11 + t15 - t5 * t16 * ze - t2 * tx2 + t5 * ty2) * dq1 + t32 * dq2) * dq1 + (t32 * dq1 + (t11 + t15) * dq2) * dq2 + (-t30 * xe + t26 * ye + t2 * t16 * ze - t5 * tx2 - t2 * ty2) * ddq1 + (-t14 * xe + t10 * ye) * ddq2;
  t55 = cos(rx1);
  t56 = t55 * t5;
  t57 = t56 * t3;
  t58 = t55 * t2;
  t59 = t6 * t8;
  t60 = t58 * t59;
  t63 = t56 * t8;
  t64 = t6 * t3;
  t65 = t58 * t64;
  t68 = t16 * ze;
  t76 = -t58 * t8 - t56 * t64;
  t80 = -t58 * t3 + t56 * t59;
  t82 = t76 * xe + t80 * ye;
  t87 = sin(rx1);
  t88 = t87 * t16;
  t90 = -t57 - t60 + t88 * t8;
  t93 = t63 - t65 + t88 * t3;
  A[1] = (((-t57 - t60) * xe + (t63 - t65) * ye + t58 * t68 - t56 * tx2 - t58 * ty2) * dq1 + t82 * dq2) * dq1 + (t82 * dq1 + (t90 * xe + t93 * ye) * dq2) * dq2 + (-t80 * xe + t76 * ye + t56 * t68 + t58 * tx2 - t56 * ty2) * ddq1 + (-t93 * xe + t90 * ye) * ddq2;
  t112 = t87 * t5;
  t113 = t112 * t3;
  t114 = t87 * t2;
  t115 = t114 * t59;
  t118 = t112 * t8;
  t119 = t114 * t64;
  t129 = -t114 * t8 - t112 * t64;
  t133 = -t114 * t3 + t112 * t59;
  t135 = t129 * xe + t133 * ye;
  t140 = t55 * t16;
  t142 = -t113 - t115 - t140 * t8;
  t145 = t118 - t119 - t140 * t3;
  A[2] = (((-t113 - t115) * xe + (t118 - t119) * ye + t114 * t68 - t112 * tx2 - t114 * ty2) * dq1 + t135 * dq2) * dq1 + (t135 * dq1 + (t142 * xe + t145 * ye) * dq2) * dq2 + (-t133 * xe + t129 * ye + t112 * t68 + t114 * tx2 - t112 * ty2) * ddq1 + (-t145 * xe + t142 * ye) * ddq2;
  cgret[0] = A[0];
  cgret[1] = A[1];
  cgret[2] = A[2];
}

void CBanmay::dhn_huong1 (
	double orx,
	double ory,
	double orz,
	double cgret[2])
{
	double A[2];
  A[0] = 0;
  A[1] = 0;
  A[0] = atan2((-orz * orx0 * orx0 + orz0 * sqrt(-orx0 * orx0 * (-orz0 * orz0 + orz * orz - orx0 * orx0))) / (orz0 * orz0 + orx0 * orx0) / orx0, (orz0 * orz + sqrt(-orx0 * orx0 * (-orz0 * orz0 + orz * orz - orx0 * orx0))) / (orz0 * orz0 + orx0 * orx0));
  A[1] = atan2(-(ory * sqrt(orx * orx * (ory * ory + orx * orx - ory0 * ory0)) - ory0 * orx * orx) / (ory * ory + orx * orx) / orx, (ory0 * ory + sqrt(orx * orx * (ory * ory + orx * orx - ory0 * ory0))) / (ory * ory + orx * orx));
  cgret[0] = A[0];
  cgret[1] = A[1];
}

void CBanmay::dhn_huong2(
  double orx,
  double ory,
  double orz,
  double cgret[2])
{
  double A[2];
  A[0] = 0;
  A[1] = 0;
  A[0] = atan2(-(orz * orx0 * orx0 + orz0 * sqrt(-orx0 * orx0 * (-orz0 * orz0 + orz * orz - orx0 * orx0))) / (orz0 * orz0 + orx0 * orx0) / orx0, -(-orz0 * orz + sqrt(-orx0 * orx0 * (-orz0 * orz0 + orz * orz - orx0 * orx0))) / (orz0 * orz0 + orx0 * orx0));
  A[1] = atan2((ory * sqrt(orx * orx * (ory * ory + orx * orx - ory0 * ory0)) + ory0 * orx * orx) / (ory * ory + orx * orx) / orx, -(-ory0 * ory + sqrt(orx * orx * (ory * ory + orx * orx - ory0 * ory0))) / (ory * ory + orx * orx));
  cgret[0] = A[0];
  cgret[1] = A[1];
}

void CBanmay::dhn_viphan (
  double q1,
  double q2,
  double orx,
  double ory,
  double orz,
  double dorx,
  double dory,
  double dorz,
  double cgret[2])
{
	double A[2];
	A[0] = 0;
	A[1] = 0;
	A[0] = -dorz / (cos(q1) * orx0 + sin(q1) * orz0);
	A[1] = -(sin(q2) * dorx + cos(q2) * dory) / (cos(q2) * orx - sin(q2) * ory);
	cgret[0] = A[0];
	cgret[1] = A[1];
}

void CBanmay::dhn_viphan2 (
  double q1,
  double q2,
  double orx,
  double ory,
  double orz,
  double dq1,
  double dq2,
  double dorx,
  double dory,
  double dorz,
  double ddorx,
  double ddory,
  double ddorz,
  double cgret[2])
{
  double t17;
  double t18;
  double t19;
  double t2;
  double t20;
  double t24;
  double t25;
  double t30;
  double t32;
  double t4;
  double t6;
  double t7;
  double A[2];
  A[0] = 0;  A[1] = 0;
  t2 = cos(q1);
  t4 = sin(q1);
  t6 = t2 * orx0 + t4 * orz0;
  t7 = t6 * t6;
  A[0] = dorz / t7 * (-t4 * orx0 + t2 * orz0) * dq1 - 0.1e1 / t6 * ddorz;
  t17 = cos(q2);
  t18 = t17 * dorx;
  t19 = sin(q2);
  t20 = t19 * dory;
  t24 = t17 * orx - t19 * ory;
  t25 = 0.1e1 / t24;
  t30 = t24 * t24;
  t32 = (t19 * dorx + t17 * dory) / t30;
  A[1] = (-(t18 - t20) * t25 + t32 * (-t19 * orx - t17 * ory)) * dq2 + t32 * t18 - t32 * t20 - t19 * t25 * ddorx - t17 * t25 * ddory;
  cgret[0] = A[0];
  cgret[1] = A[1];
}

void CBanmay::M_RR (double q1, double q2, double cgret[2][2])
{
  double M_M[2][2];
  double t1;
  double t11;
  double t14;
  double t16;
  double t17;
  double t19;
  double t22;
  double t24;
  double t26;
  double t27;
  double t28;
  double t3;
  double t36;
  double t41;
  double t42;
  double t45;
  double t50;
  double t54;
  double t58;
  double t59;
  double t6;
  double t72;
  double t73;
  double t75;
  double t77;
  double t8;
  double t89;
  double t9;
  double t90;
  double t91;
  double t96;
  double t104;
  double t109;
  double t118;
  double t123;
  double t132;
  double t134;
  double t136;
  double t138;
  M_M[0][0] = 0;
  M_M[0][1] = 0;
  M_M[1][0] = 0;
  M_M[1][1] = 0;
  t1 = sin(q1);
  t3 = cos(q1);
  t6 = pow(-t1 * xCi1 - t3 * yCi1, 0.2e1);
  t8 = cos(rx1);
  t9 = t8 * t3;
  t11 = t8 * t1;
  t14 = pow(t9 * xCi1 - t11 * yCi1, 0.2e1);
  t16 = sin(rx1);
  t17 = t3 * t16;
  t19 = t16 * t1;
  t22 = pow(t17 * xCi1 - t19 * yCi1, 0.2e1);
  t24 = cos(q2);
  t26 = cos(rx2);
  t27 = t3 * t26;
  t28 = sin(q2);
  t36 = sin(rx2);
  t41 = (-t1 * t24 - t28 * t27) * xCi2 + (t28 * t1 - t27 * t24) * yCi2 + t3 * t36 * zCi2 - t1 * tx2 - t3 * ty2;
  t42 = t41 * t41;
  t45 = t28 * t26;
  t50 = t26 * t24;
  t54 = t36 * zCi2;
  t58 = (t9 * t24 - t11 * t45) * xCi2 + (-t9 * t28 - t11 * t50) * yCi2 + t11 * t54 + t9 * tx2 - t11 * ty2;
  t59 = t58 * t58;
  t72 = (t17 * t24 - t19 * t45) * xCi2 + (-t28 * t17 - t19 * t50) * yCi2 + t19 * t54 + t17 * tx2 - t19 * ty2;
  t73 = t72 * t72;
  t75 = t36 * t28;
  t77 = t36 * t24;
  t89 = t75 * Ixz2;
  t90 = t77 * Iyz2;
  t91 = t26 * Iz2;
  M_M[0][0] = m1 * t6 + m1 * t14 + m1 * t22 + Iz1 + m2 * t42 + m2 * t59 + m2 * t73 + (t75 * Ix2 + t77 * Ixy2 + t26 * Ixz2) * t36 * t28 + (t75 * Ixy2 + t77 * Iy2 + t26 * Iyz2) * t36 * t24 + (t89 + t90 + t91) * t26;
  t96 = t1 * t26;
  t104 = (-t3 * t28 - t96 * t24) * xCi2 + (-t3 * t24 + t96 * t28) * yCi2;
  t109 = t16 * t36;
  t118 = (-t11 * t28 + t9 * t50 - t109 * t24) * xCi2 + (-t11 * t24 - t9 * t45 + t109 * t28) * yCi2;
  t123 = t8 * t36;
  t132 = (-t19 * t28 + t17 * t50 + t123 * t24) * xCi2 + (-t19 * t24 - t17 * t45 - t123 * t28) * yCi2;
  M_M[0][1] = m2 * t41 * t104 + m2 * t58 * t118 + m2 * t72 * t132 + t89 + t90 + t91;
  M_M[1][0] = M_M[0][1];
  t134 = t104 * t104;
  t136 = t118 * t118;
  t138 = t132 * t132;
  M_M[1][1] = m2 * t134 + m2 * t136 + m2 * t138 + Iz2;
  cgret[0][0] = M_M[0][0];
  cgret[0][1] = M_M[0][1];
  cgret[1][0] = M_M[1][0];
  cgret[1][1] = M_M[1][1];
  }

void CBanmay::V_RR (double q1, double q2, double dq1, double dq2, double cgret[2])
{
  double t12;
  double t13;
  double t15;
  double t18;
  double t2;
  double t23;
  double t24;
  double t26;
  double t29;
  double t34;
  double t36;
  double t37;
  double t38;
  double t4;
  double t40;
  double t44;
  double t46;
  double t52;
  double t54;
  double t56;
  double t57;
  double t60;
  double t61;
  double t66;
  double t69;
  double t7;
  double t71;
  double t74;
  double t76;
  double t78;
  double t83;
  double t84;
  double t85;
  double t88;
  double t89;
  double t95;
  double t99;
  double t103;
  double t109;
  double t110;
  double t111;
  double t114;
  double t115;
  double t121;
  double t130;
  double t131;
  double t136;
  double t137;
  double t142;
  double t143;
  double t145;
  double t147;
  double t148;
  double t153;
  double t167;
  double t168;
  double t171;
  double t177;
  double t180;
  double t182;
  double t185;
  double t187;
  double t190;
  double t192;
  double t195;
  double t197;
  double t199;
  double t204;
  double t207;
  double t208;
  double t211;
  double t215;
  double t218;
  double t222;
  double t224;
  double t225;
  double t227;
  double t244;
  double t246;
  double t248;
  double A[2];
  A[0] = 0;
  A[1] = 0;
  t2 = sin(q1);
  t4 = cos(q1);
  t7 = m1 * (-t2 * xCi1 - t4 * yCi1);
  t12 = cos(rx1);
  t13 = t12 * t4;
  t15 = t12 * t2;
  t18 = m1 * (t13 * xCi1 - t15 * yCi1);
  t23 = sin(rx1);
  t24 = t23 * t4;
  t26 = t2 * t23;
  t29 = m1 * (t24 * xCi1 - t26 * yCi1);
  t34 = cos(q2);
  t36 = cos(rx2);
  t37 = t4 * t36;
  t38 = sin(q2);
  t40 = -t2 * t34 - t37 * t38;
  t44 = t2 * t38 - t37 * t34;
  t46 = sin(rx2);
  t52 = m2 * (t40 * xCi2 + t44 * yCi2 + t4 * t46 * zCi2 - t2 * tx2 - t4 * ty2);
  t54 = t2 * t36;
  t56 = -t4 * t34 + t54 * t38;
  t57 = t56 * xCi2;
  t60 = t4 * t38 + t54 * t34;
  t61 = t60 * yCi2;
  t66 = t57 + t61 - t2 * t46 * zCi2 - t4 * tx2 + t2 * ty2;
  t69 = t36 * t38;
  t71 = t13 * t34 - t15 * t69;
  t74 = t36 * t34;
  t76 = -t13 * t38 - t15 * t74;
  t78 = t46 * zCi2;
  t83 = m2 * (t71 * xCi2 + t76 * yCi2 + t15 * t78 + t13 * tx2 - t15 * ty2);
  t84 = t15 * t34;
  t85 = t13 * t69;
  t88 = t15 * t38;
  t89 = t13 * t74;
  t95 = (-t84 - t85) * xCi2 + (t88 - t89) * yCi2 + t13 * t78 - t15 * tx2 - t13 * ty2;
  t99 = t24 * t34 - t26 * t69;
  t103 = -t24 * t38 - t26 * t74;
  t109 = m2 * (t99 * xCi2 + t103 * yCi2 + t26 * t78 + t24 * tx2 - t26 * ty2);
  t110 = t26 * t34;
  t111 = t24 * t69;
  t114 = t26 * t38;
  t115 = t24 * t74;
  t121 = (-t110 - t111) * xCi2 + (t114 - t115) * yCi2 + t24 * t78 - t26 * tx2 - t24 * ty2;
  t130 = t44 * xCi2 - t40 * yCi2;
  t131 = t52 * t130;
  t136 = t76 * xCi2 - t71 * yCi2;
  t137 = t83 * t136;
  t142 = t103 * xCi2 - t99 * yCi2;
  t143 = t109 * t142;
  t145 = t46 * t34;
  t147 = t46 * t38;
  t148 = t147 * Ixy2;
  t153 = t145 * Ixy2;
  t167 = t145 * Ixz2;
  t168 = t147 * Iyz2;
  t171 = 0.2e1 * t131 + 0.2e1 * t137 + 0.2e1 * t143 + (t145 * Ix2 - t148) * t46 * t38 + (t147 * Ix2 + t153 + t36 * Ixz2) * t46 * t34 + (t153 - t147 * Iy2) * t46 * t34 - (t148 + t145 * Iy2 + t36 * Iyz2) * t46 * t38 + (t167 - t168) * t36;
  t177 = -t60 * xCi2 + t56 * yCi2;
  t180 = t23 * t46;
  t182 = -t88 + t89 - t180 * t34;
  t185 = -t84 - t85 + t180 * t38;
  t187 = t182 * xCi2 + t185 * yCi2;
  t190 = t12 * t46;
  t192 = -t114 + t115 + t190 * t34;
  t195 = -t110 - t111 - t190 * t38;
  t197 = t192 * xCi2 + t195 * yCi2;
  t199 = m2 * t66 * t177 + t131 + m2 * t95 * t187 + t137 + m2 * t121 * t197 + t143;
  t204 = t199 * dq1;
  t207 = m2 * t130 * t177;
  t208 = t57 + t61;
  t211 = m2 * t136 * t187;
  t215 = t185 * xCi2 - t182 * yCi2;
  t218 = m2 * t142 * t197;
  t222 = t195 * xCi2 - t192 * yCi2;
  t224 = t207 + t52 * t208 + t211 + t83 * t215 + t218 + t109 * t222 + t167 - t168;
  t225 = t224 * dq2;
  t227 = 0.2e1 * t207 + 0.2e1 * t211 + 0.2e1 * t218;
  A[0] = ((0.2e1 * t7 * (-t4 * xCi1 + t2 * yCi1) + 0.2e1 * t18 * (-t15 * xCi1 - t13 * yCi1) + 0.2e1 * t29 * (-t26 * xCi1 - t24 * yCi1) + 0.2e1 * t52 * t66 + 0.2e1 * t83 * t95 + 0.2e1 * t109 * t121) * dq1 / 0.2e1 + t171 * dq2 - t199 * dq2 / 0.2e1) * dq1 + (t204 / 0.2e1 + t225 - t227 * dq2 / 0.2e1) * dq2 - t7 * g1 - t18 * g2 - t29 * g3 - t52 * g1 - t83 * g2 - t109 * g3;
  t244 = m2 * t177;
  t246 = m2 * t187;
  t248 = m2 * t197;
  A[1] = (t204 + t225 / 0.2e1 - t171 * dq1 / 0.2e1) * dq1 + (t227 * dq1 + (0.2e1 * t244 * t208 + 0.2e1 * t246 * t215 + 0.2e1 * t248 * t222) * dq2 / 0.2e1 - t224 * dq1 / 0.2e1) * dq2 - t244 * g1 - t246 * g2 - t248 * g3;
  cgret[0] = A[0];
  cgret[1] = A[1];
}

/*
void CBanmay::M_RR (double q1, double q2, double cgret[2][2])
{
  double M_M[2][2];
  double t1;
  double t11;
  double t14;
  double t16;
  double t17;
  double t19;
  double t22;
  double t24;
  double t26;
  double t27;
  double t28;
  double t3;
  double t36;
  double t41;
  double t42;
  double t45;
  double t50;
  double t54;
  double t58;
  double t59;
  double t6;
  double t72;
  double t73;
  double t75;
  double t76;
  double t79;
  double t8;
  double t82;
  double t86;
  double t9;
  double t94;
  double t99;
  double t108;
  double t113;
  double t122;
  double t125;
  double t127;
  double t129;
  M_M[0][0] = 0;
  M_M[0][1] = 0;
  M_M[1][0] = 0;
  M_M[1][1] = 0;
  t1 = sin(q1);
  t3 = cos(q1);
  t6 = pow(-t1 * xCi1 - t3 * yCi1, 0.2e1);
  t8 = cos(rx1);
  t9 = t8 * t3;
  t11 = t8 * t1;
  t14 = pow(t9 * xCi1 - t11 * yCi1, 0.2e1);
  t16 = sin(rx1);
  t17 = t3 * t16;
  t19 = t16 * t1;
  t22 = pow(t17 * xCi1 - t19 * yCi1, 0.2e1);
  t24 = cos(q2);
  t26 = cos(rx2);
  t27 = t3 * t26;
  t28 = sin(q2);
  t36 = sin(rx2);
  t41 = (-t1 * t24 - t28 * t27) * xCi2 + (t28 * t1 - t27 * t24) * yCi2 + t3 * t36 * zCi2 - t1 * tx2 - t3 * ty2;
  t42 = t41 * t41;
  t45 = t28 * t26;
  t50 = t26 * t24;
  t54 = t36 * zCi2;
  t58 = (t9 * t24 - t11 * t45) * xCi2 + (-t9 * t28 - t11 * t50) * yCi2 + t11 * t54 + t9 * tx2 - t11 * ty2;
  t59 = t58 * t58;
  t72 = (t17 * t24 - t19 * t45) * xCi2 + (-t28 * t17 - t19 * t50) * yCi2 + t19 * t54 + t17 * tx2 - t19 * ty2;
  t73 = t72 * t72;
  t75 = t36 * t36;
  t76 = t28 * t28;
  t79 = t24 * t24;
  t82 = t26 * t26;
  M_M[0][0] = m1 * t6 + m1 * t14 + m1 * t22 + Iz1 + m2 * t42 + m2 * t59 + m2 * t73 + t75 * t76 * Ix2 + t75 * t79 * Iy2 + t82 * Iz2;
  t86 = t1 * t26;
  t94 = (-t3 * t28 - t86 * t24) * xCi2 + (-t3 * t24 + t86 * t28) * yCi2;
  t99 = t16 * t36;
  t108 = (-t11 * t28 + t9 * t50 - t99 * t24) * xCi2 + (-t11 * t24 - t9 * t45 + t99 * t28) * yCi2;
  t113 = t8 * t36;
  t122 = (-t19 * t28 + t17 * t50 + t113 * t24) * xCi2 + (-t19 * t24 - t17 * t45 - t113 * t28) * yCi2;
  M_M[0][1] = m2 * t41 * t94 + m2 * t58 * t108 + m2 * t72 * t122 + t26 * Iz2;
  M_M[1][0] = M_M[0][1];
  t125 = t94 * t94;
  t127 = t108 * t108;
  t129 = t122 * t122;
  M_M[1][1] = m2 * t125 + m2 * t127 + m2 * t129 + Iz2;
  cgret[0][0] = M_M[0][0];
  cgret[0][1] = M_M[0][1];
  cgret[1][0] = M_M[1][0];
  cgret[1][1] = M_M[1][1];
}


void CBanmay::V_RR (
  double q1,
  double q2,
  double dq1,
  double dq2,
  double cgret[2])
{
  double t12;
  double t13;
  double t15;
  double t18;
  double t2;
  double t23;
  double t24;
  double t26;
  double t29;
  double t34;
  double t36;
  double t37;
  double t38;
  double t4;
  double t40;
  double t44;
  double t46;
  double t52;
  double t54;
  double t56;
  double t57;
  double t60;
  double t61;
  double t66;
  double t69;
  double t7;
  double t71;
  double t74;
  double t76;
  double t78;
  double t83;
  double t84;
  double t85;
  double t88;
  double t89;
  double t95;
  double t99;
  double t103;
  double t109;
  double t110;
  double t111;
  double t114;
  double t115;
  double t121;
  double t130;
  double t131;
  double t135;
  double t136;
  double t140;
  double t141;
  double t142;
  double t150;
  double t156;
  double t159;
  double t161;
  double t164;
  double t166;
  double t169;
  double t171;
  double t174;
  double t176;
  double t178;
  double t183;
  double t186;
  double t187;
  double t190;
  double t194;
  double t197;
  double t201;
  double t203;
  double t204;
  double t206;
  double t223;
  double t225;
  double t227;
  double A[2];
  A[0] = 0;
  A[1] = 0;
  t2 = sin(q1);
  t4 = cos(q1);
  t7 = m1 * (-t2 * xCi1 - t4 * yCi1);
  t12 = cos(rx1);
  t13 = t12 * t4;
  t15 = t12 * t2;
  t18 = m1 * (t13 * xCi1 - t15 * yCi1);
  t23 = sin(rx1);
  t24 = t23 * t4;
  t26 = t2 * t23;
  t29 = m1 * (t24 * xCi1 - t26 * yCi1);
  t34 = cos(q2);
  t36 = cos(rx2);
  t37 = t4 * t36;
  t38 = sin(q2);
  t40 = -t2 * t34 - t37 * t38;
  t44 = t2 * t38 - t37 * t34;
  t46 = sin(rx2);
  t52 = m2 * (t40 * xCi2 + t44 * yCi2 + t4 * t46 * zCi2 - t2 * tx2 - t4 * ty2);
  t54 = t2 * t36;
  t56 = -t4 * t34 + t54 * t38;
  t57 = t56 * xCi2;
  t60 = t4 * t38 + t54 * t34;
  t61 = t60 * yCi2;
  t66 = t57 + t61 - t2 * t46 * zCi2 - t4 * tx2 + t2 * ty2;
  t69 = t36 * t38;
  t71 = t13 * t34 - t15 * t69;
  t74 = t36 * t34;
  t76 = -t13 * t38 - t15 * t74;
  t78 = t46 * zCi2;
  t83 = m2 * (t71 * xCi2 + t76 * yCi2 + t15 * t78 + t13 * tx2 - t15 * ty2);
  t84 = t15 * t34;
  t85 = t13 * t69;
  t88 = t15 * t38;
  t89 = t13 * t74;
  t95 = (-t84 - t85) * xCi2 + (t88 - t89) * yCi2 + t13 * t78 - t15 * tx2 - t13 * ty2;
  t99 = t24 * t34 - t26 * t69;
  t103 = -t24 * t38 - t26 * t74;
  t109 = m2 * (t99 * xCi2 + t103 * yCi2 + t26 * t78 + t24 * tx2 - t26 * ty2);
  t110 = t26 * t34;
  t111 = t24 * t69;
  t114 = t26 * t38;
  t115 = t24 * t74;
  t121 = (-t110 - t111) * xCi2 + (t114 - t115) * yCi2 + t24 * t78 - t26 * tx2 - t24 * ty2;
  t130 = t44 * xCi2 - t40 * yCi2;
  t131 = t52 * t130;
  t135 = t76 * xCi2 - t71 * yCi2;
  t136 = t83 * t135;
  t140 = t103 * xCi2 - t99 * yCi2;
  t141 = t109 * t140;
  t142 = t46 * t46;
  t150 = 0.2e1 * t131 + 0.2e1 * t136 + 0.2e1 * t141 + 0.2e1 * t142 * t38 * Ix2 * t34 - 0.2e1 * t142 * t34 * Iy2 * t38;
  t156 = -t60 * xCi2 + t56 * yCi2;
  t159 = t23 * t46;
  t161 = -t88 + t89 - t159 * t34;
  t164 = -t84 - t85 + t159 * t38;
  t166 = t161 * xCi2 + t164 * yCi2;
  t169 = t12 * t46;
  t171 = -t114 + t115 + t169 * t34;
  t174 = -t110 - t111 - t169 * t38;
  t176 = t171 * xCi2 + t174 * yCi2;
  t178 = m2 * t66 * t156 + t131 + m2 * t95 * t166 + t136 + m2 * t121 * t176 + t141;
  t183 = t178 * dq1;
  t186 = m2 * t130 * t156;
  t187 = t57 + t61;
  t190 = m2 * t135 * t166;
  t194 = t164 * xCi2 - t161 * yCi2;
  t197 = m2 * t140 * t176;
  t201 = t174 * xCi2 - t171 * yCi2;
  t203 = t186 + t52 * t187 + t190 + t83 * t194 + t197 + t109 * t201;
  t204 = t203 * dq2;
  t206 = 0.2e1 * t186 + 0.2e1 * t190 + 0.2e1 * t197;
  A[0] = ((0.2e1 * t7 * (-t4 * xCi1 + t2 * yCi1) + 0.2e1 * t18 * (-t15 * xCi1 - t13 * yCi1) + 0.2e1 * t29 * (-t26 * xCi1 - t24 * yCi1) + 0.2e1 * t52 * t66 + 0.2e1 * t83 * t95 + 0.2e1 * t109 * t121) * dq1 / 0.2e1 + t150 * dq2 - t178 * dq2 / 0.2e1) * dq1 + (t183 / 0.2e1 + t204 - t206 * dq2 / 0.2e1) * dq2 - t7 * g1 - t18 * g2 - t29 * g3 - t52 * g1 - t83 * g2 - t109 * g3;
  t223 = m2 * t156;
  t225 = m2 * t166;
  t227 = m2 * t176;
  A[1] = (t183 + t204 / 0.2e1 - t150 * dq1 / 0.2e1) * dq1 + (t206 * dq1 + (0.2e1 * t223 * t187 + 0.2e1 * t225 * t194 + 0.2e1 * t227 * t201) * dq2 / 0.2e1 - t203 * dq1 / 0.2e1) * dq2 - t223 * g1 - t225 * g2 - t227 * g3;
  cgret[0] = A[0];
  cgret[1] = A[1];
}

*/


} // end namespace