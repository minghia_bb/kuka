/*******************************************************************************/
/* Le Minh Nghia KSTN CDT 54 *17/09/2013* **************************************/
/* Class Scene - Luu giu cac bien khoi tao moi truong OpenGL trong lop CMpView */
/*******************************************************************************/

#pragma once
#include "Vec3D.h"

namespace opengl
{
class CScene
{
public:// du lieu dau vao
	Vec3D	m_v3dColorBgrd;			
	double	m_dAlpha;				
	Vec3D	m_v3dEye;				
	Vec3D	m_v3dCenter;			
	Vec3D	m_v3dUp;				
	double	m_dFovy, m_dNear, m_dFar;	
	bool	m_bBlend;				
	bool	m_bLight;				
	bool	m_bPerspective;			
	bool	m_bAxis;				
	Vec3D	m_pCenter;
public:
	CScene(void);
	~CScene(void);
	CScene(double eyex, double eyey, double eyez, double xc=0, double yc=0, double zc=0, double r=0, double g= 0.5, double b = 0.7);
};

}