
// mophongDoc.h : interface of the CMpDoc class
//


#pragma once
#include "OpenGLView.h"
#include "PlotDlg.h"
#include "PlotManagerDlg.h"

class CMpDoc : public CDocument
{
protected: // create from serialization only
	CMpDoc();
	DECLARE_DYNCREATE(CMpDoc)

// Attributes
public:
	/* MyCodes Begin */
	// khoi tao moi truong OpenGL trong Lop CView
	opengl::COpenGLView				m_OpenGLView;
	opengl::plot::CPlots			m_Plots;
	//
	matrixXd			m_q, m_dq, m_ddq;
	matrixXd			m_rE, m_vE, m_aE;
	matrixXd			m_tau;
	vectorXd			m_time;
	vectorXd			m_initial;		// dieu kien dau
	//
	/* MyCodes End*/
// Operations
public:
	void switchOpenGLContext(void);
// Overrides
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
#ifdef SHARED_HANDLERS
	virtual void InitializeSearchContent();
	virtual void OnDrawThumbnail(CDC& dc, LPRECT lprcBounds);
#endif // SHARED_HANDLERS

// Implementation
public:
	virtual ~CMpDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()

#ifdef SHARED_HANDLERS
	// Helper function that sets search content for a Search Handler
	void SetSearchContent(const CString& value);
#endif // SHARED_HANDLERS
public:
	afx_msg void OnOptionsDlg();
	afx_msg void OnViewViewtop();
	afx_msg void OnViewViewbottom();
	afx_msg void OnViewViewleft();
	afx_msg void OnViewViewright();
	afx_msg void OnViewViewfront();
	afx_msg void OnViewViewback();
	afx_msg void OnViewAxis();
	afx_msg void OnUpdateViewAxis(CCmdUI *pCmdUI);
	afx_msg void OnViewViewperspective();
	std::vector<double> getVector(const vectorXd& vec)
	{
		std::vector<double> ret;
		int r = vec.rows(), c = vec.cols();
		int length = (r>c?r:c);
		for (int i=0;i<length;i++)
			ret.push_back(vec(i));
		return ret;
	}
};
