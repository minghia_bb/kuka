#ifndef _STL_READER_H
#define _STL_READER_H

#include <vector>
#include "Vec3D.h"
#include <GL\glut.h>
#include <string>

#ifndef max
#define max(a,b)            (((a) > (b)) ? (a) : (b))
#endif

#ifndef min
#define min(a,b)            (((a) < (b)) ? (a) : (b))
#endif

#pragma warning( disable : 4018 4996 4244)
#ifndef STL_LABEL_SIZE
#define STL_LABEL_SIZE 80
#endif //STL_LABEL_SIZE

namespace opengl
{
//structure to hold each facet
class CFacet
{
public:
	CFacet() {}
	~CFacet() {}
	CFacet(const Vec3D& an, const Vec3D& av1, const Vec3D& av2, const Vec3D& av3) { n = an; v[0] = av1; v[1] = av2; v[2] = av3;}
	CFacet(const CFacet& p) { v[0]=p.v[0]; v[1]=p.v[1]; v[2]=p.v[2]; n=p.n;}
	Vec3D n;    //normal
	Vec3D v[3]; //vertices
};

class CStlReader
{
public:
	CStlReader(void) {Clear();}
	~CStlReader(void) {}

	CStlReader(CStlReader& s) {*this = s;}
	CStlReader& operator=(const CStlReader& s)
	{
		Facets.resize(s.Size());
		for (int i = 0; i<Facets.size(); i++)
			Facets[i] = s.Facets[i];

		return *this;
	}

	std::string ObjectName;

	bool Load(std::string filename, const Vec3D & offset = Vec3D())
	{
		FILE *fp;
		bool binary = false;
		fp = fopen(filename.c_str(), "r"); 
		if(fp == NULL) return false;

		fseek(fp, 0, SEEK_END);
		int file_size = ftell(fp); 
		int facenum;
		fseek(fp, STL_LABEL_SIZE, SEEK_SET);
		fread(&facenum, sizeof(int), 1, fp); 
		int expected_file_size = STL_LABEL_SIZE + 4 + (sizeof(short) + 12 * sizeof(float))*facenum ;
		if(file_size ==  expected_file_size) 		
			binary = true;
		unsigned char tmpbuf[128]; 
		fread(tmpbuf, sizeof(tmpbuf), 1, fp);

		for(unsigned int i = 0; i < sizeof(tmpbuf); i++)
		{
			if(tmpbuf[i] > 127)
			{
				binary = true;
				break;
			}
		}
		fclose(fp);
		bool RetVal;
		if(binary) RetVal =  LoadBinary(filename, offset);
		else RetVal = LoadAscii(filename, offset);

		return RetVal;
	}
	bool Save(std::string filename, bool Binary = true) const
	{
		FILE *fp;
		if (Binary) fp = fopen(filename.c_str(),"wb");
		else fp = fopen(filename.c_str(),"w");

		if(fp==0) return false;
		int NumFaces = (int)Facets.size();

		if(Binary)
		{
			std::string s, u;
			u = s+std::string("Hi");
			std::string tmp = ObjectName + "                                                                                                    "; 
			fwrite(tmp.c_str(),80,1,fp);
			fwrite(&NumFaces,1,sizeof(int),fp); 
			unsigned short attributes=0;
			for(int i=0; i<NumFaces; i++){
				float N[3] = {Facets[i].n.x, Facets[i].n.y, Facets[i].n.z};
				float P[9] = {Facets[i].v[0].x, Facets[i].v[0].y, Facets[i].v[0].z, 
					Facets[i].v[1].x, Facets[i].v[1].y, Facets[i].v[1].z, 
					Facets[i].v[2].x, Facets[i].v[2].y, Facets[i].v[2].z};

				fwrite(&N,3,sizeof(float),fp);
				for(int k=0;k<3;k++){fwrite(&P[3*k],3,sizeof(float),fp);}
				fwrite(&attributes,1,sizeof(short),fp);
			}
		}
		else
		{
			fprintf(fp,"solid jdh\n");
			for(int i=0; i<NumFaces; i++)
			{
				fprintf(fp,"  facet normal %13e %13e %13e\n", Facets[i].n.x, Facets[i].n.y, Facets[i].n.z);
				fprintf(fp,"    outer loop\n");
				for(int k=0; k<3; k++)
				{
					fprintf(fp,"      vertex  %13e %13e %13e\n", Facets[i].v[k].x, Facets[i].v[k].y, Facets[i].v[k].z);			
				}
				fprintf(fp,"    endloop\n");
				fprintf(fp,"  endfacet\n");
			}
			fprintf(fp,"endsolid vcg\n");
		}
		fclose(fp);
		return 0;
	}
	void ComputeBoundingBox(Vec3D& pmin, Vec3D& pmax)
	{
		if (Facets.size() == 0)
			return;

		pmin = pmax = Facets[0].v[0];

		for (int i=0; i<Facets.size(); i++)
		{
			for (int j=0; j<3; j++) {
				pmin.x = min(pmin.x, Facets[i].v[j].x);
				pmin.y = min(pmin.y, Facets[i].v[j].y);
				pmin.z = min(pmin.z, Facets[i].v[j].z);
				pmax.x = max(pmax.x, Facets[i].v[j].x);
				pmax.y = max(pmax.y, Facets[i].v[j].y);
				pmax.z = max(pmax.z, Facets[i].v[j].z);		
			}
		}
	}

	std::vector<CFacet> Facets; 
	void Clear() { Facets.clear(); ObjectName = "Default";}
	int Size() const { return (int)Facets.size(); }

	void AddFacet(const Vec3D& n, const Vec3D& v1, const Vec3D& v2, const Vec3D& v3) 
	{
		Facets.push_back(CFacet(n,v1,v2,v3));	
	}
	void AddFacet(float nx, float ny, float nz, 
		float v1x, float v1y, float v1z, 
		float v2x, float v2y, float v2z, 
		float v3x, float v3y, float v3z)
	{ 
		AddFacet(Vec3D(nx, ny, nz), Vec3D(v1x,v1y,v1z), Vec3D(v2x,v2y,v2z), Vec3D(v3x,v3y,v3z)); 
	}

public:
	// file i/o
	bool LoadBinary(std::string filename, const Vec3D & offset = Vec3D())
	{
		FILE *fp;
		fp = fopen(filename.c_str(), "rb");
		if(fp == NULL) return false;

		int facenum;
		fseek(fp, STL_LABEL_SIZE, SEEK_SET);
		fread(&facenum, sizeof(int), 1, fp);

		Clear();

		// For each triangle read the normal, the three coords and a short set to zero
		float N[3];
		float P[9];
		short attr;

		for(int i=0;i<facenum;++i) {
			fread(&N,3*sizeof(float),1,fp);
			fread(&P,3*sizeof(float),3,fp);
			fread(&attr,sizeof(short),1,fp);
			AddFacet(N[0], N[1], N[2], P[0], P[1], P[2], P[3], P[4], P[5], P[6], P[7], P[8]);
		}
		fclose(fp);

		Translate(-offset);
		return true;
	}	
	bool LoadAscii(std::string filename, const Vec3D & offset = Vec3D())
	{
		FILE *fp;
		fp = fopen(filename.c_str(), "r");
		if(fp == NULL) return false;

		long currentPos = ftell(fp);		//dau file
		fseek(fp, 0L, SEEK_END);
		long fileLen = ftell(fp);			//cuoi file
		fseek(fp,currentPos,SEEK_SET);

		Clear();

		/* Skip the first line of the file */
		while(getc(fp) != '\n') { }

		float N[3];
		float P[9];
		int cnt=0;
		int lineCnt=0;
		int ret;
		/* Read a single facet from an ASCII .STL file */
		while(!feof(fp)){
			ret = fscanf(fp, "%*s %*s %f %f %f\n", &N[0], &N[1], &N[2]); // --> "facet normal 0 0 0"
			if(ret != 3){
				lineCnt++;
				continue; 
			}
			ret=fscanf(fp, "%*s %*s"); // --> "outer loop"
			ret=fscanf(fp, "%*s %f %f %f\n", &P[0],  &P[1],  &P[2]); // --> "vertex x y z"
			if(ret!=3) return false;
			ret=fscanf(fp, "%*s %f %f %f\n", &P[3],  &P[4],  &P[5]); // --> "vertex x y z"
			if(ret!=3) return false;
			ret=fscanf(fp, "%*s %f %f %f\n", &P[6],  &P[7],  &P[8]); // --> "vertex x y z"
			if(ret!=3) return false;
			ret=fscanf(fp, "%*s"); // --> "endloop"
			ret=fscanf(fp, "%*s"); // --> "endfacet"
			lineCnt+=7;
			if(feof(fp)) break;

			AddFacet(N[0], N[1], N[2], P[0], P[1], P[2], P[3], P[4], P[5], P[6], P[7], P[8]);

		}
		fclose(fp);
		Translate(-offset);
		return true;
	}
	void Draw(bool bModelhNormals, bool bShaded)
	{
		if (bShaded) 
		{ //light
			glBegin(GL_TRIANGLES);
			for (int i=0; i<Facets.size(); i++) 
			{
				glNormal3d(Facets[i].n.x, Facets[i].n.y, Facets[i].n.z);
				for (int j=0; j<3; j++) 
				{
					glVertex3d(Facets[i].v[j].x,Facets[i].v[j].y,Facets[i].v[j].z);
				}
			}
			glEnd();
		} 
		else { // wireframe		
			for (int i=0; i<Facets.size(); i++)
			{
				glBegin(GL_LINE_LOOP);
				glNormal3d(Facets[i].n.x, Facets[i].n.y, Facets[i].n.z);
				for (int j=0; j<3; j++) 
				{
					glVertex3d(Facets[i].v[j].x,Facets[i].v[j].y,Facets[i].v[j].z);
				}

				glEnd();
			}
		}

		if (bModelhNormals)
		{
			glColor3d(1,1,0);
			glBegin(GL_LINES);
			for (int i=0; i<Facets.size(); i++) 
			{
				Vec3D c = (Facets[i].v[0] + Facets[i].v[1] + Facets[i].v[2])/3;
				Vec3D c2 = c - Facets[i].n*3;
				glVertex3d(c.x, c.y, c.z);
				glVertex3d(c2.x, c2.y, c2.z);
			}
			glEnd();
		}
	}

	//add a facet
		
	//manipulation...
	void Rotate(Vec3D ax, double a)
	{
		for (int i=0; i<Facets.size(); i++) 
		{
			for (int j=0; j<3; j++) 
			{
				Facets[i].v[j] = Facets[i].v[j].Rot(ax, a);
			}
			Facets[i].n = Facets[i].n.Rot(ax, a);
		}
	}
	void RotX(double a)
	{
		for (int i=0; i<Facets.size(); i++)
		{
			for (int j=0; j<3; j++) 
			{
				Facets[i].v[j].RotX(a);
			}
			Facets[i].n.RotX(a);
		}
	}
	void RotY(double a)
	{
		for (int i=0; i<Facets.size(); i++) 
		{
			for (int j=0; j<3; j++)
			{
				Facets[i].v[j].RotY(a);
			}
			Facets[i].n.RotY(a);
		}
	}
	void RotZ(double a)
	{
		for (int i=0; i<Facets.size(); i++)
		{
			for (int j=0; j<3; j++)
			{
				Facets[i].v[j].RotZ(a);
			}
			Facets[i].n.RotZ(a);
		}
	}
	// scale geometry
	
	void Scale(Vec3D s)
	{
		//check for zero scale factor
		if(s.x==0 || s.y==0 || s.z==0) return;
		for (int i=0; i<Facets.size(); i++) 
		{
			for (int j=0; j<3; j++)
			{
				Facets[i].v[j].x *= s.x;
				Facets[i].v[j].y *= s.y;
				Facets[i].v[j].z *= s.z;
			}
			Facets[i].n.x *= s.x;
			Facets[i].n.y *= s.y;
			Facets[i].n.z *= s.z;
			///		Facets[i].n.Normalize();
		}
	}
	// translate geometry
	void Translate(const Vec3D & d)
	{
		for (int i=0; i<Facets.size(); i++) {
			for (int j=0; j<3; j++) {
				Facets[i].v[j] += d;}}
	}
};

}
#endif