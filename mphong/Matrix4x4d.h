/*
* Copyright (c) Le Minh Nghia KSTN CDT k54 - 2013
* Class CMatrix4x4d - Matran thuan nhat
*/

#ifndef CMATRIX4X4D
#define CMATRIX4X4D

#include <cmath>
#include <GL\GL.H>
#define _FACE_DEFAULT			GL_FRONT_AND_BACK

#ifndef PI
#define PI					3.1415926535897932384626433832795
#endif //PI

#ifndef RADTODEG
#define RADTODEG(rad)		(rad)*(180.0)/(PI)
#endif //RADTODEG

#ifndef DEGTORAD
#define DEGTORAD(deg)		(deg)*(PI)/(180.0)
#endif //DEGTORAD
namespace opengl
{
class CMatrix4x4d
{
//public:
	double m[16];
public:
	// Constructions
	CMatrix4x4d(void) {Identity();};
	CMatrix4x4d(double m_[]) {for (int i = 0; i < 16; i++) m[i] = m_[i] ; }
	CMatrix4x4d(double m0,double m4,double m8, double m12,
		        double m1,double m5,double m9, double m13,
		        double m2,double m6,double m10,double m14,
		        double m3,double m7,double m11,double m15)
	{
		m[0] = m0; m[4] = m4; m[8] = m8; m[12] = m12;
		m[1] = m1; m[5] = m5; m[9] = m9; m[13] = m13;
		m[2] = m2; m[6] = m6; m[10] = m10; m[14] = m14;
		m[3] = m3; m[7] = m7; m[11] = m11; m[15] = m15;
	}

	// Destructions
	~CMatrix4x4d(void) {;}
	// Operators
	// matran don vi
	void Identity()
	{
		zeros();
		m[0]=m[5]=m[10]=m[15]=1.0;
	}
	// matran 0
	void zeros()
	{
		for (int i = 0; i < 15; i++)
			m[i] = 0.0;
	}
	// 
	CMatrix4x4d operator+(const CMatrix4x4d& mat)
	{
		for (int i=0;i<16;i++)
			m[i]+=mat[i];
		return (*this);
	}
	//
	CMatrix4x4d operator-(const CMatrix4x4d& mat)
	{
		for (int i=0;i<16;i++)
			m[i]-=mat[i];
		return (*this);
	}
	//
	CMatrix4x4d operator*(const CMatrix4x4d& mat)
	{
		double mm[16]={0};
		for (int i=0;i<4;i++)
			for (int j=0; j<4;j++)
				for (int k=0;k<4;k++)
					mm[i+j*4] += (*this)(i,k) * mat(k,j);
		for (int i=0;i<16;i++)
			m[i]=mm[i];
		return (*this);
	}
	//
	CMatrix4x4d operator*(const double& scalar)
	{
		for (int i=0;i<16;i++)
			m[i] *= scalar;
		return (*this);
	}
	// phep gan 2 ma tran
	CMatrix4x4d operator=(const CMatrix4x4d& mat)
	{
		for (int i=0;i<16;i++)
			m[i] = mat[i];
		return (*this);
	}
	//
	double operator[](int i) const
	{
		return m[i];
	}
	//
	double operator()(int i, int j) const
	{
		return m[i+j*4];
	}
	//
	double* getm()
	{
		return (m);
	}
	void getm(double mm[])
	{
		for (int i=0;i<16;i++)
			mm[i]=m[i];
	}
	// Methods
	// Chuyen vi
	void Transpose()
	{
		double tmp;
		tmp = m[1]; m[1] = m[4]; m[4] = tmp;
		tmp = m[2]; m[2] = m[8]; m[8] = tmp;
		tmp = m[3]; m[3] = m[12]; m[12] = tmp;
		tmp = m[6]; m[6] = m[9]; m[9] = tmp;
		tmp = m[7]; m[7] = m[13]; m[13] = tmp;
		tmp = m[11]; m[11] = m[14]; m[14] = tmp;
	}
	// Tinh tien
	void Translate(double x = 0, double y = 0, double z = 0)
	{
		Identity();
		m[12] = x; m[13] = y; m[14] = z;
	}
	// Quay quanh truc x
	void Rotate_X(double XAngle = 0, bool bRad = true)
	{
		double x = XAngle;
		if (!bRad)
			x = DEGTORAD(XAngle);
		Identity();
		m[1+1*4] = cos(x); m[1+2*4]=-sin(x);
		m[2+1*4] = sin(x); m[2+2*4] = cos(x);
	}
	// Quay quanh truc y
	void Rotate_Y(double YAngle = 0, bool bRad = true)
	{
		double y = YAngle;
		if (!bRad)
			y = DEGTORAD(YAngle);
		Identity();
		m[0+0*4] = cos(y); m[0+2*4] = sin(y);
		m[2+0*4] = -sin(y);	m[2+2*4]= cos(y);
	}
	// Quay quanh truc z
	void Rotate_Z(double ZAngle = 0, bool bRad = true)
	{
		Identity();
		double z = ZAngle;
		if (!bRad)
			z = DEGTORAD(ZAngle);
		m[0+0*4] = cos(z); m[0+1*4] = -sin(z);
		m[1+0*4] = sin(z); m[1+1*4] = cos(z);		
	}
	// Matrix Denavit-Harteberg
	void MatrixDH(double theta, double d, double a, double alpha, bool bRad = true)
	{
		double t_,al_;
		Identity();
		t_ = theta;
		al_ = alpha ;
		if (!bRad)	{ t_ = DEGTORAD(theta) ; al_ = DEGTORAD(alpha) ; }

		m[0] = cos(t_); m[4] = -sin(t_) * cos(al_); m[8] = sin(t_) * sin(al_); m[12] = a * cos(t_);
		m[1] = sin(t_); m[5] = cos(t_) * cos(al_); m[9] = -cos(t_) * sin(al_); m[13] = a * sin(t_);
		m[2] = 0;       m[6] = sin(al_)        ; m[10] = cos(al_)        ; m[14] = d;
	}
	// Matrix Transform Craig - Rotx(alp) - Trans(x,a) - Rotz(theta) - Trans (z,d)
	void MatrixCraig(double alpha, double a, double theta, double d, bool bRad = true)
	{
		double t_, al_;
		Identity();
		if (bRad)	{ t_ = theta; al_ = alpha; }
		else	{ t_ = DEGTORAD(theta); al_ = DEGTORAD(alpha) ; }

		m[0] = cos(t_);          m[4] = -sin(t_);                           m[12] = a;
		m[1] = sin(t_) * cos(al_); m[5] = cos(t_) * cos(al_); m[9] = -sin(al_); m[13] = -d * sin(al_);
		m[2]= sin(t_) * sin(al_); m[6] = cos(t_) * sin(al_); m[10] = cos(al_); m[14] = d * cos(al_);
	}
};

class CColor4f
{
	float rgba[4];
public:
	// Constructor
	CColor4f() {rgba[0]=rgba[1]=rgba[2]=rgba[3]=0.0;}
	// Contructor
	CColor4f(float r, float g, float b, float a = 1.0)
	{ rgba[0]=r; rgba[1]=g; rgba[2]=b; rgba[3]=a;}
	//CColor4f(double r, double g, double b, double a = 1.0)
	//{ rgba[0]=(float)r; rgba[1]=(float)g; rgba[2]=(float)b; rgba[3]=(float)a;}
	float getR() {return rgba[0];}
	float getG() {return rgba[1];}
	float getB() {return rgba[2];}
	float getA() {return rgba[3];}
	void setR(float r) {rgba[0] = r;}
	void setG(float g) {rgba[1] = g;}
	void setB(float b) {rgba[2] = b;}
	void setA(float a) {rgba[3] = a;}
	void setData(float r_, float g_, float b_, float a_ = 1.0f) 
	{ rgba[0] = r_; rgba[1] = g_; rgba[2] = b_; rgba[3] = a_;}
	float* getColor() {return rgba;}
	float* getData_f() const 
	{
		float *cl;
		cl = new float[4];
		cl[0] = rgba[0]; cl[1] = rgba[1]; cl[2] = rgba[2]; cl[3] = rgba[3];
		return cl;
	}
	// Copy contructor
	CColor4f(CColor4f& color)
	{
		rgba[0] = color.getR();
		rgba[1] = color.getG();
		rgba[2] = color.getB();
		rgba[3] = color.getA();
	}
	~CColor4f() {;}

};

class CMaterial
{
public:
	CColor4f	ambientMaterial;
	CColor4f	diffuseMaterial;
	CColor4f	specularMaterial;
	float		shininessMaterial;
	CColor4f	emissionMaterial;
protected:	
	void glMaterialC(int face, int pname, const CColor4f& color) const
	{
		glMaterialfv(face, pname, color.getData_f());
	}
public:
	CMaterial(){
		ambientMaterial.setData(0.2f, 0.2f, 0.2f, 1);
		diffuseMaterial.setData(0.8f, 0.8f, 0.8f, 1);
		specularMaterial.setData(0, 0, 0, 1);
		shininessMaterial = 0.0f;
		emissionMaterial.setData(0, 0, 0, 1);
	}

	CMaterial(const CColor4f& ambient, const CColor4f& diffuse, const CColor4f& specular, float shininess=0.0f, const CColor4f& emission = CColor4f(0, 0, 0, 1)){
		shininessMaterial = shininess;
		ambientMaterial = ambient;
		diffuseMaterial = diffuse;
		specularMaterial = specular;
		emissionMaterial = emission;
	}

	CMaterial(const CMaterial& material)
	{
		shininessMaterial = material.shininessMaterial;
		ambientMaterial = material.ambientMaterial;
		diffuseMaterial = material.diffuseMaterial;
		specularMaterial = material.specularMaterial;
		emissionMaterial = material.emissionMaterial;
	}

	CMaterial& operator=(const CMaterial& material){
		shininessMaterial = material.shininessMaterial;
		shininessMaterial = material.shininessMaterial;
		ambientMaterial = material.ambientMaterial;
		diffuseMaterial = material.diffuseMaterial;
		specularMaterial = material.specularMaterial;
		emissionMaterial = material.emissionMaterial;
		return *this;
	}

	void SetData(const CColor4f& ambient, const CColor4f& diffuse, const CColor4f& specular, float shininess = 0.0f, const CColor4f& emission=CColor4f(0, 0, 0, 1)){
		shininessMaterial = shininess;
		ambientMaterial = ambient;
		diffuseMaterial = diffuse;
		specularMaterial = specular;
		emissionMaterial = emission;
		shininessMaterial = shininess;
	}

	void ApplyToFace(int face = _FACE_DEFAULT) const{
		glMaterialC(face, GL_AMBIENT, ambientMaterial);
		glMaterialC(face, GL_DIFFUSE, diffuseMaterial);
		glMaterialC(face, GL_SPECULAR, specularMaterial);
		glMaterialC(face, GL_EMISSION, emissionMaterial);
		glMaterialf(face, GL_SHININESS, shininessMaterial);
	}

	void Reset(){
		ambientMaterial.setData(0.2f, 0.2f, 0.2f, 1);
		diffuseMaterial.setData(0.8f, 0.8f, 0.8f, 1);
		specularMaterial.setData(0, 0, 0, 1);
		shininessMaterial = 0.0f;
		emissionMaterial.setData(0, 0, 0, 1);
	}
};


static CMaterial storageMaterial[] = {
	CMaterial (CColor4f(0.0215f, 0.1745f, 0.0215f, 1.0f), CColor4f(0.07568f, 0.61424f, 0.07568f, 1.0f), CColor4f(0.633f, 0.727811f, 0.633f, 1.0f), 50.0f),
	CMaterial (CColor4f(0.135f, 0.2225f, 0.1575f, 1.0f), CColor4f(0.5489f, 0.63f, 0.316228f, 1.0f), CColor4f(0.316228f, 0.316228f, 1.0f, 1.0f), 50.0f),
	CMaterial (CColor4f(0.05375f, 0.05f, 0.06625f, 1.0f), CColor4f(0.18275f, 0.17f, 0.22525f, 1), CColor4f(0.332741f, 0.326834f, 0.346435f, 1), 50),
	CMaterial (CColor4f(0.1745f, 0.01175f, 0.01175f, 1), CColor4f(0.61424f, 0.04136f, 0.04136f, 1), CColor4f(0.727811f, 0.626959f, 0.626959f, 1), 50),
	CMaterial (CColor4f(0.1f, 0.18725f, 0.1745f, 1), CColor4f(0.39674151f, 0.74151f, 0.69102f, 1), CColor4f(0.297254f, 0.30829f, 0.306678f, 1), 50),
	CMaterial (CColor4f(0.329412f, 0.223529f, 0.027451f, 1), CColor4f(0.780392f, 0.758627f, 0.113725f, 1), CColor4f(0.992157f, 0.941176f, 0.807843f, 1), 50),
	CMaterial (CColor4f(0.2125f, 0.1275f, 0.054f, 1), CColor4f(0.741f, 0.4284f, 0.18144f, 1), CColor4f(0.393548f, 0.271906f, 0.166721f, 1), 50),
	CMaterial (CColor4f(0.25f, 0.25f, 0.25f, 1), CColor4f(0.4f, 0.4f, 0.4f, 1), CColor4f(0.774597f, 0.774597f, 0.774597f, 1), 50),
	CMaterial (CColor4f(0.19125f, 0.0735f, 0.0225f, 1), CColor4f(0.7038f, 0.27048f, 0.0828f, 1), CColor4f(0.256777f, 0.137622f, 0.086014f, 1), 50),
	CMaterial (CColor4f(0.24725f, 0.1995f, 0.0745f, 1), CColor4f(0.75164f, 0.60648f, 0.22648f, 1), CColor4f(0.628281f, 0.555802f, 0.366065f, 1), 50),
	CMaterial (CColor4f(0.19225f, 0.19225f, 0.19225f, 1), CColor4f(0.50754f, 0.50754f, 0.50754f, 1), CColor4f(0.508273f, 0.508273f, 0.508273f, 1), 50),
	CMaterial (CColor4f(0.0f, 0.0f, 0.0f, 1), CColor4f(0.01f, 0.01f, 0.01f, 1), CColor4f(0.5f, 0.5f, 0.5f, 1), 50),
	CMaterial (CColor4f(0.0f, 0.1f, 0.06f, 1), CColor4f(0.0f, 0.50980392f, 0.50980392f, 1), CColor4f(0.50196078f, 0.50196078f, 0.50196078f, 1), 50),
	CMaterial (CColor4f(0.0f, 0.0f, 0.0f, 1), CColor4f(0.1f, 0.35f, 0.1f, 1), CColor4f(0.45f, 0.55f, 0.45f, 1), 50),
	CMaterial (CColor4f(0.0f, 0.0f, 0.0f, 1), CColor4f(0.5f, 0.0f, 0.0f, 1), CColor4f(0.7f, 0.6f, 0.6f, 1), 50),
	CMaterial (CColor4f(0.02f, 0.02f, 0.02f, 1), CColor4f(0.01f, 0.01f, 0.01f, 1), CColor4f(0.4f, 0.4f, 0.4f, 1), 50),
	CMaterial (CColor4f(0.0f, 0.05f, 0.05f, 1), CColor4f(0.4f, 0.5f, 0.5f, 1), CColor4f(0.4f, 0.7f, 0.4f, 1), 50),
	CMaterial (CColor4f(0.0f, 0.05f, 0.0f, 1), CColor4f(0.4f, 0.5f, 0.4f, 1), CColor4f(0.04f, 0.7f, 0.04f, 1), 50),
	CMaterial (CColor4f(0.05f, 0.0f, 0.0f, 1), CColor4f(0.5f, 0.4f, 0.4f, 1), CColor4f(0.7f, 0.04f, 0.04f, 1), 50)
};
} // end namespace opengl
#endif // CMATRIX4X4D