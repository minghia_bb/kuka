#include "StdAfx.h"
#include "Scene.h"

namespace opengl
{
CScene::CScene(void) : 
	m_v3dColorBgrd(0.0,0.5,0.7),
	m_dAlpha(1.0),
	m_v3dEye(1800, 600, -940),
	m_v3dCenter(),
	m_v3dUp(0,1,0),
	m_dFovy(45.0),
	m_dNear(.1),
	m_dFar(10000.0),
	m_bBlend(true),
	m_bLight(true), m_bPerspective(true), m_bAxis(true),
	//
	m_pCenter()
{
}

CScene::CScene(double eyex, double eyey, double eyez, double xc, double yc, double zc, double r, double g, double b) :
	m_v3dColorBgrd(r, g, b),
	m_dAlpha(1.0),
	m_v3dEye(eyex, eyey, eyez),
	m_v3dCenter(),
	m_v3dUp(0,1,0),
	m_dFovy(45.0),
	m_dNear(.1),
	m_dFar(10000.0),
	m_bBlend(true),
	m_bLight(true), m_bPerspective(true), m_bAxis(true),
	//
	m_pCenter(xc, yc, zc)
{
}
CScene::~CScene(void)
{
}

}