/*
* Copyright (c) Le Minh Nghia Hust 12/12/2013
* robot data
*/
#ifndef ROBOT_DATA_H
#define ROBOT_DATA_H

namespace robotData {
class Robot
{
public:
	int numOfConstraints;		// so rang buoc dong hoc (3 or 6)
	Robot() : numOfConstraints(3){}
	~Robot() {}
};
} // end robotdata
#endif //ROBOT_DATA_H