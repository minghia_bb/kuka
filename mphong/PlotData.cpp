#include "stdafx.h"
#include "PlotData.h"
#include <fstream>

namespace opengl
{
	namespace plot
	{
CPlotData::CPlotData(void)
{
	numpoint = 0;
	thickness = 1.0;
	color[0] = 0.0; color[1] = 0.0; color[2] = 1.0; color[3] = 1.0;
	legends = "f(x)";
	clear();
	// 
	line_type = LINE_CONTINUOUS;
}


CPlotData::~CPlotData(void)
{
}


void CPlotData::clear(void)
{
	X.clear();
	Y.clear();
}
CPlotData::CPlotData(const CPlotData& plot)
{
	numpoint = plot.numpoint;
	legends = plot.legends;
	thickness = plot.thickness;
	for (int i = 0; i<4; i++)	color[i] = plot.color[i];
	X = plot.X;
	Y = plot.Y;
	line_type = plot.line_type;
}
	}}